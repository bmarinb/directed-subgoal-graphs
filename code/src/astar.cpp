#include "search.hpp"
/* 
Functions used to run different A* implementations
*/


// Functions to call after a search is finished (curently none)
void Search::finish_search(){
}


// Use avoidance and assumes each cell can be visited at most once.
void Search::astar_avoid_visited()
{
    a_star_init_visited();
    while(open_2_crit.size() > 0  && open_2_crit.min() + epsilon <  g_cost[goal_pos_id]){
        extract_next_avoid_visited();
        for (edge_id = search_nodes[0][u_id].first_edge; edge_id < search_nodes[0][u_id].last_edge(); edge_id++)
        {
            v_id = search_edges[0][edge_id].target;
            v_pos_id = search_pos_id[0][v_id];
            if (!avoidance_table[0][FORWARD][v_id])
            {

                edge_cost = get_cost();
                g = g_cost[u_pos_id] + edge_cost;
                V2(cout << new_edge_info() << endl);
                if (g < (g_cost[v_pos_id]-epsilon)){
                    h = get_h();
                    ;
                    repr_nodes[v_pos_id] = v_id;
                    f_cost_v = (((AstarUint) ((g+h)*(prec))) << (bits_hprec)) + (AstarUint) (h*prec);
                    V2(printf("F cost: %f + %f = %f (%llX)\n", g, h, g+h, f_cost_v)); 
                    visited_add_edge_to_open();
                } 
            }
            else count_avoid();
        }
    }
}


// Assumes each cell can be visited at most once.
void Search::astar_visited()
{
    a_star_init_visited();
    while(open_2_crit.size() > 0 && open_2_crit.min() + epsilon <  g_cost[goal_pos_id]){
        extract_next_avoid_visited();
        for (edge_id = search_nodes[0][u_id].first_edge; edge_id < search_nodes[0][u_id].last_edge(); edge_id++)
        {
            v_id = search_edges[0][edge_id].target;
            v_pos_id = search_pos_id[0][v_id];
            edge_cost = get_cost();
            g = g_cost[u_pos_id] + edge_cost;
            V2(cout << new_edge_info() << endl);
            if (g < (g_cost[v_pos_id]-epsilon)){
                h = get_h();
                repr_nodes[v_pos_id] = v_id;
                f_cost_v = (((AstarUint) ((g+h)*(prec))) << (bits_hprec)) + (AstarUint) (h*prec);
                V2(printf("F cost: %f + %f = %f (%llX)\n", g, h, g+h, f_cost_v)); 
                visited_add_edge_to_open();
            }
        }
    }
}


// Use avoidance and assumes each cell can be expanded if reached by the same f-value.
void Search::astar_avoid_pos_f()
{
    a_star_init_pos_f();
    while(open_2_crit.size() > 0 && open_2_crit.min() + epsilon <  g_cost[goal_id]){
        extract_next_avoid_pos_f();
        for (edge_id = search_nodes[0][u_id].first_edge; edge_id < search_nodes[0][u_id].last_edge(); edge_id++)
        {
            v_id = search_edges[0][edge_id].target;
            if (!avoidance_table[0][FORWARD][v_id])
            {

                edge_cost = get_cost();
                g = g_cost[u_id] + edge_cost;
                V2(cout << new_edge_info() << endl);
                if (g < (g_cost[v_id]-epsilon)){
                    h = get_h();
                    f_cost_v = (((AstarUint) ((g+h)*(prec))) << (bits_hprec)) + (AstarUint) (h*prec);
                    V2(printf("F cost: %f + %f = %f (%llX)\n", g, h, g+h, f_cost_v)); 
                    if (f_cost_v - epsilon_uint < pos_f[search_pos_id[0][v_id]])
                    {
                        pos_f[search_pos_id[0][v_id]] = f_cost_v;
                        add_edge_to_open();
                    }
                    else count_skip(false);
                }
            }
            else{
                count_avoid();
            } 
        }        
    }
}


// Returns node info
string Search::new_edge_info(){
    return node_info(v_id) + " Cost: " + std::to_string(edge_cost);
}


// Assumes each cell can be expanded if reached by the same f-value.
void Search::astar_pos_f()
{
    a_star_init_pos_f();
    while(open_2_crit.size() > 0 && open_2_crit.min() + epsilon <  g_cost[goal_id]){
        extract_next_avoid_pos_f();
        for (edge_id = search_nodes[0][u_id].first_edge; edge_id < search_nodes[0][u_id].last_edge(); edge_id++)
        {
            v_id = search_edges[0][edge_id].target;
            edge_cost = get_cost();
            g = g_cost[u_id] + edge_cost;
            V2(cout << new_edge_info() << endl);
            if (g < (g_cost[v_id]-epsilon)){
                h = get_h();
                f_cost_v = (((AstarUint) ((g+h)*(prec))) << (bits_hprec)) + (AstarUint) (h*prec);
                V2(printf("F cost: %f + %f = %f (%llX)\n", g, h, g+h, f_cost_v));

                if (f_cost_v -  epsilon_uint< pos_f[search_pos_id[0][v_id]])
                {
                    pos_f[search_pos_id[0][v_id]] = f_cost_v;
                    add_edge_to_open();
                }
                else count_skip(false);
            }
        }     
    }
}


// Vanilla A* for subgoal graphs
void Search::astar()
{
    a_star_init_pos_f();
    while(open_2_crit.size() > 0 && open_2_crit.min() + epsilon <  g_cost[goal_id] ){
        extract_next();
        for (size_t i = search_nodes[0][u_id].first_edge; i < search_nodes[0][u_id].last_edge(); i++)
        {
            v_id = search_edges[0][i].target; 
            edge_cost = get_cost();
            g = g_cost[u_id] + edge_cost;
            V2(cout << new_edge_info() << endl);
            if (g < (g_cost[v_id]-epsilon)){
                h = get_h();
                f_cost_v = (((AstarUint) ((g+h)*(prec))) << (bits_hprec)) + (AstarUint) (h*prec);
                add_edge_to_open();
            }
        }
    }
}


// Extract node for pos f variants
void Search::extract_next_avoid_pos_f(){
    open_2_crit.extract_min(u_id, u_f_cost);
    while ((u_f_cost > pos_f[search_pos_id[0][u_id]] + epsilon) && open_2_crit.size()>0){
        count_skip(true);
        V2(cout << "Extrayendo (posición inutil) "<< v2s(sgs[0][u_id].get_pos()) << " Pos index: " << search_pos_id[0][u_id] << endl);
        open_2_crit.extract_min(u_id, u_f_cost);
    }
    finish_expansion();
}


// Extract node for visited variants
void Search::extract_next_avoid_visited(){
    open_2_crit.extract_min(u_pos_id, u_f_cost);
    u_id = repr_nodes[u_pos_id];

    finish_expansion();
}


// Extract node for vanilla A*
void Search::extract_next(){
    open_2_crit.extract_min(u_id, u_f_cost);
    finish_expansion();
}


// Initialization for pos f variants
void Search::a_star_init_pos_f(){
    goal_id = problem->goal_id;
    goal_x = sgs[0][goal_id].get_x();
    goal_y = sgs[0][goal_id].get_y();
    f_cost_v = (AstarUint) (oct_dist<EdgeCost>(sgs[0][problem->start_id].get_x(), sgs[0][problem->start_id].get_y(), goal_x, goal_y)
    *prec);
    open_2_crit.insert(problem->start_id, f_cost_v);
    g_cost[problem->start_id] = 0;
}


// Initialization for visited variatns
void Search::a_star_init_visited(){
    goal_id = problem->goal_id;
    goal_pos_id = search_pos_id[0][goal_id];
    goal_x = sgs[0][goal_id].get_x();
    goal_y = sgs[0][goal_id].get_y();
    f_cost_v = (AstarUint) (oct_dist<EdgeCost>(sgs[0][problem->start_id].get_x(), sgs[0][problem->start_id].get_y(), goal_x, goal_y)
    *prec);
    u_pos_id = search_pos_id[0][problem->start_id];
    repr_nodes[u_pos_id] = problem->start_id;
    open_2_crit.insert(u_pos_id, f_cost_v);
    g_cost[u_pos_id] = 0;
}


// Grid A*
void Search::grid_astar(){
    
    goal_id = problem->goal_id;
    goal_x = get_x(goal_id);
    goal_y = get_y(goal_id);
    f_cost_v = (AstarUint) (oct_dist<EdgeCost>(get_x(problem->start_id), get_y(problem->start_id), goal_x, goal_y)*prec);
    open_2_crit.insert(problem->start_id, f_cost_v);
    g_cost[problem->start_id] = 0;

    while(open_2_crit.size() > 0){

        open_2_crit.extract_min(u_id, u_f_cost);

        V2(
        cout << "------------------" << endl << "Iteración: " << n_expansions << " Open size: " << open_2_crit.size() << endl;
        printf("Extrayendo %s G: %f F: %llX \n", v2s(i2p(u_id)).c_str(), g_cost[u_id], u_f_cost);
         );
        n_expansions++;
        V2(id_expansions.push_back(u_id));    
        if (u_id == goal_id) break;  
        u_node = &grid_graph->nodes[u_id];
        u_x = u_node->get_x();
        u_y = u_node->get_y();

        // 8bit nbs
        for (size_t i = 0; i < 8; i++)
        {
            if (get_nth_byte(u_node->nbs, i)){
                v_id = u_id + dirs.dindex[i];
                edge_cost = is_cardinal(i)? 1:r2;
                g = g_cost[u_id] + edge_cost;
                V2(cout << "NB: " << v2s(i2p(v_id)) << " g: " << g << endl);
                if (g < (g_cost[v_id]-epsilon)){
                    h = oct_dist<float>(get_x(v_id), get_y(v_id), goal_x, goal_y);
                    V2(cout << "H: " << h << endl);
                    f_cost_v = (((AstarUint) ((g+h)*(prec))) << (bits_hprec)) + (AstarUint) (h*prec);
                    add_edge_to_open();
                }
            }
        }
        
    }
}


// Add edge to open (visited variants)
void Search::visited_add_edge_to_open(){
    V2(cout << " Adding node to open" << endl);
    parents[v_pos_id] = u_pos_id;
    g_cost[v_pos_id] = g;
    if (open_2_crit.in_queue(v_pos_id)){
        V2(cout << "Node was in open" << endl);
        open_2_crit.decrease_key(v_pos_id,f_cost_v);
    }   
    else{
        V2(cout << "New Node" << endl);
        open_2_crit.insert(v_pos_id,f_cost_v);
    }
    n_rel_per_exp ++;  
}


// Add edge to open 
void Search::add_edge_to_open(){
    V2(cout << " Adding node to open" << endl);
    parents[v_id] = u_id;
    g_cost[v_id] = g;
    if (open_2_crit.in_queue(v_id)){
        V2(cout << "Node was in open" << endl);
        open_2_crit.decrease_key(v_id,f_cost_v);
    }   
    else{
        V2(cout << "New Node" << endl);
        open_2_crit.insert(v_id,f_cost_v);
    }
    n_rel_per_exp ++;   
}


// Refine path (subgoal graphs)
void Search::forward_G_refine_path(){
    G_path = &problem->g_path_indices;
    G_path->push_back(problem->sg_path_indices[0]);
    for (size_t i = 0; i < problem->sg_path_indices.size()-1; i++)
    {
        // Maybe substract first then get coordinate.
        actual_index = problem->sg_path_indices[i];
        dx = get_x(problem->sg_path_indices[i+1]) - get_x(actual_index);
        dy = get_y(problem->sg_path_indices[i+1]) - get_y(actual_index);
        abs_dx = abs(dx);
        abs_dy = abs(dy);
        if (abs_dx > abs_dy){
            n_moves_diag = abs_dy;
            n_moves_card = abs_dx - abs_dy;
            diag_dindex = dirs.dindex[dirs.arr2index(sgn(dx), sgn(dy))];
            card_dindex = dirs.dindex[dirs.arr2index(sgn(dx), 0)];

        } else {
            n_moves_diag = abs_dx;
            n_moves_card = abs_dy - abs_dx;
            diag_dindex = dirs.dindex[dirs.arr2index(sgn(dx), sgn(dy))];
            card_dindex = dirs.dindex[dirs.arr2index(0, sgn(dy))];
        }
        for (int j = 0; j < n_moves_diag; j++)
        {
            actual_index += diag_dindex;
            G_path->push_back(actual_index);
        }
        for (int j = 0; j < n_moves_card; j++)
        {
            actual_index += card_dindex;
            G_path->push_back(actual_index);
        }
    }
}


// Recover and refine path
void Search::forward_search_refine_path(){
    recover_path();
    forward_G_refine_path();
    // This function verifies that the solution is a path on G with the ground truth cost.
    // problem->validate_path();
}


// Recover path after search is finished
void Search::recover_path(){    
    problem->cost = g_cost[(visit==AVOID_VISITED_POSITIONS)? goal_pos_id: goal_id];
    problem->valid = problem->cost < inf;
    if (not problem->valid) return;
    if (sub_algorithm == GRID_ASTAR){
        parent_id = goal_id;
        while (parent_id != problem->start_id){
            problem->g_path_indices.push_back(parent_id);
            parent_id = parents[parent_id];
        } 
        problem->g_path_indices.push_back(problem->start_id);
    }
    else if (visit == AVOID_VISITED_POSITIONS){ 
        parent_id = goal_pos_id;
        u_pos_id = search_pos_id[0][problem->start_id];
        while (parent_id != u_pos_id){
            problem->sg_path_indices.push_back(sgs[0][repr_nodes[parent_id]].mindex);
            V2(problem->id_path.push_back(repr_nodes[parent_id]));
            parent_id = parents[parent_id];
        } 
        V2(problem->id_path.push_back(problem->start_id));
        problem->sg_path_indices.push_back(sgs[0][problem->start_id].mindex);
        std::reverse(problem->sg_path_indices.begin(), problem->sg_path_indices.end());
        V2(std::reverse(problem->id_path.begin(), problem->id_path.end()));
    }
    else{        
        parent_id = goal_id;
        while (parent_id != problem->start_id){
            problem->sg_path_indices.push_back(sgs[0][parent_id].mindex);
            V2(problem->id_path.push_back(parent_id));
            parent_id = parents[parent_id];
        } 
        V2(problem->id_path.push_back(problem->start_id));
        problem->sg_path_indices.push_back(sgs[0][problem->start_id].mindex);
        std::reverse(problem->sg_path_indices.begin(), problem->sg_path_indices.end());
        V2(std::reverse(problem->id_path.begin(), problem->id_path.end()));
    }   
}