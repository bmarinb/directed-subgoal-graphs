#include "mapa.hpp"



/* 
Mapa (map in spanish, to avoid collision with (unordered) map)

Builds grid graph G and matrix A from a .npy or .map file.
Provides basic map interface

 */


//  Map initialization. Can build a map from .npy or .map files
Mapa::Mapa(string map_path, string map_name, string benchmark,  bool save, bool rotate_map, uint8_t free, uint8_t obs) : 
folder(map_path), name(get_prefix(map_name)),  benchmark(benchmark), rotate(rotate_map), free(free), obs(obs){
    type = get_extension(map_name);
    auto ext = find(valid_extensions.begin(), valid_extensions.end(), type);
    if (ext == valid_extensions.end()){
        cout << "Invalid extension" << endl;
        exit(1);
    }
    string complete_path = map_path + "/" + map_name;
    if (type.compare("npy") == 0){
        load_npy(complete_path);
    } else if (type.compare("map")==0){
        load_map(complete_path);
    }
    cout <<"Map size is: " <<  v2s(size) << endl;
    dirs.update_dindex();
    if (save) to_npy();
} 

void Mapa::load_npy(string path){
    cnpy::NpyArray arr = cnpy::npy_load(path);
    size = {(Pos)arr.shape[0],(Pos) arr.shape[1]};
    matrix.resize(size[0]*size[1]);
    std::copy(&arr.data<uint8_t>()[0], &arr.data<uint8_t>()[size[0]* size[1]], matrix.data());
    for (size_t i = 0; i < size[0]*size[1]; i++)
    {
        if (matrix[i] == 255){
            matrix[i] = free;
        }
        else{
            matrix[i] = obs;
        }
    }
}




// Check if a diagonal move is valid
bool Mapa::check_diag_move(PosIndex pos_index, Dir diag) const {
    return is_obstacle(move(pos_index, diag, 1));
}

// Load a map from .map files
void Mapa::load_map(std::string path){
    std::ifstream mapfile;
    mapfile.open(path);
    cout << "Reading map from: " << path << endl;
    if (!mapfile) {
    std::cerr << "Unable to open map file";
    exit(1);   // call system to stop
    }
    string line;
    
    getline(mapfile, line);
    getline(mapfile, line);
    Pos or_height, or_width;
    sscanf(line.c_str(), "height %u", &or_height);
    getline(mapfile, line);
    sscanf(line.c_str(), "width %u", &or_width);
    getline(mapfile, line);
    size.resize(2);
    size[0] = rotate ? or_width:or_height;
    size[1] = rotate ? or_height:or_width;
    width = size[1];
    matrix.resize(size[0]*size[1]);
    uint map_value;
    uint index;
    for (size_t i = 0; i < or_height; i++)
    {
        getline(mapfile, line);
        for (size_t j = 0; j < std::min((uint)line.size(), (uint)size[1]); j++)
        {
            
            if (line[j] == 46 or line[j] == 71 or line[j] == 'S'){
                map_value = free;
            }
            else if(line[j] == 64 or line[j] == 84 or line[j] == 79 or line[j] == 'W'){
                map_value = obs;
            } 
            else{
                cout << "Found: " << (int)line[j] << "|" << line[j] <<  " Quiting..." << endl;

                exit(1);
            }
            index = rotate?( i + j*size[1]) : (j + i*size[1]);
            matrix[index] = map_value;
        }
    }
    mapfile.close();
}


// Save map as npy
void Mapa::to_npy() const {
    cnpy::npy_save("../maps/"+ name + ".npy", matrix.data(),{(size_t)size[0], (size_t)size[1]}, "w");
};


// Simulates a movement and returns the final destination if valid.
PosIndex Mapa::move(PosIndex pos_index, Dir move_dir, int n_steps) const{
    PosIndex step_index = pos_index + n_steps*dirs.dindex[move_dir];
    if (is_inside(step_index)) return step_index;
    return -1;
}


// Test reachabililty between neighbors. 
bool Mapa::is_reachable_from(PosIndex pos_index, Dir dir_index) const{
    PosIndex final_pos_index = pos_index + dirs.dindex[dir_index];
    if (is_cardinal(dir_index)) return is_free(pos_index) and is_free(final_pos_index);
    return is_diagonal_reachable(pos_index, dir_index);
}



// Reachability between diagonal neighbors.
bool Mapa::is_diagonal_reachable(PosIndex pos_index, Dir d) const {
    Dir c1, c2;
    PosIndex dest_d, dest_c1, dest_c2;
    c1 = (d+1)&7;
    c2 = (d-1)&7;
    dest_d = pos_index + dirs.dindex[d];
    dest_c1 = pos_index + dirs.dindex[c1];
    dest_c2 = pos_index + dirs.dindex[c2];
    bool f0, f1, f2, f3;
    f0 = is_free(pos_index);
    f1 = is_free(dest_d);
    f2 = is_free(dest_c1);
    f3 = is_free(dest_c2);
    return f0 and f1 and f2 and f3; 
}

