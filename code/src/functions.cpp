#include "functions.hpp"
#include <string>
#include <math.h>


/* 
Functions
General-purpose functions
*/

// Directions initializer.
Directions::Directions(array<APos, 8> _arrays, 
        array<string, 9>  _names)
        : arrays(_arrays), names(_names) {
    uint index;
    for (size_t i = 0; i < 8; i++){
        index = 4 + get_index(arrays[i], 3);
        indexes[index] = i;
    }
}


// Set direction delta index based on widht of the grid.
void Directions::update_dindex(){
    for (size_t i = 0; i < 8; i++)
    {
        dindex[i] = mindex(arrays[i]);
    }
}


float sin45 = sqrt(2)/2;
float r2 = sqrt(2);
float r2p = r2-1;
string placeholder = "";
Directions dirs;
int width;
string endl = "\n";


// Number of bits that are needed to represent the heuristic in a grid graph with size map_size.
uint bits_for_heur(vector<Pos> map_size)
{
    float max_h = octile_distance<float>(APos({0, 0}), {map_size[0], map_size[1]});
    uint max_int = (uint)(max_h);
    int n_bits = ceil(log2(max_int));
    return n_bits;
}


// Number of bits that are needed to represent the g-value in a grid graph with size map_size.
uint bits_for_g(vector<Pos> map_size)
{
    float max_g = (float)map_size[0]*(float)map_size[1];
    AstarUint max_int = (AstarUint)(max_g);
    int n_bits = ceil(log2(max_int));
    return n_bits;
}


// Number of movements in diagonal and cardinal directions needed to move from pos1 to pos2 in the freespace.
array<Pos, 2> octile_moves(const APos& pos1, const APos& pos2){
    Pos dx = std::abs((signed) (pos1[0]-pos2[0]));
    Pos dy = std::abs((signed) (pos1[1]-pos2[1]));
    if (dx>dy)
    {
        return array<Pos, 2>{dx-dy, dy};
    } 
    return array<Pos, 2>{dy-dx, dx};
}


// Vector as a string.
string v2s(const APos& arr){
    return "(" + std::to_string(arr[0]) + ", " + std::to_string(arr[1]) + ")";
}


// Vector as a string.
string v2s(const vector<string>& arr, bool reversed){
    if (arr.size() == 0) return "()";
    string str = "(";
    if (reversed){
        for (auto it = arr.rbegin(); it != arr.rend(); ++it){
            str += *it + ",";
        }
    } else{
        for (auto it = arr.begin(); it != arr.end(); ++it){
            str += *it + ",";
        }  
    }
    if (arr.size()> 0){
        str = str.substr(0, str.size()-1) + ")";
    }
    return str;
}


// 2-d direction array as direction index.
Dir Directions::arr2index(int x, int y){
    return indexes[4 + get_index(x, y, 3)];
}


// 2-d direction array as direction index.
Dir Directions::arr2index(const APos& vec){
    return indexes[4 + get_index(vec, 3)];
}


// 2-d direction array as direction name.
std::string Directions::arr2names(const APos& vec){
    return names[arr2index(vec)];
}



string get_extension(string str){
    uint dot_index = str.find(".");
    if (dot_index < 0) {
        return "";
    }
    return str.substr(dot_index + 1, str.size());
}  

string get_prefix(string str){
    uint dot_index = str.find(".");
    if (dot_index < 0) {
        return str;
    }
    return str.substr(0, dot_index);
}


// Set a timer
std::chrono::_V2::system_clock::time_point set_timer(){
    return std::chrono::system_clock::now();
}


// End timer and return elapsed time
int end_timer(std::chrono::_V2::system_clock::time_point start){
    return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now() - start).count();
}


// Save json to filepath
void save_json(const nlohmann::json& json ,string filepath){
    std::ofstream json_file(filepath);
    json_file << json;
    json_file.close();

}


// Save json from filepath
bool load_json(nlohmann::json& json, string filepath){
    std::ifstream file(filepath);
    if (file.fail()) return false;
    file >> json;
    file.close();
    return true;
}


// Get memory usage.
long get_mem_usage(){
    struct rusage myusage;
    getrusage(RUSAGE_SELF, &myusage);
    return myusage.ru_maxrss;
}


// Returns the name of a vector of directions as string.
string dirs_to_names(const vector<Dir>& direcs){
    string out = "";
    for (Dir dir_index: direcs){
        out += dirs.names[dir_index] + " ";
    }
    return out;
}


// Returns a vector with all the directions of subggoals present in a cell.
void dirs_presence(IsNodeDir cell_info, vector<Dir>& dirs_present){
    dirs_present.clear();
    for (size_t i = 0; i < 8; i++)
    {
        if (get_nth_byte(cell_info, i)) dirs_present.push_back(i);
    }
}


// Returns a vector with all the name of the directions of subggoals present in a cell.
string node_presence(IsNodeDir cell_info){
    vector<Dir> dirs_present;
    dirs_presence(cell_info, dirs_present);
    return dirs_to_names(dirs_present);
}