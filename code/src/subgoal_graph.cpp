#include <utility> 
#include <limits>
#include <assert.h>
#include "subgoal_graph.hpp"


/* 
Subgoal graph
Meta-class that implements the structure of all subgoal graphs and calls instance-specific subgoal graphs.
 */

using namespace std::placeholders;
SubgoalGraph::~SubgoalGraph(){
    if (do_CH){
        delete bid_search_nodes[FORWARD];
        if (directed_graph){
            delete bid_search_nodes[BACKWARD];
        }
    }
}

string SubgoalGraph::subgoal_info(NodeId sg_id){
    return std::to_string(sg_id) + ": " +  sgs[sg_id].info() + (directed_graph? (" towards " +  dirs.names[dir_indexes[sg_id]]) : ""); 
}

string SubgoalGraph::node_info(NodeId node_id, bool nbs){
    string s = std::to_string(node_id) + ": " + subgoal_info(node_id) + " " + search_nodes[node_id].info() + endl;
    if (!nbs) return s;
    for (EdgeId eid = search_nodes[node_id].first_edge; eid < search_nodes[node_id].first_edge + search_nodes[node_id].n_edges; eid++)
    {
        s += subgoal_info(search_edges[eid].target) + endl;
    }
    return s;
}

string SubgoalGraph::rnode_info(NodeId node_id, bool nbs){
    string s = std::to_string(node_id) + ": " + subgoal_info(node_id) + " " + rsearch_nodes[node_id].info() + endl;
    if (!nbs) return s;
    for (EdgeId eid = rsearch_nodes[node_id].first_edge; eid < rsearch_nodes[node_id].first_edge + rsearch_nodes[node_id].n_edges; eid++)
    {
        s += subgoal_info(rsearch_edges[eid].target) + endl;
    }
    return s;
}


string SubgoalGraph::bid_node_info(NodeId node_id){
    string s = subgoal_info(node_id) + " " + bid_search_nodes[is_goal][0][node_id].info() + endl;
    for (EdgeId eid = bid_search_nodes[is_goal][0][node_id].first_edge; eid < bid_search_nodes[is_goal][0][node_id].last_edge(); eid++)
    {
        s += CH_edges[eid].info() + endl;
    }
    return s;
}


void SubgoalGraph::build_search_edges(){

    vector<Edge> old_search_edges = search_edges;
    search_edges.resize(get_n_edges() + get_n_nodes());
    current_edge = 0;
    EdgeId new_first_edge;
    int i = 0;
    for (Node& node: search_nodes){
        new_first_edge = current_edge;
        for (EdgeId eid = node.first_edge; eid < node.first_edge + node.n_edges; eid++)
        {
            search_edges[current_edge] = old_search_edges[eid];
            current_edge ++;
        }
        node.first_edge = new_first_edge;
        current_edge ++;
    }
    sgs.reserve(size() + 2);
    search_edges.reserve(size() + 30);
}


SubgoalGraph::SubgoalGraph(Mapa* _mapa, uint _gtype, uint _variant, bool debug_connection, NodeId _debug_node, bool do_CH, bool SG_from_scratch, bool CH_from_scratch, string custom_graph_name) : mapa(_mapa), gtype(_gtype), variant(_variant), debug_connection(debug_connection), debug_node(_debug_node), do_CH(do_CH), SG_from_scratch(SG_from_scratch), CH_from_scratch(CH_from_scratch), custom_graph_name(custom_graph_name){
    CH_loaded = false;
    directed_graph = gtype != SG;
    CH_directed = do_CH and directed_graph;
    cardinal_nodes = in_vector(gtype, {JP, JPD});
    merge_nodes = in_vector(gtype, {DSG, JPD});

    #if defined(ONLY_MERGED)
    assert(merge_nodes);
    #elif defined(NOT_MERGED)
    assert(not merge_nodes);
    #elif defined(ONLY_UNDIRECTED)
    assert(not directed_graph)
    #endif

    use_reference_nodes = not in_vector(gtype,  {SG});
    if (merge_nodes) cout << "Merging straight and diagonal nodes" << endl;
    if (use_reference_nodes) cout << "Using reference nodes" << endl;
    switch (gtype)
    {
    case DSG:
        gtype_name = "dsg";
        break;
    case DSG_NOT_MERGED:
        gtype_name = "dsgnm";
        break;
    case JP:
        gtype_name = "jp";
        break;
    case SG:
        gtype_name = "sg";
        break;
    case JPD:
        gtype_name = "jpd";
        break;
    }

    base_json_path = "../graphs/" + mapa->benchmark +"/";
    base_npy_path = "../npy_graphs/" + mapa->benchmark + "/";

    json_path = base_json_path  + mapa->name + "_" + gtype_name +".json"; 
    npy_path = base_npy_path + mapa->name + "_" + gtype_name +".npy"; 
    string custom_ending = custom_graph_name.size()>0 ? ("#" + custom_graph_name) : "";
    bid_npy_path = base_npy_path + mapa->name + "_" + gtype_name + "-CH"+ custom_ending+ ".npy"; 

    std::filesystem::create_directories(base_json_path);
    std::filesystem::create_directories(base_npy_path);    
    if (not (debug_connection and debug_node != -1) and not SG_from_scratch) load_npy();
    if (graph_loaded) cout << "Subgoal graph loaded" << endl;
    else {
        cout << "Subgoal graph not found, defining nodes" << endl;
        auto start_time = set_timer();
        define_nodes();
        sg_build_time += end_timer(start_time);
        cout << "N° nodes: " << n_nodes << endl;
    }
    cout << "Nodes defined" << endl;
};

void SubgoalGraph::define_nodes(){
    node_id = 0;
    is_node.resize(mapa->matrix.size(), 0);
    for (size_t cell_p = 0; cell_p < mapa->matrix.size(); cell_p++)
    {
        if (mapa->is_free(cell_p)){
            detect_corners(cell_p);
            if (!there_is_node) continue;
            if (directed_graph) generate_directed_nodes(cell_p);
            else generate_undirected_nodes(cell_p);
        }
    }
    n_nodes = size();
    search_nodes.resize(n_nodes);
}

void SubgoalGraph::generate_undirected_nodes(PosIndex cell_index){
    nodes_map[cell_index] = sgs.size();
    is_node[cell_index] = 1;
    sgs.push_back(Subgoal(cell_index));
}

void SubgoalGraph::prepare_search(){
    current_dir = 8;
    is_endpoint = true;
}


// Define directed subgoals (or jump points)
void SubgoalGraph::generate_directed_nodes(PosIndex cell_index){
    real_nodes.clear();
    fake_nodes.clear();  
    bool merge_this_node;
    for (size_t d = 0; d < 4; d++)
    {
        obs_dir = 2*d + 1;
        if (dir_gen[d] == 1){
            for (size_t ax = 0; ax < 2; ax++)
            {
                left_right = ax ? RIGHT:LEFT;
                cardinal_dir = dir_offset(obs_dir, left_right*5);
                diagonal_dir = dir_offset(obs_dir, left_right*6);
                merge_this_node = merge_nodes and (cardinal_nodes ? (dir_gen[(d+left_right*2)%4] and not dir_gen[(d+left_right)%4]) :  dir_gen[(d+left_right)%4]);                
                if ((cardinal_nodes and not merge_this_node) or (not cardinal_nodes and merge_this_node)) add_real_and_fake_nodes(cardinal_dir, diagonal_dir);
                else add_real_and_fake_nodes(diagonal_dir, cardinal_dir);
            }
        }
    }
    search_pos_id.resize(sgs.size() + real_nodes.size());
    dir_indexes.resize(dir_indexes.size() + real_nodes.size());
    sgs.resize(sgs.size() + real_nodes.size());
    for (auto [node_dir, node_id]: real_nodes){
        nodes_map[umap_index(cell_index, node_dir)] = node_id;
        is_node[cell_index] += 1 << node_dir;
        sgs[node_id] = Subgoal(cell_index);
        dir_indexes[node_id] = node_dir;
        search_pos_id[node_id] = n_pos;
    }
    if (use_reference_nodes){
        for (auto [node_dir, node_id]: fake_nodes){
            nodes_map[umap_index(cell_index, node_dir)] = node_id;
            is_node[cell_index] += 1 << node_dir;
        }
    }
    if (CH_directed) n_nodes_same_pos.push_back(real_nodes.size());
    n_pos ++;
}


// Detect which neighbors are convex corners
void SubgoalGraph::detect_corners(PosIndex cell_index){
    dir_gen.clear();
    dir_gen.resize(4, false);
    there_is_node = false;
    for (size_t d = 0; d < 4; d++)
    {
        dir_index = (d*2)+1;
        move_p = cell_index + dirs.dindex[dir_index];
        if (mapa->is_inside(cell_index) and mapa->is_obstacle(move_p)){
            card1_p = cell_index + dirs.dindex[(dir_index+LEFT)&7];
            card2_p = cell_index + dirs.dindex[(dir_index+RIGHT)&7];
            // If diag is inside, cards are also inside
            if (mapa->is_free(card1_p) and mapa->is_free(card2_p))
            {
                dir_gen[d] = true;
                there_is_node = true;
            }   
        }
    }
}


// Add real and fake (referenced) nodes to the subgoal graph.
void SubgoalGraph::add_real_and_fake_nodes(Dir real_dir, Dir fake_dir){
    NodeId current_id;
    if (real_nodes.count(real_dir) == 0){
        current_id = node_id;
        node_id ++;
        // Add real node
        real_nodes[real_dir] = current_id;
    } else{
        current_id = real_nodes[real_dir];
    }
    // Add fake node;
    if (fake_nodes.count(fake_dir) == 1 and current_id != fake_nodes[fake_dir])
    {
        fake_nodes[fake_dir] = (NodeId) -1;
        
    } else fake_nodes[fake_dir] = current_id;
}


// Remove edges that can be represented by two other edges. It is only usefull in JP and JPD
int SubgoalGraph::remove_redundant_edges(){
    // Removing redundant edges
    dijkstra_redundant = new Search(&sgs, &search_nodes, &search_edges, (int) DIJKSTRA, (int) DIJKSTRA_VANILLA);
    int total_removed = 0, removing_current;
    vector<EdgeId>to_remove;
    vector<EdgeId>new_first_edge(size());
    vector<int16_t>new_n_edges(size());
    for (size_t i = 0; i < size(); i++)
    {
        removing_current = 0;
        dijkstra_redundant->reset_memory_and_stats();
        dijkstra_redundant->dijkstra_vanilla(i);
        for (EdgeId eid  = search_nodes[i].first_edge; eid < search_nodes[i].last_edge(); eid++)
        {
            if (dijkstra_redundant->parents[search_edges[eid].target] != i){
                removing_current ++;
                to_remove.push_back(eid);
                V2(cout << "Edge to " << search_edges[eid].target <<  " is redundant from " << dijkstra_redundant->parents[search_edges[eid].target] <<  endl);
            } 
        }
        new_first_edge[i] = search_nodes[i].first_edge - total_removed;
        new_n_edges[i] = search_nodes[i].n_edges - removing_current;
        
        V2(if (removing_current > 0){
            cout << i << " has redundant edges" << endl;
            });
        total_removed += removing_current;
    }
    for (size_t i = 0; i < size(); i++)
    {
        search_nodes[i].first_edge = new_first_edge[i];
        search_nodes[i].n_edges = new_n_edges[i];
    }
    vector<Edge> old_search_edges = search_edges;
    search_edges.clear();
    search_edges.reserve(old_search_edges.size() - total_removed);
    int remove_index = 0;
    for (size_t i = 0; i < old_search_edges.size(); i++)
    {
        if (remove_index < to_remove.size() and i == to_remove[remove_index]){
            remove_index ++;
        } else
        {
            search_edges.push_back(old_search_edges[i]);
        }
    }

    if (total_removed > 0 and not in_vector(gtype, {JP, JPD})){
        cin >> placeholder;
    }
    delete dijkstra_redundant;
    n_edges -= total_removed;
    cout << "N redundant edges: " << total_removed << endl;
    cout << "Final SG edges: " << n_edges << endl;
    return total_removed;
}



// Update clearances once they are calculated.
void SubgoalGraph::update_clrs(vector<Clearance*>& clearances_vector){
    switch (gtype)
    {
    case DSG_NOT_MERGED:
        switch (variant)
        {
        case DSG_2_CLR:
            clr_forward = clearances_vector[CLR_DSG_BOTH];
            break;
        case DSG_3_CLR:
            clr_forward = clearances_vector[CLR_DSG_CF];
            clr_card1 = clearances_vector[CLR_DSG_CARD];
            break;
        case DSG_4_CLR:
            clr_forward = clearances_vector[CLR_DSG_CF];
            clr_card1 = clearances_vector[CLR_DSG_CARD_OUT];
            clr_card1 = clearances_vector[CLR_DSG_CARD_IN];
            break;
        }
        break;
    case DSG:
        clr_forward = clearances_vector[CLR_DSG_BOTH];
        break;

    case JP:
        clr_forward = clearances_vector[CLR_JP_FORWARD];
        clr_backward = clearances_vector[CLR_JP_BACKWARD];
        break;
    case JPD:
        clr_forward = clearances_vector[CLR_JPD_FORWARD];
        clr_backward = clearances_vector[CLR_JPD_BACKWARD];
        break;
    case SG:
        clr_forward = clearances_vector[CLR_SG2];
        break;
    }
    if (do_CH) clr_no_nodes = clearances_vector[CLR_NO_NODES];
}


// Performs the connection of a node (from the subgoal graph or a query node)
void SubgoalGraph::connect_node(){ 
    new_node_nbs.resize(0);
    V2(
        cout_node_info = debug_connection and ((debug_node == current_id) or (debug_node == -1 and is_endpoint)) 
    );
    V2(if(cout_node_info) cout << "Connecting node " << current_id << "-------------------------------------------------------------------------" << endl);

    switch (gtype){
        case SG:
            get_reachable_nodes_sg();
            break;
        case DSG:
            dsg_connection();
            break;
        case DSG_NOT_MERGED:
            dsg_connection();
            break;
        case JP:
            if (is_goal) jg_backward_connection();
            else get_reachable_nodes_jg();
            break;
        case JPD:
            if (is_goal) jg_backward_connection();
            else get_reachable_nodes_jg();
            break;
    }

}


// Connect all nodes of G.
void SubgoalGraph::connect(){
    NodeId start, end;
    search_edges.reserve(search_nodes.size()*EXPECTED_EDGES_PER_NODE);
    start = (debug_connection && (debug_node != -1)) ? debug_node:0;
    end = (debug_connection && (debug_node != -1)) ? debug_node+1:size();
    is_goal = is_endpoint = false;
    current_edge = 0;
    V2(cout << "Connecting graph from " << start << " to " << end << endl);
    for (current_id = start; current_id < end; current_id++)
    {  
        node_s = &sgs[current_id];
        if (directed_graph) current_dir = dir_indexes[current_id];

        connect_node();
        update_nbs_current_node();

    }
    n_edges = get_n_edges();
    V2(
    for (size_t i = 0; i < sgs.size(); i++)
    {
        cout << node_info(i, false) << endl;
    });
    
    cout << "N° edges: " << n_edges << endl;
    prepare_search();
}


EdgeId SubgoalGraph::get_n_edges(){
    return search_edges.size();
}


bool SubgoalGraph::is_nb(NodeId n1, NodeId n2){
    for (size_t i =  search_nodes[n1].first_edge; i < search_nodes[n1].last_edge(); i++)
    {
        if (search_edges[i].target == n2) return true;
    }
    return false;
}


NodeId SubgoalGraph::get_n_nodes(){
    return search_nodes.size();
}


// Add query point using CH data structs.
NodeId SubgoalGraph::add_endpoint_CH(const APos& pos, bool _is_goal){
    is_goal = _is_goal;
    endpoint_index = mindex(pos);
    UNDIRECTED(if (is_node[endpoint_index]>0) return nodes_map[endpoint_index]); 
    current_id = sgs.size();
    sgs.emplace_back(endpoint_index);
    bid_search_nodes[is_goal][0].emplace_back(CH_edges.size(), 0);
    DIRECTED(bid_search_nodes[1-is_goal][0].emplace_back(CH_edges.size(), 0));
    node_s = &sgs.back();
    connect_node();
    V2(assert(new_node_nbs.end() == std::unique(new_node_nbs.begin(), new_node_nbs.end())));
    n_nbs= 0;
    for (NodeId nb:new_node_nbs){
        DIRECTED(if (avoid_connect[is_goal] and avoidance_table[is_goal][nb]) continue);
        n_nbs++;        
        UNDIRECTED(undirected_add_edge_CH(nb));
        DIRECTED(directed_add_edge_CH(nb));
        DIRECTED(if (allow_avoidance[1 - is_goal]) avoidance_table[1 - is_goal][nb] = false);
    }
    bid_search_nodes[is_goal][0][current_id].n_edges = n_nbs;

    V2(
        if (cout_node_info){
            cout << bid_node_info(current_id) << endl;
            cin >> placeholder;
        }
    );
    return current_id;
}


// Add query point to the subgol graph.
NodeId SubgoalGraph::add_endpoint(const APos& pos, bool _is_goal){
    is_goal = _is_goal;
    endpoint_index = mindex(pos);
    UNDIRECTED(if (is_node[endpoint_index]>0) return nodes_map[endpoint_index]); 
    current_id = sgs.size();
    DIRECTED(
        endpoint_pos_index = n_pos;
        n_pos ++;
        search_pos_id.push_back(endpoint_pos_index); 
        dir_indexes.push_back(8);
    );
    sgs.emplace_back(endpoint_index);
    if (do_CH){
        bid_search_nodes[is_goal][0].emplace_back(CH_edges.size(), 0);
        // Empty node for filling purposes.
        DIRECTED(bid_search_nodes[1-is_goal][0].emplace_back(CH_edges.size(), 0));
    }else{
        search_nodes.emplace_back(search_edges.size());
    }

    node_s = &sgs.back();
    connect_node();
    V2(assert(new_node_nbs.end() == std::unique(new_node_nbs.begin(), new_node_nbs.end())));
    // EdgeCost ecost;
    n_nbs= 0;
    for (NodeId nb:new_node_nbs){
        DIRECTED(if (avoid_connect[is_goal] and avoidance_table[is_goal][nb]) continue);
        n_nbs++;
        if (do_CH){
            UNDIRECTED(undirected_add_edge_CH(nb));
            DIRECTED(directed_add_edge_CH(nb));
        }
        else add_edge(nb);
        // add_edge_CH(nb);
        DIRECTED(if (allow_avoidance[1 - is_goal]) avoidance_table[1 - is_goal][nb] = false);
    }
    
    if (do_CH){
        bid_search_nodes[is_goal][0][current_id].n_edges = n_nbs;
    } else{
        search_nodes[current_id].n_edges = n_nbs;
    }

    V2(
        if (cout_node_info){
            if (do_CH) cout << bid_node_info(current_id) << endl;
            else cout << node_info(current_id) << endl;
            cin >> placeholder;
        }
    );
    return current_id;
}


string SubgoalGraph::edges_info(){
    string s = "";
    int i = 0;
    for (Edge& edge:search_edges){
        s += std::to_string(i) + ": " + edge.info() + endl;
        i++;
    }
    return s;
}


// Update edges for the current node (last added node)
void SubgoalGraph::update_nbs_current_node(){
    EdgeId first_edge = current_edge;
    EdgeId last_edge = current_edge + new_node_nbs.size();
    search_nodes[current_id].first_edge = first_edge;
    search_nodes[current_id].n_edges = new_node_nbs.size();
    search_edges.resize(last_edge);

    for (size_t i = 0; i < new_node_nbs.size(); i++)
    {
        search_edges[current_edge] = Edge(new_node_nbs[i]);
        current_edge ++;
    }
    V2(
        if (cout_node_info){
            
            cout << node_info(current_id) << endl;
            cin >> placeholder;
        }
    );
}


// Remove last node from the graph when using CH. This assumes that goal is added after the start and removed before the start.
void SubgoalGraph::remove_last_node_CH(bool remove_goal){
    actual_node = &bid_search_nodes[remove_goal][0].back();
    for (EdgeId eid = actual_node->first_edge; eid < actual_node->last_edge(); eid++)
    {
        nb_id = CH_edges[eid].targets[remove_goal];
        DIRECTED(if (allow_avoidance[1- remove_goal]) avoidance_table[1- remove_goal][nb_id] = is_base_avoidable(1- remove_goal, nb_id));   
    }
    sgs.pop_back();
    CH_edges.resize(actual_node->first_edge);
    bid_search_nodes[remove_goal][0].pop_back();
    DIRECTED(bid_search_nodes[1 - remove_goal][0].pop_back());
}


// Remove last node from the graph. This assumes that goal is added after the start and removed before the start.
void SubgoalGraph::remove_last_node(bool remove_goal){
    actual_node = &search_nodes.back(); 
    for (EdgeId eid = actual_node->first_edge; eid < actual_node->last_edge(); eid++)
    {
        nb_id = search_edges[eid].target;
        if (remove_goal) search_nodes[nb_id].n_edges--;  
        DIRECTED(if (allow_avoidance[1- remove_goal]) avoidance_table[1- remove_goal][nb_id] = is_base_avoidable(1- remove_goal, nb_id));   
    }
    sgs.pop_back();
    search_edges.resize(actual_node->first_edge);
    search_nodes.pop_back();
    DIRECTED(
        search_pos_id.pop_back();
        n_pos--;
    );
}


// Load avoidance status of all subgoals.
void SubgoalGraph::check_avoidance(const nlohmann::json& avoidance_params){
    if (!directed_graph){
        for (size_t i = 0; i < 2; i++)
        {
            allow_avoidance[i] = false;
            avoid_search[i] = false;
            avoid_connect[i] =false;
            n_avoid_nodes[0] = 0;
            n_avoid_nodes[1] = 0;
        }
        return;
    }
    read_avoidance_params(avoidance_params);
    load_avoidance_table();

    
}


// Read avoidance parameters
void SubgoalGraph::read_avoidance_params(const nlohmann::json& avoidance_params){
    avoid_th[FORWARD] = avoidance_params["forward"]["th"];
    avoid_th[BACKWARD] = avoidance_params["backward"]["th"];
    avoid_search[FORWARD] = avoidance_params["forward"]["search"];
    avoid_search[BACKWARD] = avoidance_params["backward"]["search"];
    avoid_connect[FORWARD] = avoidance_params["forward"]["connect"];
    avoid_connect[BACKWARD] = avoidance_params["backward"]["connect"];

}


// Comparison used for sorting edges.
bool pair_compare(const std::pair<NodeId, NodeId>& pr1, const std::pair<NodeId, NodeId>& pr2){
    return pr1.first < pr2.first or (pr1.first == pr2.first and pr1.second < pr2.second);
}


// Build edges of the reversed graph
void SubgoalGraph::build_reversed_edges(){
    vector<std::pair<NodeId, NodeId>> edges_pairs(search_edges.size());
    int n_edge = 0;
    for (size_t i = 0; i < search_nodes.size(); i++)
    {
        for (size_t j = search_nodes[i].first_edge; j < search_nodes[i].last_edge(); j++)
        {
            
            edges_pairs[n_edge].first = search_edges[j].target;
            edges_pairs[n_edge].second = i;

            n_edge++;
        }
    }
    std::sort(edges_pairs.begin(), edges_pairs.end(), pair_compare);

    rsearch_nodes.resize(sgs.size());
    rsearch_edges.resize(search_edges.size());
    EdgeId ce = 0;
    for (size_t cn = 0; cn < search_nodes.size(); cn++)
    {
        rsearch_nodes[cn].first_edge = ce;
        while (ce < edges_pairs.size() and  edges_pairs[ce].first == cn){
            rsearch_edges[ce].target = edges_pairs[ce].second;
            ce ++;
        }
        rsearch_nodes[cn].n_edges = ce - rsearch_nodes[cn].first_edge;
    }
}


// Compute avoidance status of all subgoals.
void SubgoalGraph::load_avoidance_table(){

    if (not CH_loaded){
        n_avoid_nodes.fill(0);
        avoidance_table[FORWARD].resize(size() + 2, false);
        avoidance_table[BACKWARD].resize(size() + 2, true);
        base_avoidable[FORWARD].resize(size() + 2, false);
        base_avoidable[BACKWARD].resize(size() + 2, false);
        for (size_t i = 0; i < size(); i++)
        {
            if (search_nodes[i].n_edges == 0){
                avoidance_table[FORWARD][i] = true;
                base_avoidable[FORWARD][i] = true;
                n_avoid_nodes[FORWARD]++;
            } 
            for (EdgeId eid = search_nodes[i].first_edge; eid <search_nodes[i].last_edge(); eid++){
                avoidance_table[BACKWARD][search_edges[eid].target] = false;
            }
        }
        for (size_t i = 0; i < size(); i++)
        {
            if (avoidance_table[BACKWARD][i]) n_avoid_nodes[BACKWARD]++;
            base_avoidable[BACKWARD][i] = avoidance_table[BACKWARD][i];
        }
        avoidance_table[BACKWARD][size()] = false;
        avoidance_table[BACKWARD][size()+1] = false;

        // Procedure for detecting not avoidable nodes that are connected to only avoidable nodes
        // Therefore, partiallly avoidable nodes 
        if (false){
            int n, total = 0;
            bool ind_avoid ;
            vector<NodeId> new_avoidable;
            while (true){
                n=0;
                for (size_t i = 0; i < sgs.size(); i++)
                {
                    ind_avoid = true;
                    if (base_avoidable[FORWARD][i]) ind_avoid = false;
                    else{
                        for (size_t j = search_nodes[i].first_edge; j < search_nodes[i].last_edge(); j++)
                        {
                            if (not base_avoidable[FORWARD][search_edges[j].target]) {
                                ind_avoid = false;
                                break;
                            }
                        }
                        if (ind_avoid) {
                            new_avoidable.push_back(i);
                            n++;
                        }
                    }   
                }
                if (new_avoidable.size() == 0){
                    cout << "No more avoidable nodes" << endl;
                    break;
                }
                for (NodeId nid:new_avoidable){
                    base_avoidable[FORWARD][nid] = true;
                }
                total += new_avoidable.size();
                new_avoidable.clear();
                cout << "Indirect avoidance: " << n << endl;
            }
            build_reversed_edges();

            total = 0;
            new_avoidable.clear();

            while (true){
                n=0;
                for (size_t i = 0; i < sgs.size(); i++)
                {
                    ind_avoid = true;
                    if (base_avoidable[BACKWARD][i]) ind_avoid = false;
                    else{
                        for (size_t j = rsearch_nodes[i].first_edge; j < rsearch_nodes[i].last_edge(); j++)
                        {
                            if (not base_avoidable[BACKWARD][rsearch_edges[j].target]) {
                                ind_avoid = false;
                                break;
                            }
                        }
                        if (ind_avoid) {
                            new_avoidable.push_back(i);
                            n++;
                        }
                    }   
                }
                if (new_avoidable.size() == 0){
                    cout << "NO more avoidablenodes" << endl;
                    break;
                }
                for (NodeId nid:new_avoidable){
                    base_avoidable[BACKWARD][nid] = true;
                }
                total += new_avoidable.size();
                new_avoidable.clear();
                cout << "Indirect avoidance: " << n << endl;
            }

        }

    }
    array<float, 2> ratios;
    for (size_t i = 0; i < 2; i++)
    {
        cout << (i==FORWARD? "Forward" : "Backward") << endl;
        ratios[i] = divfloat<float>(n_avoid_nodes[i], size());
        allow_avoidance[i] = (ratios[i] > avoid_th[i]);
        avoid_search[i] = (allow_avoidance[i] && avoid_search[i]);
        avoid_connect[i] = (allow_avoidance[i] && avoid_connect[i]);
        cout << "Avoidable ratio: " <<   ratios[i] << " (" << n_avoid_nodes[i] << " of " << size() << ") " << endl;
        cout << "Threshold is " << avoid_th[i] << (allow_avoidance[i] ? ", avoidance allowed" : ", avoidance not allowed")  << endl;
    }
    cout << "Avoid search: " << v2s(avoid_search) << endl;
    cout << "Avoid connect: " << v2s(avoid_connect) << endl;
}   
vector<Edge>::iterator SubgoalGraph::first_edge_it(NodeId node_id){
    return search_edges.begin() + search_nodes[node_id].first_edge;
}
vector<Edge>::iterator SubgoalGraph::last_edge_it(NodeId node_id){
    return search_edges.begin() + search_nodes[node_id].last_edge();
}

vector<NodeId> SubgoalGraph::get_edges(NodeId node_id){
    vector<NodeId> nbs;
    for (auto it = first_edge_it(node_id); it < last_edge_it(node_id); it++)
    {
        nbs.push_back(it->target);
    }
    return nbs;
}



