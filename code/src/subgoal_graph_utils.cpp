#include "subgoal_graph.hpp"


/* 
Subgoal graph utils
Used to save and load subgoal graph, using npy and json file formats.
 */


void SubgoalGraph::to_json(){
    if (do_CH) return;
    vector<PosIndex> nodes_indexes;
    vector<EdgeId> first_edges;
    vector<EdgeId> node_n_edges;
    for (size_t i = 0; i < sgs.size(); i++)
    {
        nodes_indexes.push_back(sgs[i].mindex);
        first_edges.push_back(search_nodes[i].first_edge);
        node_n_edges.push_back(search_nodes[i].n_edges);
    }
    vector<NodeId> edges_targets;
    for (auto edge: search_edges){
        edges_targets.push_back(edge.target == -1 ? 0: edge.target);
    }
    nlohmann::json graph_json;
    graph_json["dir_index"] = dir_indexes;
    graph_json["node_index"] = nodes_indexes;
    graph_json["first_edges"] = first_edges;
    graph_json["node_n_edges"] = node_n_edges;
    graph_json["edges"] = edges_targets;
    graph_json["map_size"] = mapa->size;
    graph_json["n_nodes"] = n_nodes;
    graph_json["n_edges"] = n_edges;
    save_json(graph_json, json_path);
}


// Load preprocessing stats from npy.
void SubgoalGraph::load_stats_from_npy(){
    sg_build_time  = npy_zip["SG_PT"].data<uint>()[0];
    if (CH_loaded){
        CH_stats["SC"] = npy_zip["SC"].as_vec<int>()[0];
        CH_stats["E"]  = npy_zip["E"].as_vec<int>()[0];
        n_edges = CH_stats["E"].get<int>();
        CH_stats["ECH"]  = npy_zip["ECH"].as_vec<int>()[0];
        CH_stats["EF"]  = npy_zip["EF"].as_vec<int>()[0];
        CH_stats["RED"]  = npy_zip["RED"].as_vec<int>()[0];
        CH_stats["NDF"]  = npy_zip["NDF"].as_vec<int>()[0];
        CH_stats["NCF"]  = npy_zip["NCF"].as_vec<int>()[0];
        CH_build_time  = npy_zip["CH_PT"].data<uint>()[0];
        cout << "N original edges: " << CH_stats["E"] << endl;
        cout << "N added shortcuts: " << CH_stats["SC"]  << endl;
        cout << "N CH Edges: " << CH_stats["ECH"]  << endl;
        cout << "N redundants edges: " << CH_stats["RED"] << endl;
        cout << "N DIAGONAL_FIRST: " << CH_stats["NDF"] << endl;
        cout << "N CARDINAL_FIRST: " << CH_stats["NCF"] << endl;
    }
}


// Load avoidance status. When using CH, the avoidable status of each node can be lost.
void SubgoalGraph::load_avoidance_from_npy(){
    if (!directed_graph){
        n_avoid_nodes[0] = 0;
        n_avoid_nodes[1] = 0; 
        return;
    }
    vector<NodeId> navoid = npy_zip["n_avoidable"].as_vec<NodeId>();
    n_avoid_nodes[0] = navoid[0];
    n_avoid_nodes[1] = navoid[1];
    cout << "N avoid: " << v2s(n_avoid_nodes) << endl;
    if (n_avoid_nodes[0] > 0 ){
        avoidance_table[FORWARD] = npy_zip["avoidance_fw"].as_vec<Bool>();
        base_avoidable[0] =  vector<bool>(sgs.size());
        for (size_t i = 0; i < sgs.size(); i++)
        {
            base_avoidable[FORWARD][i] = avoidance_table[FORWARD][i];
        }
    }
    if (n_avoid_nodes[1] > 0){
        avoidance_table[BACKWARD] = npy_zip["avoidance_bw"].as_vec<Bool>();
        base_avoidable[1] =  vector<bool>(sgs.size());
        for (size_t i = 0; i < sgs.size(); i++)
        {
            base_avoidable[BACKWARD][i] = avoidance_table[BACKWARD][i];
        }
    }
}


// Load subgoals, position, direction and edges.
void SubgoalGraph::load_subgoals_from_npy(){
    is_node.resize(mapa->matrix.size(), 0);
    vector<IsNodeDir> map_dirs;
    vector<PosIndex> OR_nodes;
    Dir ndir;
    vector<Dir> dirs_present;
    if(directed_graph) {
        map_dirs = npy_zip["map_dirs"].as_vec<IsNodeDir>();
        OR_nodes = npy_zip["OR_nodes"].as_vec<PosIndex>();
    }
    vector<PosIndex> nodes_indexes = npy_zip["nodes_indexes"].as_vec<PosIndex>();
    PosIndex nindex, last_nindex;
    n_nodes = nodes_indexes.size();
    cout << "N° nodes: " << n_nodes << endl;
    if (not CH_loaded and directed_graph) search_pos_id.resize(n_nodes);
    if (not CH_loaded and CH_directed) n_nodes_same_pos.resize(n_nodes, 0);
    sgs.reserve(n_nodes);
    if (directed_graph){
        last_nindex = nodes_indexes[0];
        n_pos = 0;
        dir_indexes.reserve(n_nodes);
        for (size_t i = 0; i < n_nodes; i++){
            nindex = nodes_indexes[i]/8;
            ndir = nodes_indexes[i]%8;
            sgs.emplace_back(nindex);
            dir_indexes.emplace_back(ndir);
            // Nodes map and reference nodes
            nodes_map[umap_index(nindex, ndir)] = i;
            is_node[nindex] += map_dirs[i] + (1 << ndir);
            dirs_presence(map_dirs[i], dirs_present);
            for (Dir dir: dirs_present) nodes_map[umap_index(nindex, dir)] = i;
            // Positions
            if (last_nindex != nindex) n_pos ++;
            if (n_nodes_same_pos.size() > 0) n_nodes_same_pos[n_pos] ++;
            if (search_pos_id.size() > 0) search_pos_id[i] = n_pos;
            last_nindex = nindex;
        }
        n_pos ++;
        if (n_nodes_same_pos.size()>0) {
            n_nodes_same_pos.resize(n_pos);
        }
    } 
    else 
        for (size_t i = 0; i < n_nodes; i++){
            nindex = nodes_indexes[i];
            sgs.emplace_back(nindex);
            nodes_map[nindex] = i;
            is_node[nindex] = 1;
        }
    // Overreference nodes
    for (PosIndex pos_index: OR_nodes){
        nindex = pos_index/8;
        ndir = pos_index%8;
        is_node[nindex] += 1 << ndir;
        nodes_map[pos_index] = -1;
    }
}


// Load nodes (edges, not position or direction)
void SubgoalGraph::load_nodes_from_npy(vector<Node>*& nodes, string suffix){
    if (CH_loaded) nodes = new vector<Node>();
    nodes->reserve(n_nodes);
    vector<EdgeId> first_edges = npy_zip["first_edge" + suffix].as_vec<EdgeId>();
    vector<uint16_t> node_n_edges = npy_zip["n_edges" + suffix].as_vec<uint16_t>();
    for (size_t i = 0; i < n_nodes; i++)
    {
        nodes->emplace_back(first_edges[i], node_n_edges[i]);
    }
}


// Load subgoal graph edges (not CH)
void SubgoalGraph::load_base_edges_from_npy(){
    vector<NodeId> targets;
    targets = npy_zip["edges_targets"].as_vec<NodeId>();            
    n_edges = targets.size();
    search_edges.reserve(n_edges);
    for (size_t i = 0; i < n_edges; i++)
    {
        search_edges.emplace_back(targets[i]);
    }
    cout << "N° edges " << n_edges << endl;
}


// Load CH edges
void SubgoalGraph::load_CH_edges_from_npy(){
    vector<NodeId> sources, targets;
    vector<EdgeId> zips, e1s, e2s;
    vector<float> costs;
    targets = npy_zip["targets"].as_vec<NodeId>();            
    sources = npy_zip["sources"].as_vec<NodeId>(); 
    zips = npy_zip["zips"].as_vec<EdgeId>();
    costs = npy_zip["costs"].as_vec<float>();
    e1s = npy_zip["e1s"].as_vec<EdgeId>();
    e2s = npy_zip["e2s"].as_vec<EdgeId>();

    EdgeCost h_cost;
    CH_edges.reserve(targets.size());
    for (size_t i = 0; i < targets.size(); i++)
    {
        if (zips[i] <= NO_BASIC_REACHABILITY){
            CH_edges.emplace_back(sources[i], targets[i], costs[zips[i]], e1s[zips[i]], e2s[zips[i]]);  
        }
        else{
            if (zips[i] == DIAGONAL_FIRST or zips[i] == CARDINAL_FIRST){
                h_cost = octile_distance<float>(sgs[sources[i]].get_pos(), sgs[targets[i]].get_pos());
            } else if (zips[i] == SQUARE_0 or zips[i] == SQUARE_1){
                h_cost = manhattan_distance<float>(sgs[sources[i]].get_pos(), sgs[targets[i]].get_pos());
            }
            CH_edges.emplace_back(sources[i], targets[i], h_cost, zips[i], zips[i]);
        }
        
    }
}


// Load a subgoal graph (without CH) from a .npy file
void SubgoalGraph::load_npy(){
    graph_loaded = false;
    CH_loaded = (do_CH and not CH_from_scratch and file_exists(bid_npy_path));
    if (CH_loaded) graph_loaded = true;
    else {
        if (not file_exists(npy_path)) return;
        graph_loaded = true;
    }
    cout << (CH_loaded ? "CH loaded" : "CH not found") << endl;
    npy_zip = cnpy::npz_load(CH_loaded ? bid_npy_path:npy_path);
    load_stats_from_npy();
    load_subgoals_from_npy();
    // Nodes
    if (CH_loaded){
        bid_search_nodes[FORWARD] = nullptr;
        load_nodes_from_npy(bid_search_nodes[FORWARD], "_fw");
        if (directed_graph) load_nodes_from_npy(bid_search_nodes[BACKWARD], "_bw");
        else bid_search_nodes[BACKWARD] = bid_search_nodes[FORWARD];
    }
    else{
        vector<Node>* ptr = &search_nodes;
        load_nodes_from_npy(ptr, "");
    } 
    // Edges
    if (CH_loaded){
        load_CH_edges_from_npy();
        load_avoidance_from_npy();
    } 
    else load_base_edges_from_npy();
    prepare_search();
    return;
}


// Save subgoal information (only position and direction)
void SubgoalGraph::save_subgoals(string mode){
    vector<PosIndex> nodes_indexes(search_nodes.size());
    for (size_t i = 0; i < n_nodes; i++)
    {
        nodes_indexes[i] = directed_graph ? sgs[i].mindex*8 + dir_indexes[i] : sgs[i].mindex;
    }
    cnpy::npz_save<PosIndex>(npy_path, "nodes_indexes" , nodes_indexes, mode);

}


// Save nodes information, i.e., first edge and number of edges (not subgoal position or direction)
void SubgoalGraph::save_nodes(vector<Node>& nodes, string suffix, string mode){
    vector<EdgeId> first_edges(n_nodes);
    vector<uint16_t> node_n_edges(n_nodes);
    for (size_t i = 0; i < n_nodes; i++)
    {
        first_edges[i] = nodes[i].first_edge;
        node_n_edges[i] = nodes[i].n_edges;
    }
    cnpy::npz_save<EdgeId>(npy_path, "first_edge" + suffix, first_edges, mode);
    cnpy::npz_save<uint16_t>(npy_path, "n_edges" + suffix, node_n_edges, mode);
}


// Save base edges (not shortcuts)
void SubgoalGraph::save_base_edges(string mode){
    vector<NodeId> edges_targets(search_edges.size());
    for (size_t i = 0; i < search_edges.size(); i++) edges_targets[i] = search_edges[i].target;
    cnpy::npz_save<NodeId>(npy_path, "edges_targets", edges_targets, mode);
}


// Save reference (fake) nodes 
void SubgoalGraph::save_referenced_nodes(string mode){
    if (directed_graph){
        PosIndex node_pi;
        IsNodeDir referred_from;
        Dir referred_from_dir;
        vector<IsNodeDir> map_dirs(sgs.size());
        vector<PosIndex> OR_nodes;
        std::unordered_set<PosIndex> overreferenced_nodes_set;
        robin_hood::unordered_node_map<PosIndex, NodeId>::iterator it;
        for (size_t i = 0; i < sgs.size(); i++)
        {
            node_pi = sgs[i].mindex;
            referred_from = 0;
            for (int lr = RIGHT; lr <= LEFT; lr+=2)
            {
                referred_from_dir = dir_offset(dir_indexes[i], lr);
                it =nodes_map.find(umap_index(node_pi, referred_from_dir)); 
                if (it != nodes_map.end()){
                    if (it->second == -1){
                        overreferenced_nodes_set.insert(umap_index(node_pi, referred_from_dir));
                    } 
                    else if (it->second == i){
                        referred_from += 1 << referred_from_dir;
                    }
                } 
            }            
            map_dirs[i] = referred_from;
        }
        for (PosIndex pos_index :overreferenced_nodes_set){
            OR_nodes.push_back(pos_index);
        }
        cnpy::npz_save<IsNodeDir>(npy_path, "map_dirs", map_dirs, mode);
        cnpy::npz_save<PosIndex>(npy_path, "OR_nodes", OR_nodes, mode);
    }
}


// Save preprocessing time
void SubgoalGraph::save_preprocessing_time(string mode){
    cnpy::npz_save<uint>(npy_path, "SG_PT", &sg_build_time, {1}, mode);
}


// Save subgoal graph to npy
void SubgoalGraph::to_npy(){
    save_preprocessing_time("w");
    save_subgoals("a");
    save_nodes(search_nodes, "", "a");
    save_base_edges("a");
    save_referenced_nodes("a");
}


// Save subgoal graph preprocessing stats.
void SubgoalGraph::save_CH_SG_stats(string mode){
    int shortcuts = CH_stats["SC"].get<int>();
    int n_edges  = CH_stats["E"].get<int>();
    int n_edges_CH  = CH_stats["ECH"].get<int>();
    int edges_fw  = CH_stats["EF"].get<int>();
    int n_red_egdes  = CH_stats["RED"].get<int>();
    int n_df_edges  = CH_stats["NDF"].get<int>();
    int n_cf_edges  = CH_stats["NCF"].get<int>();
    cnpy::npz_save(npy_path, "SC", &shortcuts, {1}, mode);
    cnpy::npz_save(npy_path, "E", &n_edges, {1}, mode);
    cnpy::npz_save(npy_path, "ECH", &n_edges_CH, {1}, mode);
    cnpy::npz_save(npy_path, "EF", &edges_fw, {1}, mode);
    cnpy::npz_save(npy_path, "RED", &n_red_egdes, {1}, mode);
    cnpy::npz_save(npy_path, "NDF", &n_df_edges, {1}, mode);
    cnpy::npz_save(npy_path, "NCF", &n_cf_edges, {1}, mode);
    cnpy::npz_save<uint>(npy_path, "CH_PT", &CH_build_time, {1}, mode);
}


// Save CH edges into a numpy dictionary.
void SubgoalGraph::save_CH_edges(string mode){
    int total_edges = CH_edges.size();
    vector<NodeId> sources(total_edges);
    vector<NodeId> targets(total_edges);
    vector<EdgeId> zips(total_edges);
    int zip_id = 0;
    int n_comp = n_compressed;
    vector<float> costs(n_comp);
    vector<EdgeId> e1s(n_comp);
    vector<EdgeId> e2s(n_comp); 
    for (size_t i = 0; i < total_edges; i++)
    {
        sources[i] = CH_edges[i].targets[BACKWARD];
        targets[i] = CH_edges[i].targets[FORWARD];
        if (CH_edges[i].e1 <= NO_BASIC_REACHABILITY){
            costs[zip_id] = CH_edges[i].cost;
            e1s[zip_id] = CH_edges[i].e1;
            e2s[zip_id] = CH_edges[i].e2;
            zips[i] =  zip_id;
            zip_id++;
        } else{
            zips[i] =  CH_edges[i].e1;
        }

    }
    cnpy::npz_save<NodeId>(npy_path, "targets", targets , mode);
    cnpy::npz_save<NodeId>(npy_path, "sources", sources , mode);
    cnpy::npz_save<EdgeId>(npy_path, "zips", zips , mode);
    cnpy::npz_save<EdgeId>(npy_path, "e1s", e1s , mode);
    cnpy::npz_save<EdgeId>(npy_path, "e2s", e2s , mode);
    cnpy::npz_save<float>(npy_path, "costs", costs , mode);  
}


// Save avoidance info
void SubgoalGraph::save_avoidance(string mode){
    cnpy::npz_save<NodeId>(npy_path, "n_avoidable", n_avoid_nodes.data(), vector<size_t>{2} , mode);
    cnpy::npz_save<uint8_t>(npy_path, "avoidance_fw", avoidance_table[FORWARD] , mode);
    cnpy::npz_save<uint8_t>(npy_path, "avoidance_bw", avoidance_table[BACKWARD] , mode);
}


// Save bidirectional graph (CH) to npy
void SubgoalGraph::bid_to_npy(){
    npy_path = bid_npy_path;
    save_preprocessing_time("w");
    save_CH_SG_stats("a");
    save_subgoals("a");
    save_referenced_nodes("a");
    save_nodes(bid_search_nodes[FORWARD][0], "_fw", "a");
    if (directed_graph) save_nodes(bid_search_nodes[BACKWARD][0], "_bw", "a");
    save_CH_edges("a");
    if (directed_graph) save_avoidance("a");
}

