#include "subgoal_graph.hpp"

/* 
Connection algorithms for Jump Point Graphs (JP)
*/

// Determine if a cardinal node must perform connection in a given quadrant.
void SubgoalGraph::check_quadrants(){
    diag_dir = dir_offset(current_dir, 3*RIGHT);
    final_index = jump_index + dirs.dindex[diag_dir];
    if (mapa->is_obstacle(final_index)){
        first_quadrant = dir_offset(current_dir, RIGHT);
        card_dir = dir_offset(current_dir, 2*RIGHT);
        cardinal_scan(jump_index);
    } else first_quadrant = dir_offset(current_dir, LEFT);

    diag_dir = dir_offset(current_dir, 3*LEFT);
    final_index = jump_index + dirs.dindex[diag_dir];
    if (mapa->is_obstacle(final_index)){
        last_quadrant = dir_offset(current_dir, LEFT);
        card_dir = dir_offset(current_dir, 2*LEFT);
        cardinal_scan(jump_index);
    } else last_quadrant = dir_offset(current_dir, RIGHT);
    if (last_quadrant < first_quadrant) last_quadrant += 8;
}


// JP cardinal connection
void SubgoalGraph::jg_cardinal_connection(){
    V2(if (debug_connection) cout << "Cardinal connections" << endl);
    if (is_endpoint)
    {
        for (size_t i = 0; i < 4; i++)
        {
            card_dir = i*2;
            V2(if (debug_connection) cout << "Cardinal connection in: " <<dirs.names[card_dir] << endl);
            cardinal_scan(jump_index);
        }
        first_quadrant = 1;
        last_quadrant = 7;
        
    }
    else if (is_cardinal(current_dir)){
        card_dir = current_dir;
        cardinal_scan(jump_index);
        check_quadrants();

    } else{
        card_dir = dir_offset(current_dir,LEFT);
        cardinal_scan(jump_index);
        card_dir = dir_offset(current_dir, RIGHT);
        cardinal_scan(jump_index);
        first_quadrant = current_dir;
        last_quadrant = current_dir;
    }
}


// JP forward connect. (For jump points and query nodes)
void SubgoalGraph::get_reachable_nodes_jg(){
    V2(if (debug_connection) cout << "Connecting node " << subgoal_info(current_id) << endl);
    jump_index = node_s->mindex;
    jg_cardinal_connection();
    for (int iter = first_quadrant; iter <= last_quadrant; iter+=2)
    {
        diag_dir = iter&7;
        jump_index = node_s->mindex;
        clr = clr_forward->get_clr(jump_index, diag_dir);
        V2(if (debug_connection) cout << "Diagonal first scan in " << dirs.names[diag_dir] << " clr " << (int) clr << endl);
        jg_forward_loop();
    }
}


// Main diagonal first scan.
void SubgoalGraph::jg_forward_loop(){
    while(clr > 0){
        jump_index += dirs.dindex[diag_dir]*clr;
        V2(if (debug_connection) cout << "Jumping at: " << v2s(i2p(jump_index)) << endl);

        #if defined(VERBOSE2)
        if (gtype == JPD and jg_single_connection(diag_dir, jump_index)) {
            V2( if (debug_connection) cout << "Breaking, node in diagonal" << endl);
            break;
        }
        #elif defined(ONLY_MERGED)
        if (jg_single_connection(diag_dir, jump_index)) {
            V2( if (debug_connection) cout << "Breaking, node in diagonal" << endl);
            break;
        }
        #endif

        card_dir = dir_offset(diag_dir, LEFT);
        cardinal_scan(jump_index);
        card_dir = dir_offset(diag_dir, RIGHT);
        cardinal_scan(jump_index);

        clr = clr_forward->get_clr(jump_index, diag_dir);
        
    }
}


// Target diagonal connection
void SubgoalGraph::jg_diagonal_connection(){
    clr = clr_backward->get_clr(jump_index, diag_dir);
    if (clr >0){
        final_index = jump_index +dirs.dindex[diag_dir]*clr;
        opposite_diag_dir = dir_offset(diag_dir, 4);
        V2(if(debug_connection) cout << "Scanning diagonal: " << dirs.names[diag_dir] << " clr: " << clr << " Reaching cell " << v2s(i2p(final_index)) << endl);
        #if defined(VERBOSE2)
        if (gtype == JPD) jg_single_diagonal_connection();
        else jg_loop_diagonal_connection();
        #elif defined(ONLY_MERGED)
        jg_single_diagonal_connection();
        #else
        jg_loop_diagonal_connection();
        #endif
    }
}


// JP Backward connect
void SubgoalGraph::jg_backward_connection(){
    #if defined(ONLY_MERGED) or defined(VERBOSE2)
    last_connection = (NodeId) -1;
    #endif
    if(debug_connection) cout << "Goal Connection " << v2s(node_s->get_pos()) << " ------------------------------- " << endl;
    jg_all_diagonals_connection();
    
    if(debug_connection) cout << "Iteration through cardinal-----------------------" << endl;
    for (card_dir = 0; card_dir < 8; card_dir += 2)
    {
        opposite_dir = dir_offset(card_dir, 4);
        left_dir = dir_offset(card_dir, 2);
        obs_dir_left = dir_offset(card_dir, 5);
        right_dir = dir_offset(card_dir, -2);
        obs_dir_right = dir_offset(card_dir, -5);
        jump_index = node_s->mindex;
        main_clr = clr_backward->get_clr(jump_index, card_dir);
        if(debug_connection) cout << "Iteration through cardinal "<< dirs.names[card_dir] << " clr " << (int) main_clr  << endl;
        inc_left = false;
        inc_right = false;
        connect_cardinal = true;
        while (main_clr and connect_cardinal){
            jump_index = jump_index + dirs.dindex[card_dir]*main_clr;
            which = is_node[jump_index];

            #if defined(VERBOSE2)
            if (gtype == JPD) cell_inc_right = cell_inc_left = false;
            #elif defined(ONLY_MERGED)
            cell_inc_right = cell_inc_left = false;
            #endif
            if(debug_connection) cout << "Jumping at: " << v2s(i2p(jump_index)) <<  " Which: " << (int) which << endl;
            goal_cardinal_connection();
            
            #if defined(VERBOSE2)
            if (gtype == JPD and !connect_cardinal) break;
            #elif defined(ONLY_MERGED)
            if (!connect_cardinal) break;
            #endif

            // Left
            #if defined(VERBOSE2)
            if (gtype != JPD or (gtype == JPD and not cell_inc_right)) {
            diag_dir = dir_offset(card_dir, LEFT);
            jg_diagonal_connection();
            }
            #elif defined(ONLY_MERGED)
            if (not cell_inc_right){
            diag_dir = dir_offset(card_dir, LEFT);
            jg_diagonal_connection();
            } 
            #else 
            diag_dir = dir_offset(card_dir, LEFT);
            jg_diagonal_connection();
            #endif

            // Left
            #if defined(VERBOSE2)
            if (gtype != JPD or (gtype == JPD and not cell_inc_left)) {
            diag_dir = dir_offset(card_dir, RIGHT);
            jg_diagonal_connection();
            }
            #elif defined(ONLY_MERGED)
            if (not cell_inc_left){
            diag_dir = dir_offset(card_dir, RIGHT);
            jg_diagonal_connection();
            } 
            #else 
            diag_dir = dir_offset(card_dir, RIGHT);
            jg_diagonal_connection();
            #endif
            main_clr = clr_backward->get_clr(jump_index, card_dir);
        }
    }
}


// Initial diagonal connection for JPD
void SubgoalGraph::jg_all_diagonals_connection(){

    jump_index = node_s->mindex;
    for (diag_dir = 1; diag_dir < 8; diag_dir+=2)
    {
        jg_diagonal_connection();
    }   
}


// Diagonal scan for backward connection
void SubgoalGraph::jg_single_diagonal_connection(){
    which = is_node[final_index];
    // Invalid reference
    if (get_nth_byte(which, opposite_diag_dir)){
        nb_id = nodes_map[umap_index(final_index, opposite_diag_dir)];
        // This happens with very low frequency
        if (nb_id == -1){
            V2(if (debug_connection) cout << "Overcharged reference. Connecting to 2 nodes ");
            nb_id = nodes_map[umap_index(final_index, dir_offset(opposite_diag_dir, LEFT))];
            V2(if (debug_connection) cout << subgoal_info(nb_id));
            new_node_nbs.push_back(nb_id);
            nb_id = nodes_map[umap_index(final_index, dir_offset(opposite_diag_dir, RIGHT))];
            V2(if (debug_connection) cout << " and " << subgoal_info(nb_id) << endl);
            new_node_nbs.push_back(nb_id);
        } else{
            V2(if(debug_connection) cout << "Connecting to "<< subgoal_info(nb_id) << endl);
            new_node_nbs.push_back(nb_id);
        }

    }
}


// JP backward diagonal connection loop
void SubgoalGraph::jg_loop_diagonal_connection(){
    while (clr > 0)
    {
        V2( if (debug_connection) cout << "Pos reached: " << v2s(i2p(final_index)) << endl);
        jg_single_diagonal_connection();
        clr = clr_backward->get_clr(final_index, diag_dir);
        final_index = final_index + dirs.dindex[diag_dir]*clr;
    }
}


// Goal cardinal connection (JP and JPD)
void SubgoalGraph::goal_cardinal_connection(){
    if (get_nth_byte(which, opposite_dir)){
        V2(if (debug_connection) cout << "Incoming connection to " << subgoal_info(nb_id) << endl);
        nb_id = nodes_map[umap_index(jump_index, opposite_dir)];
        new_node_nbs.push_back(nb_id);
        connect_cardinal = false;
        #if defined(ONLY_MERGED) or defined(VERBOSE2)
        last_connection = nb_id;
        #endif
    }
    if (not inc_left and get_nth_byte(which, left_dir)){
        V2(if (debug_connection) cout << "Left node found " << endl);
        if (jg_incoming_lateral_conn(left_dir, obs_dir_left)){
            inc_left = true;
            #if defined(VERBOSE2)
            if (gtype == JPD) cell_inc_left = true;
            #elif defined(ONLY_MERGED)
            cell_inc_left = true;
            #endif
        };
    }

    if (not inc_right and get_nth_byte(which, right_dir)){
        V2(if (debug_connection) cout << "Right node found " << endl);
        if (jg_incoming_lateral_conn(right_dir, obs_dir_right)){
            inc_right = true;
            #if defined(VERBOSE2)
            if (gtype == JPD) cell_inc_right = true;
            #elif defined(ONLY_MERGED)
            cell_inc_right = true;
            #endif
        };
    }
}
