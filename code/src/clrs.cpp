#include "clrs.hpp"
#include "unordered_set"


/* 
Clrs
Clearances computation for all subgoal subgoal graphs.
Provides clearance interface for subgoal graphs.
POI: point of interest, used to detect different type of subgoals or cells.
 */


using namespace std::placeholders;


// Initialize and compute a type of clearances.
Clearance::Clearance(CLR_TYPE clr_type, const Mapa& mapa, cnpy::npz_t* clr_map,
 nlohmann::json clr_attr, const vector<IsNodeDir>& is_node, bool from_scratch)
: clr_type(clr_type), mapa(mapa), is_node(is_node), from_scratch(from_scratch)
{   

    dense_type = clr_attr["dense_type"].get<bool>();
    direct_type = clr_attr["direct_type"].get<CLR_TYPE>();
    do_cardinal = clr_attr["do_cardinal"].get<bool>();
    do_diagonal = clr_attr["do_diagonal"].get<bool>();
    name = clr_attr["name"].get<string>();
    save = clr_attr["save"] == nullptr;

    cardinal_first = direct_type != CLR_CARDINAL_INDIRECT;

    if (clr_map != nullptr and save){
        load_clrs(clr_map);
        return;
    } 
    auto start_time = set_timer();
    
    if (matrix.size() != 8) matrix.resize(8);
    set_point_of_interest_function();
    if (cardinal_first){
        if (do_cardinal) cardinal_clearances();
        if (do_diagonal) diagonal_clearances();
    } else {
        if (do_diagonal) diagonal_clearances();
        if (do_cardinal) cardinal_clearances();
    }
    if (not save) ptime = end_timer(start_time);
}


// No point of interest
bool Clearance::no_subgoal_poi(PosIndex pos_index) const{
    return false;
}


// Any subgoal poi
bool Clearance::any_subgoal_poi(PosIndex pos_index) const{
    if (is_node[pos_index] > 0) return true;
    return false;
}


// Subgoal with a direction difference with the scanning direction.
bool Clearance::subgoal_given_direction_exact_offset_poi(PosIndex pos_index, int offset) const{
    IsNodeDir which = is_node[pos_index]; 
    if (get_nth_byte(which, dir_offset(dir_index, offset)) == 1)
    {
        return true;
    } 
    return false;
}


// Subgoal with a symmetric difference with the scanning direction.
bool Clearance::subgoal_given_direction_symmetric_offset_poi(PosIndex pos_index, int offset) const{
    IsNodeDir which = is_node[pos_index];
    if (get_nth_byte(which, dir_offset(dir_index, offset)) ==1 or 
        get_nth_byte(which, dir_offset(dir_index, -(int)offset)) ==1) 
        return true;
    return false;
}


// Check if a cell with a clearance leads to a point of interest.
bool Clearance::look_clearance(PosIndex pos_index){
    Pos clr;
    PosIndex new_pos;
    Dir check_dir;
    for (size_t i = 0; i < 2; i++)
    {
        check_dir = dir_offset(dir_index, (i==0) ? 1:-1);
        clr = get_clr(pos_index, check_dir);
        if (!dense_type){
            if (clr > 0) return true;
        }
        else{
            new_pos = mapa.move(pos_index, check_dir, clr);
            if (is_point_of_interest(new_pos)) return true;
        }
    }
    return false;
}


// Load clearnaces from a numpy map
bool Clearance::load_clrs(cnpy::npz_t* clr_map){
    if (matrix.size() != 8) matrix.resize(8);
    string clr_key;
    cnpy::NpyArray* npy_arr;
    for (Dir dir_index = 0; dir_index < 8; dir_index++)
    {
        if (!do_diagonal and is_diagonal(dir_index))continue;
        if (!do_cardinal and is_cardinal(dir_index))continue;
        clr_key = name + "_" + dirs.names[dir_index];
        npy_arr = &clr_map[0][clr_key];
        matrix[dir_index] = vector<CLR>(&npy_arr->data<CLR>()[0], &npy_arr->data<CLR>()[mapa.matrix.size()]);
    }
    return true;
}


// Types of clearances. For DSG: CLR_DSG_CF.
void ClearanceManager::load_clearance_types(){
    clrs_attr[CLR_SG]["do_cardinal"] = true;
    clrs_attr[CLR_SG]["do_diagonal"] = true;
    clrs_attr[CLR_SG]["direct_type"] = CLR_DIRECT;
    clrs_attr[CLR_SG]["name"] = "both";
    clrs_attr[CLR_SG]["dense_type"] = true; 

    clrs_attr[CLR_SG2]["do_cardinal"] = true;
    clrs_attr[CLR_SG2]["do_diagonal"] = true;
    clrs_attr[CLR_SG2]["direct_type"] = CLR_DIAGONAL_INDIRECT;
    clrs_attr[CLR_SG2]["name"] = "both";
    clrs_attr[CLR_SG2]["dense_type"] = false; 

    clrs_attr[CLR_NO_NODES]["do_cardinal"] = true;
    clrs_attr[CLR_NO_NODES]["do_diagonal"] = true;
    clrs_attr[CLR_NO_NODES]["direct_type"] = CLR_DIRECT;
    clrs_attr[CLR_NO_NODES]["name"] = "no_nodes";
    clrs_attr[CLR_NO_NODES]["dense_type"] = true; 
    clrs_attr[CLR_NO_NODES]["save"] = false;
    // This are the clearances used in DSG
    clrs_attr[CLR_DSG_CF]["do_cardinal"] = true;
    clrs_attr[CLR_DSG_CF]["do_diagonal"] = true;
    clrs_attr[CLR_DSG_CF]["direct_type"] = CLR_CARDINAL_INDIRECT;
    clrs_attr[CLR_DSG_CF]["name"] = "CF";
    clrs_attr[CLR_DSG_CF]["dense_type"] = false; 

    clrs_attr[CLR_DSG_BOTH]["do_cardinal"] = true;
    clrs_attr[CLR_DSG_BOTH]["do_diagonal"] = true;
    clrs_attr[CLR_DSG_BOTH]["direct_type"] = CLR_CARDINAL_INDIRECT;
    clrs_attr[CLR_DSG_BOTH]["name"] = "both";
    clrs_attr[CLR_DSG_BOTH]["dense_type"] = false; 


    clrs_attr[CLR_DSG_CARD_OUT]["do_cardinal"] = true;
    clrs_attr[CLR_DSG_CARD_OUT]["do_diagonal"] = false;
    clrs_attr[CLR_DSG_CARD_OUT]["direct_type"] = CLR_DIRECT;
    clrs_attr[CLR_DSG_CARD_OUT]["name"] = "card_out";
    clrs_attr[CLR_DSG_CARD_OUT]["dense_type"] = true; 

    clrs_attr[CLR_DSG_CARD_IN]["do_cardinal"] = true;
    clrs_attr[CLR_DSG_CARD_IN]["do_diagonal"] = false;
    clrs_attr[CLR_DSG_CARD_IN]["direct_type"] = CLR_DIRECT;
    clrs_attr[CLR_DSG_CARD_IN]["name"] = "card_in";
    clrs_attr[CLR_DSG_CARD_IN]["dense_type"] = true; 

    clrs_attr[CLR_DSG_CARD]["do_cardinal"] = true;
    clrs_attr[CLR_DSG_CARD]["do_diagonal"] = false;
    clrs_attr[CLR_DSG_CARD]["direct_type"] = CLR_DIRECT;
    clrs_attr[CLR_DSG_CARD]["name"] = "card_all";
    clrs_attr[CLR_DSG_CARD]["dense_type"] = true; 

    clrs_attr[CLR_JP_FORWARD]["do_cardinal"] = true;
    clrs_attr[CLR_JP_FORWARD]["do_diagonal"] = true;
    clrs_attr[CLR_JP_FORWARD]["direct_type"] = CLR_DIAGONAL_INDIRECT;
    clrs_attr[CLR_JP_FORWARD]["name"] = "fw";
    clrs_attr[CLR_JP_FORWARD]["dense_type"] = false; 

    clrs_attr[CLR_JP_BACKWARD]["do_cardinal"] = true;
    clrs_attr[CLR_JP_BACKWARD]["do_diagonal"] = true;
    clrs_attr[CLR_JP_BACKWARD]["direct_type"] = CLR_CARDINAL_INDIRECT;
    clrs_attr[CLR_JP_BACKWARD]["name"] = "bw";
    clrs_attr[CLR_JP_BACKWARD]["dense_type"] = false; 

    clrs_attr[CLR_JPD_FORWARD]["do_cardinal"] = true;
    clrs_attr[CLR_JPD_FORWARD]["do_diagonal"] = true;
    clrs_attr[CLR_JPD_FORWARD]["direct_type"] = CLR_DIAGONAL_INDIRECT;
    clrs_attr[CLR_JPD_FORWARD]["name"] = "fw";
    clrs_attr[CLR_JPD_FORWARD]["dense_type"] = false; 

    clrs_attr[CLR_JPD_BACKWARD]["do_cardinal"] = true;
    clrs_attr[CLR_JPD_BACKWARD]["do_diagonal"] = true;
    clrs_attr[CLR_JPD_BACKWARD]["direct_type"] = CLR_CARDINAL_INDIRECT;
    clrs_attr[CLR_JPD_BACKWARD]["name"] = "bw";
    clrs_attr[CLR_JPD_BACKWARD]["dense_type"] = false; 
}


ClearanceManager::~ClearanceManager(){
    for (auto clr_type: used_clrs)
    {
        if (clr_type != CLR_NO_NODES) delete clearances_vector[clr_type];
    }
}


// Clearance manager
// Invokes the corresponding clearances for each subgoal graph.
ClearanceManager::ClearanceManager(const Mapa& mapa, int gtype, int variant, string  gname, const vector<IsNodeDir>& is_node, bool do_CH, bool from_scratch)
    : mapa(mapa), gtype(gtype), variant(variant), do_CH(do_CH) {
    clearances_vector.resize(CLR_LAST);
    define_used_clrs();
    load_clearance_types();
    base_path = "../clearances/" + mapa.benchmark + "/";
    std::filesystem::create_directories(base_path);

    npy_file = base_path + mapa.name + "#" + gname + ".npy";
    cnpy::npz_t clr_map;
    bool loaded = false;
    if (not from_scratch and file_exists(npy_file)){
        cout << "Loading clearance from " << npy_file << endl;
        clr_map = cnpy::npz_load(npy_file);
        ptime = clr_map["PT"].data<uint>()[0];
        loaded = true;
    } else cout << "Clearances from scratch" << endl;
    
    auto start_time = set_timer();
    for (auto clr_type: used_clrs)
    {   if (clrs_attr[clr_type]["save"] != nullptr) continue;
        clearances_vector[clr_type] = new Clearance(clr_type, mapa, loaded ? &clr_map:nullptr, clrs_attr[clr_type], is_node, from_scratch);
    }
    if (not loaded){
        ptime = end_timer(start_time);
        cnpy::npz_save<uint>(npy_file, "PT", &ptime, {1}, "w");
        string clr_name;
        for (auto clr_type: used_clrs)
        {
            if (clrs_attr[clr_type]["save"] != nullptr) continue;
            for (size_t i = 0; i < 8; i++)
            {
                if (not clearances_vector[clr_type]->do_cardinal and is_cardinal(i))  continue;
                if (not clearances_vector[clr_type]->do_diagonal  and is_diagonal(i)) continue;
                
                clr_name = clearances_vector[clr_type]->name + "_" + dirs.names[i];
                cnpy::npz_save<uint8_t>(npy_file, clr_name,  clearances_vector[clr_type]->matrix[i].data(), {(size_t)mapa.size[0] *(size_t)mapa.size[1]}, "a");
            }
        }
    }
    if (do_CH){
        clearances_vector[CLR_NO_NODES] = new Clearance(CLR_NO_NODES, mapa, nullptr, clrs_attr[CLR_NO_NODES], is_node, true);
        ptime += clearances_vector[CLR_NO_NODES]->ptime;
    }
}


// Calculate a clearance for all the grid and a direction. 
void Clearance::calculate_clearance(){
    matrix[dir_index].resize(mapa.matrix.size(), 0);
    APos dir_arr, cell, step, poi;
    int main_axis, secondary_axis;
    size_t its;
    bool start_at_end, seen_poi, found_condition, is_card;
    int secondary_axis_sign;
    CLR clr;
    dir_arr = dirs.arrays[dir_index];
    is_card = is_cardinal(dir_index); 
    its = is_card ? 1: 2; 
    step = multiply(dir_arr, -1);
    for (size_t it = 0; it < its; it++)
    {   

        if (is_card){
            main_axis = abs(dir_arr[0]) > abs(dir_arr[1]) ? 0 : 1;
            secondary_axis = 1 - main_axis;
        } else
        {
            main_axis = it;
            secondary_axis = 1-it;
        }
        start_at_end = dir_arr[main_axis] == 1;
        secondary_axis_sign = sgn(dir_arr[secondary_axis]);
        for (size_t i = 0 + (secondary_axis_sign == -1); i < mapa.size[secondary_axis] - (secondary_axis_sign == 1); i++)
        {
            cell[main_axis] = start_at_end ? mapa.size[main_axis] - 1 - 1 : 1;
            cell[secondary_axis] = i ;
            clr = 0 ;
            seen_poi = false;
            poi = add_vectors(cell, dir_arr);
            while (mapa.is_inside(cell))
            {
                assert(mapa.is_inside(mindex(cell)));
                if (not mapa.is_reachable_from(mindex(cell), dir_index)){
                    clr = 0;
                    seen_poi = false;
                }
                else{
                    found_condition = 
                    (is_direct && is_point_of_interest(mindex(poi)))
                    or
                    (!is_direct && 
                        (look_clearance(mindex(poi)) or (second_point_of_interest and second_point_of_interest(mindex(poi))))
                    );
                    if (found_condition)
                    {
                        seen_poi = true;
                        clr = 1;
                    }
                    else if (is_dense and mapa.is_free(mindex(poi)) and clr == 0){
                        clr ++;
                    }
                } 
                matrix[dir_index][mindex(cell)] = clr;
                if (clr < (CLR)-1){
                    if (is_dense ) clr++;
                    else if (seen_poi) clr++;
                }
                
                poi = cell;
                cell = add_vectors(cell, step);
            }
        }  
    }
}


// Clearance interface for subgoal graphs
Pos Clearance::get_clr(PosIndex pos_index, Dir dir_index){
    short_clr  =  matrix[dir_index][pos_index];
    total_clr = short_clr;
    while (short_clr == (CLR)-1){
        short_clr -= 1;
        pos_index += short_clr*dirs.dindex[dir_index];
        short_clr =  matrix[dir_index][pos_index];
        total_clr += short_clr -1;
    }
    return total_clr;
}


// Defin clearances to calculate in each subgoal graph. Variants of a graph are ways to build a graph using different clearances.
void ClearanceManager::define_used_clrs(){
    switch (gtype)
    {
    case DSG_NOT_MERGED:
        switch (variant)
        {
        case DSG_2_CLR:
            used_clrs = {
                CLR_DSG_BOTH
            };            
            break;
        
        case DSG_3_CLR:
            used_clrs = {
                CLR_DSG_CF
                ,CLR_DSG_CARD
            };
            break;
        case DSG_4_CLR:
            used_clrs = {
                CLR_DSG_CF
                ,CLR_DSG_CARD_OUT
                ,CLR_DSG_CARD_IN
            };
            break;
        }    
        break;
    case DSG:
        used_clrs = {
            CLR_DSG_BOTH
        };
        break;
    case JP:
        used_clrs = {
            CLR_JP_FORWARD
            ,CLR_JP_BACKWARD
        };
        break;
    case JPD:
        used_clrs = {
            CLR_JPD_FORWARD
            ,CLR_JPD_BACKWARD
        };
        break;
    case SG:
        used_clrs = {
            CLR_SG2
        };
        break;
    }
    if (do_CH) used_clrs.push_back(CLR_NO_NODES);
}


// Set the corresponding poit of interest function
void Clearance::set_point_of_interest_function(){
    switch (clr_type)
    {
    case CLR_NO_NODES:
        is_point_of_interest =  std::bind(&Clearance::no_subgoal_poi, this, _1);
        break;
    case CLR_SG:
        is_point_of_interest =  std::bind(&Clearance::any_subgoal_poi, this, _1);
        break;
    case CLR_SG2:
        is_point_of_interest =  std::bind(&Clearance::any_subgoal_poi, this, _1);
        second_point_of_interest = std::bind(&Clearance::any_subgoal_poi, this, _1);
        break;
    case CLR_DSG_CF:
        is_point_of_interest =  std::bind(&Clearance::subgoal_given_direction_exact_offset_poi, this, _1, 0);
        break;
    case CLR_DSG_BOTH:
        is_point_of_interest =  std::bind(&Clearance::subgoal_given_direction_exact_offset_poi, this, _1, 0);
        second_point_of_interest = std::bind(&Clearance::any_subgoal_poi, this, _1);
    case CLR_DSG_CARD_OUT:
        is_point_of_interest =  std::bind(&Clearance::subgoal_given_direction_exact_offset_poi, this, _1, 0);
        break;
    case CLR_DSG_CARD_IN:
        is_point_of_interest =  std::bind(&Clearance::subgoal_given_direction_exact_offset_poi, this, _1, 4);
        break;
    case CLR_DSG_CARD:
        is_point_of_interest =  std::bind(&Clearance::any_subgoal_poi, this, _1);
        break;
    case CLR_JP_FORWARD:
        is_point_of_interest =  std::bind(&Clearance::subgoal_given_direction_exact_offset_poi, this, _1, 0);
        break;
    case CLR_JP_BACKWARD:
        is_point_of_interest =  std::bind(&Clearance::subgoal_given_direction_exact_offset_poi, this, _1, 4);
        second_point_of_interest = std::bind(&Clearance::any_subgoal_poi, this, _1);
        break;
    case CLR_JPD_FORWARD:
        is_point_of_interest = std::bind(&Clearance::subgoal_given_direction_exact_offset_poi, this, _1, 0);
        second_point_of_interest = std::bind(&Clearance::subgoal_given_direction_exact_offset_poi, this, _1, 0);
        break;
    case CLR_JPD_BACKWARD:
        is_point_of_interest =  std::bind(&Clearance::subgoal_given_direction_exact_offset_poi, this, _1, 4);
        second_point_of_interest = std::bind(&Clearance::any_subgoal_poi, this, _1);
        break;
    }

}


// Calculate all cardinal clearances
void Clearance::cardinal_clearances(){
    is_direct = (direct_type == CLR_CARDINAL_INDIRECT) ? false : true;
    is_dense = (direct_type == CLR_CARDINAL_INDIRECT) ? false : dense_type;
    for (size_t i = 0; i < 4; i++)
    {
        dir_index = 2*i;
        calculate_clearance();
    }
}


// Calculate all diagonal clearances
void Clearance::diagonal_clearances(){
    is_direct = (direct_type == CLR_DIAGONAL_INDIRECT) ? false:true;
    is_dense = (direct_type == CLR_DIAGONAL_INDIRECT) ? false : dense_type;
    for (size_t i = 0; i < 4; i++)
    {
        dir_index = 2*i+1;
        calculate_clearance();
    }
}



