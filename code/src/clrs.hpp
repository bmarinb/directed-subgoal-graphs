#ifndef JPC_H
#define JPC_H
#include "functions.hpp"
#include "mapa.hpp"
#include <functional>

/* 
Clrs
Clearances computation for all subgoal subgoal graphs.
Provides clearance interface for subgoal graphs.
 */

struct SubgoalGraph;


// Types of CLR
const CLR_TYPE CLR_NO_NODES = 0;
// 2013 SG connection
const CLR_TYPE CLR_SG = 1;
// Final DSG connection
const CLR_TYPE CLR_DSG_CF = 2;
// Other variant clearnaces
const CLR_TYPE CLR_DSG_BOTH = 4;
const CLR_TYPE CLR_DSG_CARD_OUT = 5;
const CLR_TYPE CLR_DSG_CARD_IN = 6;
const CLR_TYPE CLR_DSG_CARD = 7;
const CLR_TYPE CLR_JP_FORWARD = 8;
const CLR_TYPE CLR_JP_BACKWARD = 9;
const CLR_TYPE CLR_JPD_FORWARD = 10;
const CLR_TYPE CLR_JPD_BACKWARD = 11;
// Optimized SG connection
const CLR_TYPE CLR_SG2 = 12;
const CLR_TYPE CLR_LAST = 13;

// CLR direction type
const CLR_TYPE CLR_DIRECT = 0;
const CLR_TYPE CLR_CARDINAL_INDIRECT = 1;
const CLR_TYPE CLR_DIAGONAL_INDIRECT = 2;

class Clearance{
    public:
        Clearance(CLR_TYPE clr_type,  const Mapa& mapa,
        cnpy::npz_t* clr_map, nlohmann::json clr_attr
        ,const vector<IsNodeDir>& is_node, bool from_scratch);
        Pos get_clr(PosIndex pos_index, Dir dir_index);
        bool do_cardinal, do_diagonal;
        string  name;
        vector<vector<CLR>> matrix;
        uint ptime;


    private:
        void calculate_clearance();
        bool no_subgoal_poi(PosIndex pos_index) const;
        bool any_subgoal_poi(PosIndex pos_index) const ;
        bool subgoal_given_direction_exact_offset_poi(PosIndex pos_index, int offset) const;
        bool subgoal_given_direction_symmetric_offset_poi(PosIndex pos_index, int offset) const;
        bool look_clearance(PosIndex pos_index);
        // input params
        CLR_TYPE clr_type;
        const Mapa& mapa;
        const vector<IsNodeDir>& is_node;
        // Other params
        CLR short_clr;
        Pos total_clr;
        Dir dir_index;
        void set_point_of_interest_function();
        bool load_clrs(cnpy::npz_t* clr_map);
       
        void cardinal_clearances();
        void diagonal_clearances();
        // Function used to detect different cells of interest.
        std::function<bool(PosIndex)> is_point_of_interest;
        // Function used for recursive clearances.
        std::function<bool(PosIndex)> second_point_of_interest;
        bool dense_type;
        int direct_type;
        bool is_direct;
        bool is_dense;
        bool cardinal_first, from_scratch, save;

};

class ClearanceManager{
    public:
    ClearanceManager(const Mapa& mapa, int gtype, int variant, string gname, const vector<IsNodeDir>& is_node, bool do_CH, bool from_scratch);
    ~ClearanceManager();
    vector<Clearance*> clearances_vector;
    vector<CLR_TYPE> used_clrs; 
    uint ptime;
    private:
    string base_path, npy_file;

    const Mapa& mapa;
    int gtype;
    int variant;
    bool do_CH;
    bool load_npy();
    void define_used_clrs();        
    void load_clearance_types();
    nlohmann::json clrs_attr;
};



#endif