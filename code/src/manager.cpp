#include "manager.hpp"
#include "solver.hpp"


/* 
Manager
Receives command line arguments and performs corresponding Solver and SolverStats calls.
Main usages:
- Solve a single map
- Solve a single benchmark
 */

Manager::Manager(
        string root_folder, string game_name, string map_name, string custom_map_name,  string custom_graph_name, string ch_params_path, string avoidance_params_path, string stats_file_path, int gtype, int variant, int sub_algorithm, int n_problem,  uint n_maps, int first_map, int debug_node, bool debug_connection,  bool save_jsons, bool do_CH, bool rotate_map, bool solve, bool save_stats, const vector<int>& start, const vector<int>& goal, bool SG_from_scratch ,bool CH_from_scratch ,bool clrs_from_scratch
    )
    :
    root_folder(root_folder), game_name(game_name), map_name(map_name), custom_graph_name(custom_graph_name), gtype(gtype), variant(variant), sub_algorithm(sub_algorithm), n_problem(n_problem), n_maps(n_maps), first_map(first_map), debug_node(debug_node), debug_connection(debug_connection), save_jsons(save_jsons), do_CH(do_CH), rotate_map(rotate_map),  solve(solve), save_stats(save_stats), SG_from_scratch(SG_from_scratch), CH_from_scratch(CH_from_scratch), clrs_from_scratch(clrs_from_scratch)
    {

    // Assert graph type
    assert(in_vector(gtype, {SG, DSG, JP, DSG_NOT_MERGED, JPD, GRID_GRAPH}));
    cout << "Using ";
    switch (gtype)
    {
    case SG:
        cout << "Subgoal Graphs (SG)" << endl;
        break;
    case DSG:
        cout << "Directed Subgoal Graphs (DSG)" << endl;
        break;
    case JP:
        cout << "Jump Point Graphs (JP)" << endl;
        break;
    case JPD:
        cout << "Diagonal Merged Jump Point Graphs (JPD)" << endl;
        break;
    case GRID_GRAPH:
        cout << "Grid Graph" << endl;
        break;
    case DSG_NOT_MERGED:
        cout << "Directed Subgoal Graphs (only diagonal nodes) DSGNM" << endl;
        break;
    }
    // Assert search algorithm
    if (sub_algorithm != DEFAULT_ALGORITHM){
        if (do_CH) assert(sub_algorithm >= FIRST_BID_DIJKSTRA);
        else assert(sub_algorithm >= FIRST_ASTAR and sub_algorithm < FIRST_BID_DIJKSTRA);
    } 

    // Keep the same params for the whole execution
    load_json(avoidance_params, avoidance_params_path);
    if (do_CH) load_json(ch_params, ch_params_path);
    load_json(stats_dict, stats_file_path);
    if (not solve) stats_dict.erase("problem");

    
    // Custom problem
    if (start.size() == 2 and goal.size() == 2){
        cout << "Solving custom problem" << endl;
        APos start_pos = {start[0], start[1]};
        APos goal_pos = {goal[0], goal[1]};
        custom_problem = new Problem(start_pos, goal_pos, 0, 1, do_CH);
    }

    // Custom Map
    if (custom_map_name.size() > 0)
    {
        game_path = root_folder;
        map_name = custom_map_name;
        solve_map();
    }

    // Map from benchmark
    set_benchmarks_path();
    string scen_file; 
    solver_stats = new SolverStats(get_graph_name(), do_CH , n_maps, stats_dict, custom_graph_name);
    
    if (map_name.size() > 0 and game_name.size() > 0){
        cout << "Solving single map" << endl;
        solver_stats->init_game_folder_stats(game_name);
        solve_map();
    }
    else if (game_name.size() > 0){
        cout << "Solving single benchmark" << endl;
        benchmark_folder = root_folder + "/maps/" + game_name + "-map";
        solve_benchmark();
    } 
    else
    {
        cout << "Must provide a benchmark" << endl;
        exit(0);
    }
    return;
}

Manager::~Manager(){
    if (custom_problem != nullptr) delete custom_problem;
    if (solver != nullptr) delete solver;
    if (solver_stats != nullptr) delete solver_stats;
}

//  Solve all instances from a map.
void Manager::solve_map(){
    set_game_path();

    solver = new Solver(gtype, game_path, game_name, map_name, variant, debug_connection, debug_node, save_jsons, sub_algorithm, do_CH, ch_params, avoidance_params, rotate_map, solver_stats->stats_dict, solver_stats->game_folder_stats, solver_stats->map_stats, SG_from_scratch, CH_from_scratch, clrs_from_scratch, custom_graph_name);

    if (custom_problem != nullptr){
        solver->solve_single_problem(custom_problem[0]);
        return;
    } 
    
    else if (solve){
        set_scen_path();
        solver->solve_problems(scen_file, n_problem);
    }
    if (save_stats){
        average_and_save_stats(solver->get_stats_filepath(solver_stats->stats_path), solver->map_stats, true);
        }   
    delete solver;
    solver = nullptr;
}


// Solve all instances from all maps in a benchmark
void Manager::solve_benchmark(){
    set_game_path();
    solver_stats->init_game_folder_stats(game_name);
    cout << "Solving benchmark: " << game_name << endl;
    std::set<string> maps_by_name;
    for (auto map_directory: fs::directory_iterator(game_path)){
        maps_by_name.insert(map_directory.path().filename().string());
    }
    int i = 1;
    for (auto &filename : maps_by_name)
    {
        if (i >= first_map){
            map_name = filename;
            cout << "Map " << i << ": " << map_name << endl;
            solve_map();
            solver_stats->update();
            n_maps --;
            if (n_maps == 0) break;
            }
        i ++;
    }
    if (save_stats){
        average_and_save_stats(solver_stats->get_stats_filepath(), solver_stats->game_folder_stats, false);
    }
}

string Manager::get_graph_name(){
    string name;
    switch (gtype)
    {
    case SG:
        name = "sg";
        break;
    case DSG:
        name = "dsg";
        break;
    case DSG_NOT_MERGED:
        name = "dsgnm";
        break;
    case JP:
        name = "jp";
        break;
    case JPD:
        name = "jpd";
        break;
    case GRID_GRAPH:
        name = "grid-graph";
        break;
    }
    return name;
}



void Manager::set_benchmarks_path(){
    all_benchmarks_path = root_folder + "maps/"; 
}

void Manager::set_game_path(){
    game_path =  all_benchmarks_path + game_name + "-map";
}
void Manager::set_map_path(){
    map_path = game_path + map_name;
}

void Manager::set_scen_path(){
    scen_file = root_folder + "/scens/" + game_name + "-scen/" + map_name + ".scen";
}


void Manager::average_and_save_stats(string filepath, nlohmann::json& stats, bool single_game, bool print){
    nlohmann::json averaged_stats;
    average_stats(solver_stats->stats_dict, averaged_stats, stats, single_game);
    save_json(averaged_stats, filepath);
    if (print) cout << averaged_stats.dump(4) << endl;
}


void average_stats(nlohmann::json& stats_dict,nlohmann::json& averaged_stats, nlohmann::json& stats, bool include_map_name){
    for (auto& [level, stat_json] : stats_dict.items()) {
        for (auto& [stat, descr] : stat_json.items()){
            if (descr["incremental"] != nullptr or 
                level.compare("static") == 0){
                averaged_stats[level][stat]  = stats[level][stat];
            }
            else if (level.compare("problem") == 0 and descr["ratio"] == nullptr){
                {
                    averaged_stats[level][stat] =  divfloat<double>(stats[level][stat].get<double>(), stats[level][ (descr["connect"] == nullptr)?  "n_problems" :"n_problems_with_search"].get<int64_t>());
                }
            }
            else if (level.compare("map") == 0 and descr["ratio"] == nullptr){
                if (stats[level][stat] != nullptr)
                    averaged_stats[level][stat] =  divfloat<double>(stats[level][stat].get<double>(), stats[level]["n_maps"].get<double>());   
            } 
        }
    }
    averaged_stats["map"]["perc_edges_forward"] = divfloat<double>(stats["map"]["n_edges_forward"].get<int64_t>(), stats["map"]["n_edges_CH"].get<int64_t>());
    averaged_stats["map"]["E/N"] = divfloat<double>(stats["map"]["n_edges"].get<int64_t>(), stats["map"]["n_nodes"].get<int64_t>());

    averaged_stats["map"]["E/N_CH"] = divfloat<double>(stats["map"]["n_edges_CH"].get<int64_t>(), stats["map"]["n_nodes"].get<int64_t>());
    if (averaged_stats.contains("problem"))
    {
        averaged_stats["problem"]["n_relaxed_edges_per_exp"] = divfloat<double>(stats["problem"]["n_relaxed_edges_per_exp"].get<double>(),stats["problem"]["n_expansions"].get<double>());
        averaged_stats["problem"]["avg_n_problems"]  = divfloat<double>(stats["problem"]["n_problems"].get<int64_t>(), stats["map"]["n_maps"].get<int64_t>());
    }
    if (include_map_name) averaged_stats["map"]["map_name"] = stats["map"]["map_name"];
}
