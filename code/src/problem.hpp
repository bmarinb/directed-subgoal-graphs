#ifndef PROBLEM_H
#define PROBLEM_H

#include "graph.hpp"
#include <boost/range/adaptor/reversed.hpp>

/* 
Problem
Stores information of an instance and its solution.
*/


struct Problem{
    void validate_path();
    const APos& start;
    const APos& goal;
    NodeId start_id;
    NodeId goal_id;
    AstarCost cost;
    AstarCost gt_cost;
    bool by_ch;
    bool valid;
    bool direct{false};

    NodeId new_nodes{0}, parent_id;
    EdgeId start_nbs{0}, goal_nbs{0};
    uint id_problem;
    uint search_time{0};
    uint connect_time{0};
    uint problem_time{0};
    uint refine_time{0};
    uint CSR_time{0};
    vector<Pos> path_x;
    vector<Pos> path_y;
    vector<NodeId> id_path;
    vector<PosIndex> sg_path_indices;
    vector<PosIndex> g_path_indices;
    string info();
    Problem(const APos& start,const APos& goal, AstarCost real_cost, uint id_problem, bool by_ch);
    nlohmann::json to_json();
    Problem(const APos& start, const APos& goal, AstarCost real_cost = 0, uint id_problem = 0);
    void preallocate(int sg_path_size, int g_path_size);
    inline void set_direct(AstarCost direct_cost){
        direct = true;
        valid = true;
        cost = direct_cost;
    };
    APos get_pos(int i){
        return APos{path_x[i], path_y[i]};
    };
    
};

#endif