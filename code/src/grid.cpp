#include "grid.hpp"

/* 
Grid
Grid exploration. Check freespace reachability relations and direct paths.
*/


Grid2d::Grid2d(Mapa* mapa) : mapa(mapa){};


Grid2d::Grid2d(Clearance* clrs) : clrs(clrs){};


// Try a freespace diagonal first path using clearances. Returns the distance.
float Grid2d::try_DF_path_clrs(const APos& start, const APos& goal){
     if (vector_equal(start, goal)){
        return 0;
    }
    dif[0] = goal[0]-start[0];
    dif[1] = goal[1]-start[1];
    big_axis = abs(dif[0]) >= abs(dif[1]) ? 0:1;
    diag[big_axis] = sgn(dif[big_axis]);
    diag[!big_axis] = sgn(dif[!big_axis]);
    card1[big_axis] = diag[big_axis];
    card1[!big_axis] = 0;
    mov1_len = abs(dif[!big_axis]);
    mov2_len = abs(dif[big_axis]) - mov1_len;
    diag_index = dirs.arr2index(diag);
    card_index = dirs.arr2index(card1);
    start_index = mindex(start);
    if (mov1_len > 0){
        if (mov1_len > clrs->get_clr(start_index, diag_index)){
            return -1;
        }
    } 
    if (mov2_len > 0){
        jump_index = start_index + dirs.dindex[diag_index]*mov1_len;
        if (mov2_len > clrs->get_clr(jump_index, card_index)){
            return -1;
        }
    }
    return mov1_len*r2 + mov2_len;
}


// Try a freespace diagonal first path using the grid graph. Returns the distance.
float Grid2d::try_DF_path_map(const APos& start, const APos& goal){
    if (vector_equal(start, goal)){
        return 0;
    }
    dif[0] = goal[0]-start[0];
    dif[1] = goal[1]-start[1];
    big_axis = abs(dif[0]) >= abs(dif[1]) ? 0:1;
    diag[big_axis] = sgn(dif[big_axis]);
    diag[!big_axis] = sgn(dif[!big_axis]);
    card1[big_axis] = diag[big_axis];
    card1[!big_axis] = 0;
    card2[!big_axis] = diag[!big_axis];
    card2[big_axis] = 0;
    mov1_len = abs(dif[!big_axis]);
    mov2_len = abs(dif[big_axis]) - mov1_len;
    jump_index = mindex(start);
    diag_pos_index = mindex(diag);
    c1_index = mindex(card1);
    c2_index = mindex(card2);
    u = 0;
    unblocked = true;
    while (u < mov1_len && unblocked){
        jump_index += diag_pos_index;
        c1 = jump_index - c1_index;
        c2 = jump_index - c2_index;
        if (mapa->is_obstacle_unsafe(jump_index) || mapa->is_obstacle_unsafe(c1) || mapa->is_obstacle_unsafe(c2)) unblocked = false;
        u ++;
        }
    if (unblocked){
        u = 0;
        while (u < mov2_len && unblocked){
            jump_index += c1_index;
            if (mapa->is_obstacle_unsafe(jump_index)) unblocked = false;
            u ++;
        }
    }
    if (unblocked) return mov1_len*r2 + mov2_len; 
    return -1;
}


// Try four reachability relations: DF, CF and square (manhattan distance)
int Grid2d::free_space_reachable(const APos& init, const APos& end){
    // Diagonal first 
    if (check_diagonal_first(init, end)) return DIAGONAL_FIRST;
    if (check_diagonal_first(end, init)) return CARDINAL_FIRST;
    if (check_square_0(init, end)) return SQUARE_0;
    if (check_square_0(end, init)) return SQUARE_1;
    return NO_BASIC_REACHABILITY;
}


// Try a freespace diagonal first path using clearances. Returns boolean.
bool Grid2d::check_diagonal_first(const APos& start, const APos& goal){
    return try_DF_path_clrs(start, goal) >= 0;
}


// Try a freespace square path using clearances. Returns the distance.
float Grid2d::try_sq0_path(const APos& start, const APos& goal){
    if (vector_equal(start, goal)){
        return true;
    }
    dif[0] = goal[0]-start[0];
    dif[1] = goal[1]-start[1];
    card1[0] = sgn(dif[0]);
    card1[1] = 0;
    card2[0] = 0;
    card2[1] = sgn(dif[1]);
    mov1_len = abs(dif[0]);
    mov2_len = abs(dif[1]);    
    start_index = mindex(start);
    card_index =dirs.arr2index(card1);

    unblocked = true;
    // Checkeo diagonal first
    if (mov1_len > clrs->get_clr(start_index, card_index)){
        unblocked = false;
    }
    if (mov2_len > 0){
        jump_index = start_index + dirs.dindex[card_index]*mov1_len;
        if (mov2_len > clrs->get_clr(jump_index, dirs.arr2index(card2))){
            unblocked = false;
        }
    }
    if (unblocked) return mov1_len + mov2_len;
    return -1;
}


// Try a freespace square path using clearances. Returns boolean.
bool Grid2d::check_square_0(const APos& start, const APos& goal){
    if (try_sq0_path(start, goal) >= 0) return true;
    return false;
}
