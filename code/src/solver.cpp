#include "solver.hpp"
#include <deque>
#include <limits>




/* 
Solver
Main class used to preprocess and solve all subgoal graphs. 

Solver Stats
Main class used to stats gathering and averaging.
 */



Solver::~Solver(){
    if (map != nullptr) delete map;
    if (clrs != nullptr) delete clrs;
    if (sg_graph != nullptr) delete sg_graph; 
    if (search != nullptr) delete search;
    if (grid_explorer != nullptr) delete grid_explorer;
}

/* 
Initialization
This performs the following preprocessing tasks.
1. Load map
2. Create subgoal graph 
    a. Define nodes
    b. Compute clearances
    c. Connect graph
    d. Remove redundant edges(only JP and JPD)
    e. Set avoidance parameters
3. Invokes CH procedure.
 */
// 
// This perform preprocessing 

Solver::Solver(int _gtype, string base_path, string benchmark,  string _map_name, int _variant, bool _debug_connection, NodeId _debug_node, bool _save_jsons, int sub_algorithm, bool _CH, const nlohmann::json& ch_params, const nlohmann::json& avoidance_params, bool rotate_map, nlohmann::json& _stats_dict, nlohmann::json& _stats, nlohmann::json& map_stats, bool SG_from_scratch, bool CH_from_scratch, bool clrs_from_scratch, string custom_graph_name) 
: gtype(_gtype), benchmark(benchmark), save_jsons(_save_jsons), sub_algorithm(sub_algorithm), do_CH(_CH), debug_connection(_debug_connection), rotate(rotate_map), stats_dict(_stats_dict), general_stats(_stats), map_stats(map_stats), custom_graph_name(custom_graph_name)
    {
    
    map = new Mapa(base_path, _map_name, benchmark, save_jsons, rotate_map);
    if (gtype == GRID_GRAPH){
        grid_graph = new Graph(map[0]);
    } else{

        sg_graph = new SubgoalGraph(map, _gtype, _variant, _debug_connection, _debug_node, do_CH, SG_from_scratch, CH_from_scratch, custom_graph_name);

        clrs = new ClearanceManager(map[0], _gtype, _variant, sg_graph->gtype_name,  sg_graph->is_node, do_CH and not sg_graph->CH_loaded, clrs_from_scratch);
        auto start_time = set_timer();
        sg_graph->update_clrs(clrs->clearances_vector);
        if (!sg_graph->graph_loaded){
            sg_graph->connect();
            if (in_vector(gtype, {JP, JPD})) sg_graph->remove_redundant_edges();
        }

        sg_graph->check_avoidance(avoidance_params);
        if (!sg_graph->graph_loaded){
            sg_graph->sg_build_time += end_timer(start_time);
        }
        avoid_search = sg_graph->avoid_search;
        avoid_connect = sg_graph->avoid_connect;
        if (!sg_graph->graph_loaded and _debug_node == -1) sg_graph->to_npy();
        this->algorithm = (do_CH) ? BID_SEARCH : ASTAR;
        if (save_jsons) sg_graph->to_json();
        if (do_CH)
        {   
            if (not sg_graph->CH_loaded){
                start_time = set_timer();
                CHier = new CH(sg_graph, map->name, sg_graph->gtype_name, save_jsons, ch_params);
                sg_graph->CH_build_time = end_timer(start_time);
                sg_graph->bid_to_npy();
            }
        } 
    }
    preprocessing_time = sg_graph->sg_build_time + clrs->ptime + sg_graph->CH_build_time;
    init_stats();
    update_map_stats();
    if (custom_graph_name.size() > 0){
        if (do_CH) save_json(ch_params, "./params/" + custom_graph_name + "_CH.json");
        save_json(avoidance_params, "./params/" + custom_graph_name + "_avoid.json");
    }
    if (do_CH){
        delete CHier;
        delete sg_graph->clr_no_nodes;
    }
}


// After preprocessing is done, this function prepares Solver to solve problems.
void Solver::prepare_for_problems(){
    if (gtype != GRID_GRAPH and sg_graph->directed_graph){
        for (size_t i = 0; i < 2; i++)
        {
            if (!sg_graph->allow_avoidance[i]){
                cout << "Clearing avoidance memory" << endl;
                sg_graph->avoidance_table[i].clear();
                sg_graph->avoidance_table[i].shrink_to_fit();
                sg_graph->base_avoidable[i].clear();
                sg_graph->base_avoidable[i].shrink_to_fit();
            }
        }
    }
    g_estimated_length = 5000;
    if (gtype != GRID_GRAPH){
        sg_graph->new_node_nbs.reserve(100);
        sg_estimated_length = (map->size[0]+map->size[1])/5;
    } 
    cout << "Estimated lengths: " << g_estimated_length << ", " << sg_estimated_length << endl;
    if (gtype == GRID_GRAPH){
        search = new Search(grid_graph, ASTAR, GRID_ASTAR, true);
    }
    else if (do_CH)
    {
        if (sub_algorithm == DEFAULT_ALGORITHM){
            sub_algorithm = BID_ASTAR_WITH_STALL;
        } else if (sub_algorithm == BID_ASTAR_AVOIDANCE){
            if (not sg_graph->directed_graph or (not avoid_search[0] and not avoid_search[1])){
                sub_algorithm = BID_ASTAR_WITH_STALL;         
            }
        }
        search = new Search(&sg_graph->sgs, &sg_graph->bid_search_nodes, &sg_graph->CH_edges, &sg_graph->avoidance_table, sg_graph->avoid_search, algorithm, sub_algorithm, sg_graph->directed_graph, sg_graph->mapa->size, MIN_MIN, true);
    }
    else{
        sg_graph->build_search_edges();
        if (! sg_graph->directed_graph){
            this->sub_algorithm = ASTAR_VANILLA;
        } else{
            if (avoid_search[FORWARD]) this->sub_algorithm = in_vector(sg_graph->gtype, {JP, DSG_NOT_MERGED})  ? ASTAR_AVOID_VISITED:ASTAR_AVOID_POS_F;
            else this->sub_algorithm = in_vector(sg_graph->gtype, {JP, DSG_NOT_MERGED})  ? ASTAR_VISITED:ASTAR_POS_F;
        }
        search = new Search(&sg_graph->sgs, &sg_graph->search_nodes, &sg_graph->search_edges, &sg_graph->search_pos_id, &sg_graph->avoidance_table, map->size, sg_graph->avoid_search, algorithm, sub_algorithm);
    }
}


void Solver::init_stats(){
    for (auto& [stat, descr] : stats_dict["static"].items()){
        map_stats["static"][stat] = general_stats["static"][stat];
    }
    for (auto& [stat, descr] : stats_dict["map"].items()){
        map_stats["map"][stat] = 0;
    }
    for (auto& [stat, descr] : stats_dict["problem"].items()){
        map_stats["problem"][stat] = 0;
    }
    
}

void Solver::update_map_stats(){
    string lvl = "map";
    plus_equal_json<int64_t>(map_stats[lvl]["n_maps"], 1);
    map_stats[lvl]["map_name"] = map->name;
    if (gtype == GRID_GRAPH){
        map_stats[lvl]["n_nodes"] = grid_graph->n_nodes;
        map_stats[lvl]["n_edges"] = grid_graph->n_edges;        
    }
    else{
        map_stats[lvl]["n_nodes"] = sg_graph->n_nodes;
        map_stats[lvl]["n_edges"] = sg_graph->n_edges;
        map_stats[lvl]["preprocessing_time"] = preprocessing_time;
        map_stats[lvl]["n_avoidable_nodes_forward"] = sg_graph->n_avoid_nodes[FORWARD];
        map_stats[lvl]["n_avoidable_nodes_backward"] = sg_graph->n_avoid_nodes[BACKWARD];
        if (do_CH){
            map_stats[lvl]["n_shortcuts"] = sg_graph->CH_stats["SC"];
            map_stats[lvl]["n_edges_CH"] = sg_graph->CH_stats["ECH"];
            map_stats[lvl]["n_edges_forward"] = sg_graph->CH_stats["EF"];
            map_stats[lvl]["n_redundant_shortcuts"] = sg_graph->CH_stats["RED"];
            map_stats[lvl]["n_diagonal_first_shortcuts"] = sg_graph->CH_stats["NDF"];
            map_stats[lvl]["n_cardinal_first_shortcuts"] = sg_graph->CH_stats["NCF"];
        } 
    }
}




// Solve all problems using subgoal graphs
void Solver::solve_problems(string scen_file, int n_problem){
    if (gtype != GRID_GRAPH) grid_explorer = new Grid2d(map);
    prepare_for_problems();
    std::chrono::_V2::system_clock::time_point start_time;
    int elapsed;
    std::ifstream problems_file;
    problems_file.open(scen_file);
    cout << "Scen file is: " << scen_file << endl;
    if (!problems_file) {
    std::cerr << "Unable to open scen file " << endl;
    exit(1);   
    }
    string line;
    getline(problems_file, line);
    APos start, goal;
    float cost;
    int n = 0;
    Problem* problem;
    int x_comp = rotate?0:1;
    int y_comp = rotate?1:0;
    while(getline(problems_file, line) && !line.empty()){
        sscanf(
            line.c_str(), "%*d %*s %*d %*d %d %d %d %d %f", 
            &start[x_comp], &start[y_comp], &goal[x_comp], &goal[y_comp], &cost);  
        if ((n+2== n_problem) or (n_problem < 0) ){
            V1(cout << "Solving problem ------------------------------------------------" << n+2 << endl << v2s(start) << ", " << v2s(goal) << endl);
            problem = (gtype == GRID_GRAPH) ?  (new Problem(start, goal, cost, n+2)) : (new Problem(start, goal, cost, n+2, do_CH));
            start_time = set_timer();
            if (gtype != GRID_GRAPH) solve_problem(*problem);
            else solve_grid_problem(*problem);
            elapsed = end_timer(start_time);
            V1(cout << "Time elapsed solving problem: " << elapsed << endl);
            update_problem_stats(problem[0]);
            delete problem;
        }
        n++;
    }
    problems_file.close();
}


//  Solve a single problem using grid Astar
void Solver::solve_single_problem(Problem& problem){
    grid_explorer = new Grid2d(map);
    solve_problem(problem);
}


//  Solve a single problem using a subgoal graph
void Solver::solve_grid_problem(Problem& problem){
    start_time_total = set_timer();
    search->reset_memory_and_stats();
    start_time = set_timer();
    search->solve(&problem);
    problem.search_time = end_timer(start_time);
    start_time = set_timer();
    search->recover_path();
    problem.refine_time = end_timer(start_time);
    process_problem_output(problem);
    problem.problem_time = end_timer(start_time_total);
}


// Solve a single problem using the CSR procedure
void Solver::solve_problem(Problem& problem){
    old_size = sg_graph->size();
    start_id = -1;
    goal_id = -1;
    start_time_total = set_timer();
    problem.preallocate(sg_estimated_length, g_estimated_length);

    // Try a direct path
    start_time = set_timer();
    direct_cost = grid_explorer->try_DF_path_map(problem.start, problem.goal);
    problem.connect_time = end_timer(start_time);
    
    // Refine the direct path
    if (direct_cost >= 0){
        start_time = set_timer();
        problem.set_direct(direct_cost);
        search->problem = &problem;
        problem.sg_path_indices.push_back(mindex(problem.start));
        problem.sg_path_indices.push_back(mindex(problem.goal));
        search->forward_G_refine_path();
        problem.refine_time = end_timer(start_time);
        V2(cout << problem.info() << endl);
        V2(problem.validate_path());
    } else {
        // Connect
        V1(cout << "Path is not direct. Searching" << endl;);
        start_time = set_timer();
        start_id = do_CH ? sg_graph->add_endpoint_CH(problem.start, false): sg_graph->add_endpoint(problem.start, false);
        problem.start_nbs = sg_graph->n_nbs;
        goal_id = do_CH ? sg_graph->add_endpoint_CH(problem.goal, true): sg_graph->add_endpoint(problem.goal, true);
        problem.goal_nbs = sg_graph->n_nbs;
        problem.connect_time += end_timer(start_time);
        problem.start_id = start_id;
        problem.goal_id = goal_id;
        V2(cout << "Goal and start ids: " << goal_id << ", " << start_id << endl);
        // Search
        search->reset_memory_and_stats();
        start_time = set_timer();
        search->solve(&problem);
        problem.search_time = end_timer(start_time);
        // Refine
        start_time = set_timer();
        if (do_CH) search->bid_search_refine_path();
        else search->forward_search_refine_path();
        problem.refine_time = end_timer(start_time);
        // Reset graph.
        process_problem_output(problem);
        if (goal_id >= old_size) do_CH? sg_graph->remove_last_node_CH(true) : sg_graph->remove_last_node(true);
        if (start_id >= old_size) do_CH? sg_graph->remove_last_node_CH(false): sg_graph->remove_last_node(false);
    }
    elapsed_total = end_timer(start_time_total);
    problem.problem_time = elapsed_total;
    problem.CSR_time = problem.connect_time + problem.search_time + problem.refine_time;
    
} 


// After a problem is solved, we compare the solution const with the ground truth cost
// Also saves the problem info.
void Solver::process_problem_output(Problem& problem){
    raise = false;
    if (problem.valid)
    {
        dif = fabs(problem.cost - problem.gt_cost); 
        if (dif >= search->epsilon)
        {
            count_path_moves(problem.id_path);
            exact_dif = fabs((total_moves[0] + total_moves[1]*r2) - problem.gt_cost);
            if (exact_dif >= search->epsilon){
                cout << "Problem " << problem.id_problem << endl;
                printf("Cost: %.9f, GT Cost: %.9f \n", problem.cost, problem.gt_cost);
                printf("Difference is: %.9f", dif);
                raise = true;
            } 
        }
        V2( 
            if (!problem.direct) cout << problem.info();
            else cout << "Direct path"<<endl;
            problem.validate_path();
        );

    } else {
        cout << "Problema " << problem.id_problem << " has no solution" << endl;
        raise = true;
    }
    
    if (save_jsons and gtype != GRID_GRAPH)
    {
        string CH_string = do_CH ? "-CH" : "";
        string path = "../problems/problem_" + map->name + "_" + sg_graph->gtype_name + CH_string;
        nlohmann::json json;
        nlohmann::json problem_json = problem.to_json();
        nlohmann::json search_json = search->to_json();
        vector<APos> start_nbs, goal_nbs;
        int i;
        if (not do_CH){
            goal_nbs.resize(sg_graph->search_nodes[problem.goal_id].n_edges);
            start_nbs.resize(sg_graph->search_nodes[problem.start_id].n_edges);  
            i = 0;
            for (EdgeId eid = sg_graph->search_nodes[problem.start_id].first_edge; eid < sg_graph->search_nodes[problem.start_id].last_edge(); eid++)
            {
                start_nbs[i] = sg_graph->sgs[sg_graph->search_edges[eid].target].get_pos();
                i++;
            }
            i = 0;
            for (EdgeId eid = sg_graph->search_nodes[problem.goal_id].first_edge; eid < sg_graph->search_nodes[problem.goal_id].last_edge(); eid++)
            {
                goal_nbs[i] = sg_graph->sgs[sg_graph->search_edges[eid].target].get_pos();
                i++;
            }
            json["problem"] = problem_json;
            json["search"] = search_json;
            json["goal_pos"] = sg_graph->sgs[problem.goal_id].get_pos(); 
            json["start_pos"] = sg_graph->sgs[problem.start_id].get_pos();
            nlohmann::json id_nbs;
            json["goal_id_nbs"] = sg_graph->get_edges(problem.goal_id);
            json["goal_pos_nbs"] = goal_nbs;
            json["start_id_nbs"] = sg_graph->get_edges(problem.start_id);
            json["start_pos_nbs"] = start_nbs;
            save_json(json, path + "_" + std::to_string(problem.id_problem) + ".json");
            sg_graph->to_json();
        } else{
            V2(cout << "Saving problem with CH is not currently supported" << endl);
        }
    }
    assert(not raise);
}


// Used to count diagonal and cardinal movements in a path, so that we can compare the cost of that movements with the ground truth.
// This is used when a problem solution cost differs from the ground truth.
array<Pos, 2> Solver::count_path_moves(vector<NodeId>&id_path){
    total_moves = {0,0};
    if (gtype != GRID_GRAPH){
        for (int i = id_path.size() - 1; i > 0 ; i--)
        {
            move = octile_moves(sg_graph->sgs[id_path[i]].get_pos(), sg_graph->sgs[id_path[i-1]].get_pos());
            total_moves[0] += move[0];
            total_moves[1] += move[1];
        }
    } 
    else {  
        for (int i = id_path.size() - 1; i > 0 ; i--)
        {
            move = octile_moves(i2p(id_path[i]), i2p(id_path[i-1]));
            total_moves[0] += move[0];
            total_moves[1] += move[1];
        }
    }
}


void Solver::update_problem_stats(Problem& problem){
    string lvl = "problem";
    plus_equal_json(map_stats[lvl]["n_problems"], 1);
    if (gtype == GRID_GRAPH){
        plus_equal_json<int64_t>(map_stats[lvl]["solution_depth"], problem.id_path.size());
        plus_equal_json<int64_t>(map_stats[lvl]["solution_cost"], problem.cost);
        plus_equal_json<int64_t>(map_stats[lvl]["search_time"], problem.search_time);
        plus_equal_json<int64_t>(map_stats[lvl]["refine_time"], problem.refine_time);
        plus_equal_json<int64_t>(map_stats[lvl]["problem_time"], problem.problem_time);
        plus_equal_json<int64_t>(map_stats[lvl]["n_percolations"], search->n_percolations);
        plus_equal_json<int64_t>(map_stats[lvl]["n_expansions"], search->n_expansions);
        plus_equal_json<double>(map_stats[lvl]["n_relaxed_edges_per_exp"], search->n_rel_per_exp);
    } 
    else{
        plus_equal_json<int64_t>(map_stats[lvl]["solution_depth"], problem.id_path.size());
        plus_equal_json<int64_t>(map_stats[lvl]["solution_cost"], problem.cost);

        plus_equal_json<int64_t>(map_stats[lvl]["added_nodes"], problem.new_nodes);
        plus_equal_json<int64_t>(map_stats[lvl]["added_edges"], problem.start_nbs + problem.goal_nbs);
        plus_equal_json<int64_t>(map_stats[lvl]["added_edges_start"], problem.start_nbs);
        plus_equal_json<int64_t>(map_stats[lvl]["added_edges_goal"], problem.goal_nbs);
        plus_equal_json<int64_t>(map_stats[lvl]["search_time"], problem.search_time);
        plus_equal_json<int64_t>(map_stats[lvl]["connect_time"], problem.connect_time);
        plus_equal_json<int64_t>(map_stats[lvl]["refine_time"], problem.refine_time);
        plus_equal_json<int64_t>(map_stats[lvl]["problem_time"], problem.problem_time);
        plus_equal_json<int64_t>(map_stats[lvl]["CSR_time"], problem.CSR_time);

        if (not problem.direct) {
            plus_equal_json<int64_t>(map_stats[lvl]["n_problems_with_search"], 1);
            plus_equal_json<int64_t>(map_stats[lvl]["n_percolations"], search->n_percolations);
            plus_equal_json<int64_t>(map_stats[lvl]["n_expansions"], search->n_expansions);
            plus_equal_json<double>(map_stats[lvl]["n_relaxed_edges_per_exp"], search->n_rel_per_exp);
            if (algorithm== BID_SEARCH)
            {
                plus_equal_json<double>(map_stats[lvl]["n_expansions_forward"], search->n_expansions_forward);

                if (sub_algorithm >= BID_DIJKSTRA_WITH_STALL){
                    plus_equal_json<double>(map_stats[lvl]["n_stalled"], search->stalled);
                }
                if (sub_algorithm == BID_DIJKSTRA_AVOIDANCE or sub_algorithm == BID_ASTAR_AVOIDANCE){
                    plus_equal_json<double>(map_stats[lvl]["n_avoided"], search->n_avoided);
                }
            } else if (algorithm == ASTAR){   

                if (search->avoid_search[FORWARD]) plus_equal_json<double>(map_stats[lvl]["n_avoided"], search->n_avoided);
                if (search->visit != VISIT_ALL) plus_equal_json<double>(map_stats[lvl]["n_skipped"], search->n_skipped);
            }
        } 
    }
}


string Solver::get_stats_filepath(string base_path){
    string filepath;
    string custom_ending = custom_graph_name.size() > 0 ? "#" + custom_graph_name : ""; 
    cout << "Base path: " << base_path << endl;
    filepath = base_path + "maps/" + map_stats["map"]["map_name"].get<string>() + "_"  + map_stats["static"]["benchmark"].get<string>() + 
    "_" + ((gtype == GRID_GRAPH)? grid_graph->name :sg_graph->gtype_name) + 
    ((map_stats["static"]["CH"].get<bool>())?"-CH":"")+ custom_ending + ".json";
    return filepath;
}


void SolverStats::update(){
    for (auto& [level, stat_json] : stats_dict.items()) {
        for (auto& [stat, descr] : stat_json.items()){
            if (level.compare("static") != 0)
            {
                plus_equal_json<double>(game_folder_stats[level][stat], map_stats[level][stat].get<double>());
            }
        }
    }
}


void SolverStats::update_all_stats(){
    for (auto& [level, stat_json] : stats_dict.items()) {
        for (auto& [stat, descr] : stat_json.items()){
            if (level.compare("static") != 0)
            {
                plus_equal_json<double>(all_stats[level][stat], game_folder_stats[level][stat].get<double>());
            }
        }
    }    
}


void SolverStats::init_game_folder_stats(string game_name){
    init_stats(game_folder_stats, game_name);
    stats_path = base_path + game_name  + "/";
    std::filesystem::create_directories(stats_path + "maps/");
}

void SolverStats::init_stats(nlohmann::json& stats, string name){
    for (auto& [level, json] : stats_dict.items()) {
        for (auto& [stat, descr]: json.items()){
            stats[level][stat] = 0;
        }
    }
    stats["static"]["graph_type"] = graph_name;
    stats["static"]["CH"] = do_CH ?true:false;
    stats["static"]["benchmark"] = name;

}

SolverStats::SolverStats(string graph_name, bool do_CH, int limit, nlohmann::json& stats_dict, string custom_graph_name) :
    stats_dict(stats_dict),
    graph_name(graph_name),
    custom_graph_name(custom_graph_name),
    do_CH(do_CH), limit(limit){
    base_path = "../stats/";
    init_stats(all_stats, "all");
};



string SolverStats::get_stats_filepath(){
    string CH_str = game_folder_stats["static"]["CH"].get<bool>()? "-CH" : ""; 
    string limit_str = limit < UINT_MAX ? "_" + std::to_string(limit) : "";
    string custom_ending = custom_graph_name.size()> 0 ? "#" +  custom_graph_name : "";
    cout << "custom ending: " << custom_ending << endl;
    string filepath = stats_path + game_folder_stats["static"]["graph_type"].get<string>() + CH_str + limit_str + custom_ending + ".json";
    return filepath;
}

