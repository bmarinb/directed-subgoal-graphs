#include "boost/program_options.hpp"
#include <string>
#include <iostream>
#include <vector>
#include <utility> 
#include <fstream>
#include <assert.h>
#include "manager.hpp"

namespace po = boost::program_options;


/* 
Main
Receives command line arguments and starts program workflow.
Command line arguments.
 */

int main(int argc, char const *argv[])
{

    string root_folder, map_name, custom_map_name, custom_graph_name, game_name, ch_params_path, avoidance_params_path, stats_file_path;
    int n_problem, gtype, variant, first_map, sub_algorithm, debug_node;
    uint n_maps;
    bool debug_connection, solve, save_jsons, do_CH, rotate_map=false, save_stats, SG_from_scratch, CH_from_scratch, clrs_from_scratch;
    int dsg_default;
    #ifdef ONLY_UNDIRECTED
        dsg_default = SG;
    #else
        MERGED(
            dsg_default = DSG);
        NOT_MERGED(dsg_default = JP;);  
    #endif
    vector<Pos> start, goal;
    po::options_description options("Allowed options");
    options.add_options()
        ("help,h", "Produce help message")
        ("root_folder,R", po::value<string>(&root_folder)->default_value("../MovingAI/"), "Root folder to MovingAI benchmark")
        ("benchmark_name,B", po::value<string>(&game_name), "Specific benchmark folder name")
        ("map_name,M", po::value<string>(&map_name), "Map name (from benchmark)")
        ("custom_map_name,c", po::value<string>(&custom_map_name), "Custom Map name (.npy or .map")
        ("n_problem,n", po::value<int>(&n_problem)->default_value(-1), "ID of a specific problem to solve")
        ("start,S", po::value<vector<Pos>>(&start)->multitoken(), "Start position of a single problem")
        ("goal,G", po::value<vector<Pos>>(&goal)->multitoken(), "Goal position of a single problem")
        ("graph_type,T", po::value<int>(&gtype)->default_value(dsg_default), "Graph type:\n0: SG\n1: JP\n2: JPD\n3: DSG\n4: DSG (only diagonal subgoals)\n5: Grid Graph")
        ("debug_connection,d", po::value<bool>(&debug_connection)->default_value(false), "Produce output for connection")        
        ("debug_node,D", po::value<int>(&debug_node)->default_value(-1), "Produce output for specific node")
        ("solve,W", po::value<bool>(&solve)->default_value(true), "To solve instances")
        ("limit,l", po::value<uint>(&n_maps)->default_value(UINT_MAX), "Number of maps to solve in each benchmark")
        ("variant,v", po::value<int>(&variant)->default_value(DSG_2_CLR), "Variant of the algorithm")
        ("save_jsons,s", po::value<bool>(&save_jsons)->default_value(false), "Save poblem info")
        ("search_algorithm,A", po::value<int>(&sub_algorithm)->default_value(0), "Search algorithm (for query phase)")
        ("CH,C", po::value<bool>(&do_CH)->default_value(false), "Use Contraction hierarchies")
        ("first map to search,f", po::value<int>(&first_map)->default_value(0), "First map to search (lexicographically ordered")
        ("ch_params_path,p", po::value<string>(&ch_params_path)->default_value("./params/ch_params.json"), "Path to json file with CH params")
        ("avoidance_params_path,a", po::value<string>(&avoidance_params_path)->default_value("./params/smart_avoid.json"), "Path to json file with avoidance params")
        // ("rotate_map,r", po::value<bool>(&rotate_map)->default_value(false), "Rotate map")
        ("json_stats_file,j", po::value<string>(&stats_file_path)->default_value("./params/stats_to_capture.json"), "Path to json file with stats to capture.")
        ("save_stats, u", po::value<bool>(&save_stats)->default_value(true), "Save stats")
        ("compute_sg_from_scratch,x", po::value<bool>(&SG_from_scratch)->default_value(false), "Compute SG from scratch")
        ("compute_ch_from_scratch,y", po::value<bool>(&CH_from_scratch)->default_value(false), "Compute CH from scratch")
        ("compute_clearances_from_scratch,z", po::value<bool>(&clrs_from_scratch)->default_value(false), "Compute clearances from scratch")
        ("use_special_identifier_for_graph,i", po::value<string>(&custom_graph_name), "Special identifier")
    ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, options), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << options << "\n";
        return 1;
    }

    Manager(
        root_folder, game_name, map_name, custom_map_name, custom_graph_name, ch_params_path, avoidance_params_path, stats_file_path, gtype, variant, sub_algorithm, n_problem, n_maps, first_map, debug_node, debug_connection, save_jsons, do_CH, rotate_map, solve, save_stats, start, goal, SG_from_scratch ,CH_from_scratch ,clrs_from_scratch); 
        
    return 0;
}
