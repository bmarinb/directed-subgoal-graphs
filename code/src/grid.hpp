#ifndef GRID_H
#define GRID_H
#include "clrs.hpp"
/* 
Grid
Grid exploration. Check freespace reachability relations and direct paths.
*/


class Grid2d{
    public:
    Grid2d(Mapa* mapa);
    Grid2d(Clearance* clrs);
    int free_space_reachable(const APos& a,const APos& b);
    bool check_diagonal_first(const APos& a,const APos& b);
    bool check_square_0(const APos& a,const APos& b);
    float try_sq0_path(const APos& a,const APos& b);
    float try_DF_path_map(const APos& start, const APos& goal);
    float try_DF_path_clrs(const APos& start, const APos& goal);
    Clearance* clrs;

    private:
    Mapa* mapa;
    APos diag, dif, card1, card2;
    bool big_axis, unblocked;
    Pos mov1_len, mov2_len, u;
    Dir diag_index, card_index;
    PosIndex start_index, jump_index, diag_pos_index;
    PosIndex c1_index, c2_index, c1, c2;
};


class FreespaceExplorer{
    public:
    template <class Value>
    Value octile_distance(const APos& pos1, const APos& pos2){
        dx = std::abs((signed) (pos1[0]-pos2[0]));
        dy = std::abs((signed) (pos1[1]-pos2[1]));
        if (dx >= dy)
        {
            return dx + dy*r2m1;
        } 
        return dy + dx*r2m1;
    }

    template<class Value>
    inline Value octile_distance(int x1, int x2, int y1, int y2){
        dx = std::abs((x1-y1));
        dy = std::abs((x2-y2));
        if (dx>=dy)
        {
            return dx + dy*r2m1;
        } 
        return dy + dx*r2m1;
    }


    private:
        float r2m1{0.41421356237};
        Pos dx;
        Pos dy;
};


#endif