
#ifndef BINARYHEAP_HPP_
#define BINARYHEAP_HPP_


#include <assert.h>
#include <vector>
#include <algorithm>
#include <string>

/* 
Implementation of a min-heap with decrease key operation.
Uses Payload and Value for each item, where Payload is the item to be prioritized and Value is its priority.
Uses indexes to track items position in the heap.
Uses an empty first element to facilitate index operations.
*/


// Constant to store the status of each item
const uint32_t EXTRACTED = -2;
const uint32_t UNVISITED = -1;
const uint32_t IN_QUEUE = -5; // Les than IN_QUEUE means that the item is in the queue.


// Item, with its identifier (Payload) and its value (Value)
template <class Payload, class Value>
struct Item{
    Payload id;
    Value value;
    Item(Payload id=-1, Value value=10) : id(id), value(value){};

};


// Binary heap (min heap).
template <class Payload, class Value>
class BinaryHeap {
public:
    
    std::vector<uint32_t> indexes;
    std::vector<Item<Payload, Value>> heap;


    // Initilizes a binary heap. Preallocates indexes and heaps according to max_size
    BinaryHeap(size_t max_size=0)
    : max_size(max_size), heap_size(0), n_percolations(0)
    {   
        if (std::numeric_limits<Value>::has_infinity){
            inf = std::numeric_limits<Value>::infinity();
            epsilon = 0.001;
        } else {
            inf = -1;
            epsilon = 1;
        }
        indexes.resize(max_size, UNVISITED);
        heap.resize(max_size+1);
        }


    // Initilizes a binary heap according to a vector of pairs of payloads and values.
    BinaryHeap(vector<std::pair<Payload, Value>> pairs)
    : n_percolations(0), heap_size(pairs.size())
    {
        if (std::numeric_limits<Value>::has_infinity){
            inf = std::numeric_limits<Value>::infinity();
            epsilon = 0.001;
        } else {
            inf = -1;
            epsilon = 1;
        }        
        indexes.resize(pairs.size());
        heap.resize(pairs.size()+1);
        max_size = heap_size;
        for (size_t i = 1; i < heap.size(); i++)
        {
            heap[i].id = pairs[i-1].first;
            heap[i].value = pairs[i-1].second;
            indexes[heap[i].id] = i; 
        }
        for (size_t i = heap.size()/2; i > 0  ; i--)
        {
            sink_down(i);
        }
    }


    // Initilizes a binary heap according to a vector of weights assuming the index in the vector are the identifiers.
    BinaryHeap(vector<Value> weights)
    : n_percolations(0)
    {
        if (std::numeric_limits<Value>::has_infinity){
            inf = std::numeric_limits<Value>::infinity();
            epsilon = 0.001;
        } else {
            inf = -1;
            epsilon = 1;
        }        heap.resize(weights.size()+1);
        heap_size = weights.size();
        max_size = heap.size();
        indexes.resize(weights.size());
        for (size_t i = 1; i < heap.size(); i++)
        {
            heap[i].id = i-1;
            heap[i].value = weights[i-1];
            indexes[i-1] = i;
        }
        for (size_t i = heap.size()/2; i > 0  ; i--)
        {
            sink_down(i);
        }
    }


    // Returns queue information as string.
    void print_queue(){
        cout << "Displaying heap." << endl << "Size: " << this->size() << endl;
        for (size_t i = 0; i < this->size(); i++)
        {
            cout << "Id: " << heap[i+1].id << ". Value: " << heap[i+1].value << " index: " << indexes[heap[i+1].id] <<  endl;
        }
        
    }

    uint index, parent, min_index, left, right,max_size, heap_size{0}, aux_index;
    uint n_percolations{0};
    Value old_value;
    Payload aux_id;
    Value aux_value, inf, epsilon;
    

    // Reset indexes and size without reallocation.
    void reset_queue(){
        memset(&indexes[0], -1, sizeof(indexes[0])*max_size);
        heap_size = 0;
        n_percolations = 0;
    }


    // Inserts an item without performing complete swappings. The inserted value is copied only in its final position.
    void insert(const Payload& payload, const Value& value) {        
        heap_size ++;
        heap[heap_size].id = payload;
        heap[heap_size].value = value; 
        indexes[payload] = heap_size;
        bubble_up_aux(heap_size);
    }

    // Assigns the payload and value corresponding to the min element.
    inline void top(Payload& payload, Value& value) const {
        value = heap[1].value;
        payload = heap[1].id;
    }


    // Extracts the minimum element while assigning its content to payload and value.
    inline void extract_min(Payload& payload, Value& value){
        value = heap[1].value;
        payload = heap[1].id;
        pop();
    }


    // Returns the priority of a given payload.
    inline NodeWeight get_value(Payload& payload) const {
        return heap[indexes[payload]].value;
    }


    // Update all priorities in a queue given by new_values
    void update_all(vector<Value>& new_values){
        for (size_t i = 0; i < new_values.size(); i++)
        {
            if (indexes[i]<heap.size()) heap[indexes[i]].value = new_values[i];
        }
        for (size_t i = heap.size()/2; i > 0  ; i--)
        {
            sink_down(i);
        }
        check();
    }


    // Empty heap. This occurs when size is 1.
    inline bool empty() const {
        return heap.size()==1;
    }

    inline size_t size() const {
        return heap_size;
    }


    // Pop the last Item  performing complete swappings. The inserted value is copied only in its final position.
    inline void pop() {
        if (heap_size > 1){
            indexes[heap[1].id] = EXTRACTED;
            aux_value = heap[heap_size].value;
            aux_id = heap[heap_size].id;
            swap_aux(1, heap_size);
            heap_size --;
            sink_down_aux_from_top();
        } else{
            indexes[heap[1].id] = EXTRACTED;
            heap_size --;
        }
    }


    // Return true if the id was extracted
    inline bool was_extracted(Payload id){
        return indexes[id] == EXTRACTED;
    }


    // Return true if the id is in queue
    inline bool in_queue(Payload id){
        return indexes[id] < IN_QUEUE;
    }


    // Return true if the was extracted or if it is in queue. This excludes unvisited items/
    inline bool extracted_or_in_queue(Payload id){
        return indexes[id] <= EXTRACTED;
    }


    // Returns value of the minimum item.
    inline Value min(){
        return (heap_size > 0) ?  heap[1].value:inf;
    }


    // Returns id of the minimum item.
    inline Payload min_id(){
        return heap[1].id;
    }


    // Checks heap property.
    void check(){
        if (size() > 1)
        {
            for (size_t index = 2; index < heap_size ; index++) {
                parent = parent_of(index);
                assert(!less(index, parent));
                assert(indexes[heap[index].id] == index);
            }
        }
    }


    // Decrease key operation. It assumes new_value is passed correctly.
    inline void decrease_key(Payload payload, Value new_value) {    
        heap[indexes[payload]].value = new_value;
        bubble_up_aux(indexes[payload]);
    }


    // Increase heap operation. It assumes new_value is passed correctly.
    inline void increase_key(Payload payload, Value new_value) {    
        heap[indexes[payload]].value = new_value;
        sink_down(indexes[payload]);
    }    


    // Update key and returns True if key was updated, False if the remains the same.
    bool update_key(Payload payload, Value new_value){
        old_value =  heap[indexes[payload]].value;
        if (new_value > old_value + epsilon){
            increase_key(payload, new_value);
        } 
        else if (new_value < old_value - epsilon)
        {
            decrease_key(payload, new_value);
        } else {
            return false;
        }
        return true;
        
    }

private:


    // Comparison between values.
    inline bool less(uint32_t i1, uint32_t i2) const {
        return heap[i1].value < heap[i2].value;
    }


    // Comparison (is aux lower than) between auxiliar element and value.
    inline bool aux_lower(uint32_t i2) const{
        return aux_value < heap[i2].value  ;
    }
    

    // Comparison (is aux greater than) between auxiliar element and value.
    inline bool aux_higher(uint32_t i2) const{
        return heap[i2].value < aux_value ;
    }


    // Returns parent's index of child_index.
    inline uint32_t parent_of(uint32_t child_index) {
        return child_index>>1;
    }

    
    // Returns left child's index of index.
    inline uint32_t left_child_of(uint32_t parent_index) {
        return parent_index<<1;
    }


    // Returns right child's index of index.
    inline uint32_t right_child_of(uint32_t parent_index) {
        return (parent_index<<1) + 1;
    }


    // Bubble up opperation.
    void bubble_up(uint32_t index) {
        parent = parent_of(index);
        while (index > 1 && less(index, parent)) {
            swap(index, parent);
            index = parent;
            parent = parent_of(index);
        }
    }


    // Bubble up opperation. Optimized versions that only performs one swapping of the bubbled up item.
    void bubble_up_aux(uint32_t index) {
        parent = parent_of(index);
        aux_id = heap[index].id;
        aux_value = heap[index].value;
        aux_index = index;
        while (index > 1 && aux_lower(parent)) {
            swap_aux(index, parent);
            index = parent;
            parent = parent_of(parent);
        }
        if (aux_index != index){
            heap[index].id = aux_id;
            heap[index].value = aux_value;
            indexes[aux_id] = index;
        }
    }


    // Sink down opperation. Optimized versions that only performs one swapping of the sinkned down item.
    void sink_down_aux_from_top(){
        aux_id = heap[1].id;
        aux_value = heap[1].value;
        index = 1;
        while (true) {
            min_index = index;
            left = left_child_of(index);
            right = right_child_of(index);
            if (right <= heap_size && less (right, left)){
                min_index = right;
            } else if (left<= heap_size){
                min_index = left;
            } 
            if (min_index != index && aux_higher(min_index)){
                swap_aux(index, min_index);
                index = min_index;
            } else break;
        }

        if (index != 1){
            heap[index].id = aux_id;
            heap[index].value = aux_value;
            indexes[aux_id] = index;
        }
    }


    // Sink down operation.
    void sink_down(uint32_t index) {
        while (true) {
            min_index = index;
            left = left_child_of(index);
            right = right_child_of(index);
            if (left <= heap_size && less(left, min_index)) {
                min_index = left;
            }
            if (right <= heap_size && less(right, min_index)) {
                min_index = right;
            }
            if (min_index == index) {
                break;
            }
            swap(index, min_index);
            index = min_index;
        }
    }


    // Swap two items. Auxiliar value will be copied only at the end of the current operation (bubble up or sink down).
    inline void swap_aux(uint32_t i1, uint32_t i2) {
        heap[i1].id = heap[i2].id;
        heap[i1].value = heap[i2].value;
        indexes[heap[i1].id] = i1;
        n_percolations++;
    }


    // Swap two items.
    inline void swap(uint32_t i1, uint32_t i2) {
        aux_id = heap[i1].id;
        aux_value = heap[i1].value;
        heap[i1].id = heap[i2].id;
        heap[i1].value = heap[i2].value;
        heap[i2].id = aux_id;
        heap[i2].value = aux_value;
        indexes[heap[i1].id] = i1;
        indexes[heap[i2].id] = i2;
        n_percolations++;
    }
};

#endif

