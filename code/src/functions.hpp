#ifndef FUNCTIONS_H
#define FUNCTIONS_H
#include <vector>
#include <array>
#include <string>
#include <fstream>
#include <iostream>
#include <chrono>
#include <math.h>
#include <cstdlib>
#include <numeric>
#include "nlohmann/json.hpp"
#include <filesystem>
#include <sys/resource.h>
#include "boost/unordered_map.hpp"
#include "robin_hood.hpp"
#include <unordered_set>


/* 
Functions
General-purpose functions
*/

namespace fs = std::filesystem;


// Verbose mode 1.
#ifdef VERBOSE1
#define V1(x) x
#else
#define V1(x) do {} while (0)
#endif


// Verbose mode 2. Is more verbose than verbose 1.
#ifdef VERBOSE2
#define V2(x) x
#else
#define V2(x) do {} while (0)
#endif



// Only subgoal graphs with diagonal and cardinal nodes (merged). Used for high performance evaluation.
#ifdef ONLY_MERGED
#define MERGED(x) x
#define NOT_MERGED(x) do {} while (0)
#else
#define NOT_MERGED(x) x
#define MERGED(x) do {} while (0)
#endif


// Only SG is undirected. Used for high performance evaluation.
#ifdef ONLY_UNDIRECTED
#define UNDIRECTED(x) x
#define DIRECTED(x) do {} while (0)
#else 
#define DIRECTED(x) x
#define UNDIRECTED(x) do {} while (0)
#endif 


using std::vector;
using std::string;
using std::cout;
using std::cin;
using std::array;

// Algorithms
const int DIJKSTRA = 0;
const int ASTAR = 1;
const int BID_SEARCH = 2;


// A*
const int DEFAULT_ALGORITHM = 0;
const int FIRST_ASTAR = 1;
const int ASTAR_VANILLA = 1;
const int ASTAR_VISITED = 2;
const int ASTAR_POS_F = 3;
const int ASTAR_AVOID_VISITED = 4;
const int ASTAR_AVOID_POS_F = 5;
const int GRID_ASTAR = 6;


// A* variants for SG 
const int VISIT_ALL = 0;
const int AVOID_VISITED_POSITIONS = 1;
const int AVOID_POSITIONS_HIGHER_F = 2;


// Bidirectional search
const int FIRST_BID_DIJKSTRA = 10;
const int BID_DIJKSTRA_WITHOUT_STALL = 10;
const int BID_DIJKSTRA_WITH_STALL = 11;
const int BID_DIJKSTRA_AVOIDANCE = 12;
const int LAST_BID_DIJKSTRA = 12;
const int FIRST_BID_ASTAR = 13;
const int BID_ASTAR_WITH_STALL = 13;
const int BID_ASTAR_AVOIDANCE = 14;


// Bidirectional search criterions
const int MIN_MIN = 0;
const int MIN_SIZE = 1;
const int ALTERNATE = 2;


// Dijkstra
const int DIJKSTRA_SKIPPING_V = 20;
const int POSITIONAL_DIJKSTRA = 21;
const int DIJKSTRA_VANILLA = 22;


// Types of grid exploration. Counterclockwise (CCW) and Clockwise (CW)
const int CCW = 0;
const int CW = 1;


// Variant of subgoal graph. Currently not supported.
const int DSG_2_CLR = 0;
const int DSG_3_CLR = 1;
const int DSG_4_CLR = 2;


// Graph/search direction.
const int FORWARD = 0;
const int BACKWARD = 1;


// Constants
extern float sin45;
extern float r2;
extern float r2p;
extern int width;
extern float F_MUL;
extern string endl;
extern string placeholder;


// Graph types.
inline constexpr int SG = 0;
inline constexpr int JP = 1;
inline constexpr int JPD = 2;
inline constexpr int DSG = 3;
inline constexpr int DSG_NOT_MERGED = 4;
inline constexpr int GRID_GRAPH = 5;
inline constexpr int RIGHT = -1;
inline constexpr int LEFT = 1;


// Type definitions
typedef float NodeWeight;
typedef double EdgeCost;
typedef uint NodeId;
typedef uint EdgeId;
typedef int Pos;
typedef double AstarCost;
typedef unsigned long long AstarUint;
typedef uint8_t Bool;
// CLR differs from Pos in that CLR could be limited to a lower value in order to optimize memory
typedef uint8_t CLR;
typedef uint8_t IsNodeDir;
typedef signed char Dir;
// Pos index is int because sometimes we need to substract indexes and may lead to negative values;
typedef int PosIndex;
typedef int CLR_TYPE;
typedef array<Pos, 2> APos;


// Directions struct. 
struct Directions{
    // Directions as vectors
    array<APos, 8> arrays;
    // Direction indexes
    array<uint, 9> indexes;
    // Direction names 
    array<string, 9> names;
    // Direction deltas to move in the linearized grid.
    array<int, 8> dindex;
    Directions(
        array<APos, 8> _directions = 
        {{{1, 0}, {1, 1}, {0, 1}, {-1, 1}, {-1, 0}, {-1, -1}, {0, -1}, {1,-1}}}, 
        array<string, 9>  _names = {"S", "SE", "E", "NE", "N","NW",  "W", "SW", "All"});
    void update_dindex();
    Dir arr2index(const APos& vec);
    Dir arr2index(int x, int y);
    string arr2names(const APos& vec);
};


extern Directions dirs;
inline constexpr Pos MAX_CLR = 999999;


// Types of freespace reachability relations used in this work.
const EdgeId DIAGONAL_FIRST = -1;
const EdgeId CARDINAL_FIRST = -2;
const EdgeId SQUARE_0 = -3;
const EdgeId SQUARE_1 = -4;
const EdgeId NO_BASIC_REACHABILITY = -5;


// Vector as a string.
string v2s (const vector<string>& arr, bool reversed=false);


// Returns true if file exists.
inline bool file_exists(string path){
    return std::filesystem::exists(std::filesystem::path(path));
}


// Returns index in the linearized array of pos.
template <class T>
inline PosIndex mindex(const T& pos){
        return pos[1] + pos[0]*width;
}


// Returns index in the linearized array of (i,j)
template <class num>
inline PosIndex mindex(num i, num j){
    return j + i*width;
}


// Returns index in a linearized space of the grid cells and directions. Used in unordered_map
inline PosIndex umap_index(const vector<Pos>& pos, uint dir_index){
    return pos[0]*width*8 + pos[1]*8 + dir_index;
}


// Returns index in a linearized space of the grid cells and directions. Used in unordered_map
inline PosIndex umap_index(PosIndex pos_index, uint dir_index){
    return pos_index*8 + dir_index;
}


// Returns index in the linearized array of (A,B) using width w.
template <class N, class M>
int get_index( N A,  M B,  uint w){
    return B + A*w;
};


// Returns index in the linearized array of arr using width w.
template <class T>
int get_index(const T& arr,  uint w){
    return arr[1] + arr[0]*w;
};


// Float division.
template <class Value, class num1, class num2>
inline Value divfloat(num1 n, num2 d ){
    return (float) n / (float) d;
}


// Average of elements of a vector
template <class T>
float average(vector<T> v1){
    return 
    divfloat<float>(std::accumulate(v1.begin(), v1.end(), 0.0f), v1.size());
}


// Sum of elements of a vector
template <class T>
float suma(vector<T> v1){
    return std::accumulate(v1.begin(), v1.end(), 0.0f);
}


// Sign
template <typename T>  inline int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}



// Vector as a string.
template <typename T>
string v2s(const T& arr, bool reversed=false){
    if (arr.size() == 0) return "()";
    string str = "(";
    if (reversed){
        for (auto it = arr.rbegin(); it != arr.rend(); ++it){
            str += std::to_string(*it) + ",";
        }
    } else{
        for (auto it = arr.begin(); it != arr.end(); ++it){
            str += std::to_string(*it) + ",";
        }  
    }
    if (arr.size()> 0){
        str = str.substr(0, str.size()-1) + ")";
    }
    return str;
}


// Vector as a string.
string v2s(const APos& arr);


// Octile distance.
template<class Value>
inline Value octile_distance(const APos& pos1, const APos& pos2){
    Pos dx = std::abs((signed) (pos1[0]-pos2[0]));
    Pos dy = std::abs((signed) (pos1[1]-pos2[1]));
    if (dx>dy)
    {
        return dx + dy*r2p;
    } 
    return dy + dx*r2p;
}


// Number of movements in diagonal and cardinal directions needed to move from pos1 to pos2 in the freespace.
array<Pos, 2> octile_moves(const APos& pos1, const APos& pos2);



// Octile distance using separated values.
template<class Value>
inline Value octile_distance(int x1, int x2, int y1, int y2){
    int dx = std::abs((x1-y1));
    int dy = std::abs((x2-y2));
    if (dx>dy)
    {
        return dx + dy*r2p;
    } 
    return dy + dx*r2p;
}


// Euclidian distance between two positions
template<class Value>
Value euclidean_distance(const vector<Pos>& pos1, const vector<Pos>& pos2){
    return sqrt(pow((signed)(pos1[0]-pos2[0]), 2) + pow((signed)(pos1[1] - pos2[1]), 2));
}


// Manhattan distance between two positions
template<class Value>
Value manhattan_distance(const APos& pos1, const APos& pos2){
    return (Value) std::abs((signed) (pos1[0]-pos2[0])) + std::abs((signed) (pos1[1]-pos2[1]));
}


// Manhattan distance between two positions. Uses separated elements.
template<class Value>
Value manhattan_distance(Pos x1, Pos y1, Pos x2, Pos y2){
    return (Value) std::abs((signed) (x1-x2)) + std::abs((signed) (y1-y2));
}


// Number of bits that are needed to represent the heuristic in a grid graph with size map_size.
uint bits_for_heur(vector<Pos> map_size);


// Number of bits that are needed to represent the g-value in a grid graph with size map_size.
uint bits_for_g(vector<Pos> map_size);


// Minimum element that is not zero.
template <class T>
T min_not_zero(T n1, T n2){
    if (n1 > 0)
    {
        if (n2 > n1 || n2 == 0)
        {
            return n1;
        }
    }
    return n2;
}


// Minimum element that is not zero.
template <class T>
T min_not_zero(T n1, T n2, T n3){
    return min_not_zero(min_not_zero(n1, n2), n3);
}


string get_extension(string str);


string get_prefix(string str);


// Linearized index to position (2-dimensional array).
inline APos i2p(PosIndex index){
    return APos({index/width, index%width});
};


// Get X component from a linearized index
inline Pos get_x(PosIndex index){
    return index/width;
};


// Get Y component from a linearized index
inline Pos get_y(PosIndex index){
    return index%width;
}


// Return true if two vectors are equal.
template<class T>
bool vector_equal(const vector<T>& v1, const vector<T>& v2){
    if (v1.size() != v2.size()) return false;
    bool equal = true;
    for (size_t i = 0; i < v1.size(); i++)
    {
        if (v1[i] != v2[i]){
            return false;
        } 
    }
    return true;
}


// Return true if two vectors of size 2 are equal.
inline bool vector_equal(const APos& v1, const APos& v2){
    return v1[0] == v2[0] and v1[1] == v2[1];
}


// Get then n-th byte from a number num.
inline bool get_nth_byte(IsNodeDir num, Dir n){
    return (num>>n) & 1;
}


// Return true if the direction is cardinal (even)
inline bool is_cardinal(Dir dir_index){
    return dir_index%2==0;
}


// Return true if the direction is diagonal (odd)
inline bool is_diagonal(Dir dir_index){
    return dir_index%2==1;
}


// Returns the index of a direction after rotating it offset times. Each rotation equals to 45° degrees.
inline Dir dir_offset(Dir dir, int offset){
        return (Dir) ((int) dir + offset)&7;
}


// Save json to filepath
void save_json(const nlohmann::json& json ,string filepath);


// Save json from filepath
bool load_json(nlohmann::json& json, string filepath);


// Set a timer
std::chrono::_V2::system_clock::time_point set_timer();

// End timer and return elapsed time
int end_timer(std::chrono::_V2::system_clock::time_point start);


// Plus equal (+=) operation for json items.
template<class T>
void plus_equal_json(nlohmann::json& json, T plus){
    json = json.get<T>() + plus;
}


// Multiply a vector by a scalar
template<class T>
inline vector<T> multiply(const vector<T>& v1, int scalar){
    return vector<T> {v1[0]*scalar, v1[1]*scalar};
}


// Multiply an 2-d array by a scalar
inline APos multiply(const APos& v1, int scalar){
    return APos {v1[0]*scalar, v1[1]*scalar};
}


// Add two vectors. Safe function.
template <class T>
vector<T> add_vectors(const vector<T>& v1, const vector<T>& v2){
    assert(v1.size() == v2.size());
    return vector<T>{v1[0] + v2[0], v1[1] + v2[1]};
}


// Add two 2-d arrays.
inline APos add_vectors(const APos& v1, const APos& v2){
    assert(v1.size() == v2.size());
    return APos{v1[0] + v2[0], v1[1] + v2[1]};
}


// Get memory usage.
long get_mem_usage();


// Returns true if "looked" is in vector vec.
template <class T> 
bool in_vector(T looked, vector<T> vec){
    for (T item: vec){
        if (item ==looked ) return true;
    }
    return false;
}


// Returns a pair as a string.
template <class C1, class C2>
string pair_string(std::pair<C1, C2> pr){
    string out = "(" + std::to_string(pr.first) + ", " + std::to_string(pr.second) + ")";
    return out;
}


// Returns a vector with all the directions of subggoals present in a cell.
void dirs_presence(IsNodeDir cell_info, vector<Dir>& dirs_present);


// Returns a vector with all the name of the directions of subggoals present in a cell.
string node_presence(IsNodeDir cell_info);


// Returns the name of a vector of directions as string.
string dirs_to_names(const vector<Dir>& direcs);
#endif

