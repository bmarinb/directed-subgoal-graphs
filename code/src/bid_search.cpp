#include "search.hpp"

/* 
Functions used to run different bidirectional search implementations
Bidirectional Dijkstra
Bidirectional A*
*/


// Bidirectional A*
void Search::bid_astar(){
    bid_init_problem_astar();
    while (search_radius(0) < best_cost or search_radius(1) < best_cost)
    {
        if (not choose_open_and_expand_astar()) break;
        check_stall_astar();
        check_frontier_astar();
        if (!can_stall) bid_relax_edges_astar();
    }
}


// Bidirectional A* with avoidance
void Search::bid_astar_avoidance(){
    bid_init_problem_astar();
    while (search_radius(0) < best_cost or search_radius(1) < best_cost)
    {
        if (not choose_open_and_expand_astar()) break;
        check_stall_astar();
        check_frontier_astar();
        if (!can_stall) bid_relax_edges_astar_avoidance();
    }
}


// Bidirectional Dijkstra with avoidance
void Search::bid_dijkstra_avoidance(){
    bid_init_problem_dijkstra();
    while (opens[0].min() < best_cost or opens[1].min() < best_cost)
    {
        if (not choose_open_and_expand()) break;
        check_stall_dijkstra();
        check_frontier_dijkstra();
        if (!can_stall) bid_relax_edges_dijkstra_avoidance();
    }
    // refine_path(best_node, best_cost);
}


// Bidirectional Dijkstra
void Search::bid_dijkstra(){
    bid_init_problem_dijkstra();
    while (opens[0].min() < best_cost or opens[1].min() < best_cost)
    {
        if (not choose_open_and_expand()) break;
        check_stall_dijkstra();
        check_frontier_dijkstra();
        if (!can_stall) bid_relax_edges_dijkstra();
    }
    // refine_path(best_node, best_cost);
}


// Bidirectional Dijkstra without stall
void Search::bid_dijkstra_wo_stall(){
    bid_init_problem_dijkstra();
    while (opens[0].min() < best_cost or opens[1].min() < best_cost)
    {
        if (not choose_open_and_expand()) break;
        check_frontier_dijkstra();
        bid_relax_edges_dijkstra();
    }
    // refine_path(best_node, best_cost);
}


// Init dijkstra
void Search::bid_init_problem_dijkstra(){
    opens[0].insert(problem->start_id, 0);
    opens[1].insert(problem->goal_id, 0);
    bi_g[0][problem->start_id] = 0;
    bi_g[1][problem->goal_id] = 0;
}


// Init A*
void Search::bid_init_problem_astar(){
    opens_astar[0].insert(problem->start_id, 0);
    opens_astar[1].insert(problem->goal_id, 0);

    goal_x = sgs[0][problem->goal_id].get_x();
    goal_y = sgs[0][problem->goal_id].get_y();
    start_x = sgs[0][problem->start_id].get_x();
    start_y = sgs[0][problem->start_id].get_y();
    bi_g[0][problem->start_id] = 0;
    bi_g[1][problem->goal_id] = 0;
    bi_f[0][problem->start_id] = 0;
    bi_f[1][problem->goal_id] = 0;
}
 

// Choose open and expand A*. Criterion is fixed to min-min
bool Search::choose_open_and_expand_astar(){
    sel_open = opens_astar[1].min() < opens_astar[0].min(); 

    if (search_radius(sel_open) > best_cost) {
        sel_open = 1 - sel_open;
        if (search_radius(sel_open) > best_cost) return false;
    }
    aux_sel_open = 1 - sel_open;
    opens_astar[sel_open].extract_min(u_id, u_f_cost);
    u_cost = bi_g[sel_open][u_id];
    V2(bid_expansions[sel_open].push_back(u_id));
    V2(if(verbose) cout << "Extraction " << n_expansions << ((sel_open==FORWARD)?" FORWARD":" BACKWARD")  <<  " cost: " << u_cost  << " " << bid_node_info(u_id)  << endl);
    return true;
}


// Choose open and expand Dijkstra. Criterion is fixed to min-min
bool Search::choose_open_and_expand(){
    sel_open = opens[1].min() < opens[0].min(); 
    if (opens[sel_open].min() > best_cost) {
        sel_open = 1 - sel_open;
        if (opens[sel_open].min() > best_cost) return false;
    }
    aux_sel_open =  1 - sel_open;
    opens[sel_open].extract_min(u_id, u_cost);
    V2(bid_expansions[sel_open].push_back(u_id));
    V2(if(verbose) cout << "Extraction " << n_expansions << ((sel_open==FORWARD)?" FORWARD":" BACKWARD")  <<  " cost: " << u_cost  << " " << bid_node_info(u_id)  << endl);
    return true;
}


// Check if frontiers have met Dijkstra.
void Search::check_frontier_dijkstra(){
    if (opens[aux_sel_open].extracted_or_in_queue(u_id)){
        match_cost = bi_g[aux_sel_open][u_id] + u_cost;
        V2(if(verbose) cout << "Node "<< u_id << " was in the other search. Match cost is: " <<  match_cost << endl);
        if (match_cost < best_cost){
            // Plus epsilon for easier comparision
            best_cost = match_cost + epsilon;
            best_node = u_id;
        }
        V2(if(verbose) cout << "Best node, best cost: " << best_node << ", " << best_cost  << endl);
    }
}


// Check if frontiers have met A*.
void Search::check_frontier_astar(){
    if (opens_astar[aux_sel_open].extracted_or_in_queue(u_id) ){
        match_cost = bi_g[aux_sel_open][u_id] + u_cost;
        V2(if(verbose) cout << "Node "<< u_id << " was in the other search. Match cost is: " <<  match_cost << endl);
        if (match_cost < best_cost){
            // Plus epsilon for easier comparision
            best_cost = match_cost  - epsilon;
            best_node = u_id;
        }
        V2(if(verbose) cout << "Best node, best cost: " << best_node << ", " << best_cost  << endl);
    }
}


// Bid Dijkstra Stall
void Search::check_stall_dijkstra(){
    can_stall = false;
    V2(if(verbose) cout << "Checking stall "  << u_id << ": " << bid_search_nodes[0][aux_sel_open][0][u_id].n_edges << " nbs." << endl);
    for (v_eid = bid_search_nodes[0][aux_sel_open][0][u_id].first_edge; v_eid < bid_search_nodes[0][aux_sel_open][0][u_id].last_edge(); v_eid++)
    {
        UNDIRECTED(v_id = CH_edges[0][v_eid].targets[FORWARD]);
        DIRECTED(v_id = CH_edges[0][v_eid].targets[aux_sel_open]);
        V2(if(verbose) cout << "V id: " << v_id << endl);

        if (opens[sel_open].extracted_or_in_queue(v_id)){
            v_cost = bi_g[sel_open][v_id];
            V2(if(verbose) cout << "U cost: " << u_cost << "> ? V cost: " <<  v_cost << " + Redge cost: " << CH_edges[0][v_eid].cost<< endl);
            if (v_cost  + CH_edges[0][v_eid].cost - epsilon < u_cost){
                V2(if(verbose) cout << "Found stall edge: " << v_id << endl);
                can_stall = true;
                break;
            } 
        }
    }
    V2(if(verbose) cout << ((can_stall)? "Stalling node" : "Expanding node") << endl);
    V2(if (can_stall) id_stalled.push_back(u_id));
    if (can_stall) stalled++;
    else {
        n_expansions++;  
        V2(bid_expansions[sel_open].push_back(u_id));
        if (sel_open == 0) n_expansions_forward++;
    }
}


// Bid A* Stall
void Search::check_stall_astar(){
    can_stall = false;
    V2(if(verbose) cout << "Checking stall "  << u_id << ": " << bid_search_nodes[0][aux_sel_open][0][u_id].n_edges << " nbs." << endl);
    for (v_eid = bid_search_nodes[0][aux_sel_open][0][u_id].first_edge; v_eid < bid_search_nodes[0][aux_sel_open][0][u_id].last_edge(); v_eid++)
    {
        UNDIRECTED(v_id = CH_edges[0][v_eid].targets[FORWARD]);
        DIRECTED(v_id = CH_edges[0][v_eid].targets[aux_sel_open]);
        V2(if(verbose) cout << "V id: " << v_id << endl);
        if (opens_astar[sel_open].extracted_or_in_queue(v_id)){
            v_cost = bi_g[sel_open][v_id];
            V2(if(verbose) cout << "U cost: " << u_cost << "> ? V cost: " <<  v_cost << " + Redge cost: " << CH_edges[0][v_eid].cost<< endl);
            if (v_cost  + CH_edges[0][v_eid].cost - epsilon < u_cost){
                V2(if(verbose) cout << "Found stall edge: " << v_id << endl);
                can_stall = true;
                break;
            } 
        }
    }
    V2(if(verbose) cout << ((can_stall)? "Stalling node" : "Expanding node") << endl);
    V2(if (can_stall) id_stalled.push_back(u_id));
    if (can_stall) stalled++;
    else {
        n_expansions++;  
        V2(bid_expansions[sel_open].push_back(u_id));
        if (sel_open == 0) n_expansions_forward++;
    }
}


// Bid A* relax edges
void Search::bid_relax_edges_astar(){
    for (v_eid = bid_search_nodes[0][sel_open][0][u_id].first_edge; v_eid < bid_search_nodes[0][sel_open][0][u_id].last_edge(); v_eid++)
    {
        DIRECTED(v_id = CH_edges[0][v_eid].targets[sel_open]);
        UNDIRECTED(v_id = CH_edges[0][v_eid].targets[FORWARD]);
        bid_process_edge_astar();
    }
}


// Bid A* relax edges avoidance
void Search::bid_relax_edges_astar_avoidance(){
    for (v_eid = bid_search_nodes[0][sel_open][0][u_id].first_edge; v_eid < bid_search_nodes[0][sel_open][0][u_id].last_edge(); v_eid++)
    {
        V2(cout << "Processing: " << CH_edges[0][v_eid].info() << endl);

        v_id = CH_edges[0][v_eid].targets[sel_open];
        if (!avoidance_table[0][sel_open][v_id]){          
            bid_process_edge_astar();
        } else {
            V2(cout << "Avoiding: " << bid_node_info(v_id) << "-----------------------------------------------------------------------" << endl);
            n_avoided++;
        }            
    }
}


// Bid Dijkstra relax edges
void Search::bid_relax_edges_dijkstra(){
    for (v_eid = bid_search_nodes[0][sel_open][0][u_id].first_edge; v_eid < bid_search_nodes[0][sel_open][0][u_id].last_edge(); v_eid++)
    {
        DIRECTED(v_id = CH_edges[0][v_eid].targets[sel_open]);
        UNDIRECTED(v_id = CH_edges[0][v_eid].targets[FORWARD]);        
        bid_process_edge_dijkstra();
    }
}


// Bid Dijkstra relax edges avoidance.
void Search::bid_relax_edges_dijkstra_avoidance(){
    for (v_eid = bid_search_nodes[0][sel_open][0][u_id].first_edge; v_eid < bid_search_nodes[0][sel_open][0][u_id].last_edge(); v_eid++)
    {
        V2(cout << "Processing: " << CH_edges[0][v_eid].info() << endl);
        v_id = CH_edges[0][v_eid].targets[sel_open];
        if (!avoidance_table[0][sel_open][v_id]){          
            bid_process_edge_dijkstra();
        } else {
            V2(cout << "Avoiding: " << bid_node_info(v_id) << "-----------------------------------------------------------------------" << endl);
            cin >> placeholder;
            n_avoided++;
        }
    }
}


// Bid Dijkstra process edge.
void Search::bid_process_edge_dijkstra(){
    g = u_cost + CH_edges[0][v_eid].cost;
    V2(if(verbose) cout << bid_node_info(v_id) << " Cost: " << g << endl);
    if (g < (bi_g[sel_open][v_id]-epsilon)){
        n_rel_per_exp ++;
        V2(if(verbose) cout << " Adding node to open" << endl);
        bi_parents[sel_open][v_id] = v_eid;
        bi_g[sel_open][v_id] = g;
        if (opens[sel_open].in_queue(v_id)){
            V2(if(verbose) cout << "Node was in open" << endl);
            opens[sel_open].decrease_key(v_id,g);
        }   
        else{
            V2(if(verbose) cout << "New node" << endl);
            opens[sel_open].insert(v_id,g);
        }
    }
    V2( 
        else if (verbose) {
            cout << "Node was not added. old cost: " << bi_g[sel_open][v_id] << endl;
        }
        );
}


// Bid A* process edge.
void Search::bid_process_edge_astar(){
    g = u_cost + CH_edges[0][v_eid].cost;
    h = sel_open ? get_reversed_h() : get_h();
    f_cost_v = (((AstarUint) ((g+h)*(prec))) << (bits_hprec)) + (AstarUint) (h*prec);
    V2(if(verbose) cout  << " " <<  bid_node_info(v_id) << " Cost: " << g << endl);

    V2(printf("F cost: %f + %f = %f (%llX)\n", g, h, g+h, f_cost_v)); 
    if (g < (bi_g[sel_open][v_id]-epsilon)){
        bi_f[sel_open][v_id] = g+h;
        n_rel_per_exp ++;
        V2(if(verbose) cout << " Adding node to open" << endl);
        bi_parents[sel_open][v_id] = v_eid;
        bi_g[sel_open][v_id] = g;
        if (opens_astar[sel_open].in_queue(v_id)){
            V2(if(verbose) cout << "Node was in open" << endl);
            opens_astar[sel_open].decrease_key(v_id,f_cost_v);
        }   
        else{
            V2(if(verbose) cout << "New node" << endl);
            opens_astar[sel_open].insert(v_id,f_cost_v);
        }
    }
    V2( 
        else if (verbose) {
            cout << "Node was not added. old cost: " << bi_g[sel_open][v_id] << endl;
        }
        );
}


// Check if two nodes are neighbors.
bool Search::is_nb(NodeId n1, NodeId n2, int graph_dir){
    for (size_t i = bid_search_nodes[0][graph_dir][0][n1].first_edge; i < bid_search_nodes[0][graph_dir][0][n1].last_edge(); i++)
    {
        if (CH_edges[0][i].targets[FORWARD] == n2) return true;
        else if (CH_edges[0][i].targets[BACKWARD] == n2) return true;
    }
    return false;
}


// Bid search refine path (undirected graphs)
void Search::refine_undirected_half_path(bool gdir){
    parent_id = best_node;
    V2(cout << "Best node: " << best_node << endl);
    ep_id = gdir ? problem->goal_id: problem->start_id;
    while (parent_id != ep_id){
        edge_id = bi_parents[gdir][parent_id];
        parent_id = CH_edges[0][edge_id].targets[BACKWARD];
        ud_unpack_stack.emplace_back(edge_id, false);
        while (not ud_unpack_stack.empty()){    
            unpack_edge = &CH_edges[0][ud_unpack_stack.back().first];
            reversed = ud_unpack_stack.back().second;
            V2(cout << "Processing edge " << ud_unpack_stack.back().first << " " << unpack_edge->info() << " reversed: " << reversed << endl);
            ud_unpack_stack.pop_back();
            if (unpack_edge->e1 > NO_BASIC_REACHABILITY){
                V2(cout << "Pre Adding to path: "  << unpack_edge->targets[reversed? 1- gdir: gdir] << endl);
                V2(problem->id_path.push_back(unpack_edge->targets[reversed? 1- gdir: gdir]));
                if (reversed){
                    actual_index = sgs[0][unpack_edge->targets[BACKWARD]].mindex;
                    previous_index = sgs[0][unpack_edge->targets[FORWARD]].mindex;
                } else{
                    actual_index = sgs[0][unpack_edge->targets[FORWARD]].mindex;
                    previous_index = sgs[0][unpack_edge->targets[BACKWARD]].mindex;

                }
                V2(cout << "Adding subedges from " << v2s(i2p(actual_index)) <<" to "<< v2s(i2p(previous_index))<< endl);
                dx = get_x(previous_index) - get_x(actual_index);
                dy = get_y(previous_index) - get_y(actual_index);
                abs_dx = abs(dx);
                abs_dy = abs(dy);
                if (abs_dx > abs_dy){
                    n_moves_diag = abs_dy;
                    n_moves_card = abs_dx - abs_dy;
                    diag_dindex = dirs.dindex[dirs.arr2index(sgn(dx), sgn(dy))];
                    card_dindex = dirs.dindex[dirs.arr2index(sgn(dx), 0)];
                }
                else {
                    n_moves_diag = abs_dx;
                    n_moves_card = abs_dy - abs_dx;
                    diag_dindex = dirs.dindex[dirs.arr2index(sgn(dx), sgn(dy))];
                    card_dindex = dirs.dindex[dirs.arr2index(0, sgn(dy))];
                }

                if (((EdgeId) (unpack_edge->e1 + gdir)) ==  DIAGONAL_FIRST){
                    for (int j = 0; j < n_moves_card; j++)
                    {
                        actual_index += card_dindex;
                        G_path->push_back(actual_index);
                    }
                    for (int j = 0; j < n_moves_diag; j++)
                    {
                        actual_index += diag_dindex;
                        G_path->push_back(actual_index);
                    }
                }
                else{
                    for (int j = 0; j < n_moves_diag; j++)
                    {
                        actual_index += diag_dindex;
                        G_path->push_back(actual_index);
                    }
                    for (int j = 0; j < n_moves_card; j++)
                    {
                        actual_index += card_dindex;
                        G_path->push_back(actual_index);
                    }
                }
            }
            else{
                V2(cout << "Adding edges " << unpack_edge->e1 << " and " << unpack_edge->e2 << endl);
                if (not reversed){
                    ud_unpack_stack.emplace_back(unpack_edge->e1, true);
                    ud_unpack_stack.emplace_back(unpack_edge->e2, false);
                } else{
                    ud_unpack_stack.emplace_back(unpack_edge->e2, true);
                    ud_unpack_stack.emplace_back(unpack_edge->e1, false);
                }
            }
        }

        V2(cout << "New parent id: " << parent_id << endl);
    }
    return;
}


// Bid search refine path (directed graphs)
void Search::refine_directed_half_path(bool gdir){
    parent_id = best_node;
    ep_id = gdir ? problem->goal_id: problem->start_id;
    while (parent_id != ep_id){
        edge_id = bi_parents[gdir][parent_id];
        unpack_stack.push_back(edge_id);
        while (not unpack_stack.empty()){
            unpack_edge = &CH_edges[0][unpack_stack.back()];
            V2(cout << "Processing edge " << unpack_stack.back() << " " << unpack_edge->info() << endl);
            unpack_stack.pop_back();
            if (unpack_edge->e1 > NO_BASIC_REACHABILITY){
                // G refinement
                actual_index = sgs[0][unpack_edge->targets[gdir]].mindex;
                previous_index = sgs[0][unpack_edge->targets[1-gdir]].mindex;
                V2(problem->id_path.push_back(unpack_edge->targets[1-gdir]));
                V2(cout << "Adding to path: "  << unpack_edge->targets[1-gdir] << endl);
                dx = get_x(previous_index) - get_x(actual_index);
                dy = get_y(previous_index) - get_y(actual_index);
                abs_dx = abs(dx);
                abs_dy = abs(dy);
                if (abs_dx > abs_dy){
                    n_moves_diag = abs_dy;
                    n_moves_card = abs_dx - abs_dy;
                    diag_dindex = dirs.dindex[dirs.arr2index(sgn(dx), sgn(dy))];
                    card_dindex = dirs.dindex[dirs.arr2index(sgn(dx), 0)];
                }
                else {
                    n_moves_diag = abs_dx;
                    n_moves_card = abs_dy - abs_dx;
                    diag_dindex = dirs.dindex[dirs.arr2index(sgn(dx), sgn(dy))];
                    card_dindex = dirs.dindex[dirs.arr2index(0, sgn(dy))];
                }

                if (((EdgeId) (unpack_edge->e1 + gdir)) ==  DIAGONAL_FIRST){
                    for (int j = 0; j < n_moves_card ; j++)
                    {
                        actual_index += card_dindex;
                        G_path->push_back(actual_index);
                    }
                    for (int j = 0; j < n_moves_diag; j++)
                    {
                        actual_index += diag_dindex;
                        G_path->push_back(actual_index);
                    }
                }
                else{
                    for (int j = 0; j < n_moves_diag ; j++)
                    {
                        actual_index += diag_dindex;
                        G_path->push_back(actual_index);
                    }
                    for (int j = 0; j < n_moves_card; j++)
                    {
                        actual_index += card_dindex;
                        G_path->push_back(actual_index);
                    }
                }
            }
            else{
                V2(cout << "Adding edges " << unpack_edge->e1 << " and " << unpack_edge->e2 << endl);
                if (gdir == BACKWARD){
                    unpack_stack.push_back(unpack_edge->e2);
                    unpack_stack.push_back(unpack_edge->e1);
                } else{
                    unpack_stack.push_back(unpack_edge->e1);
                    unpack_stack.push_back(unpack_edge->e2);
                }
            }
        }
        parent_id = unpack_edge->targets[1 - gdir];
        V2(cout << "New parent id: " << parent_id << endl);
    }
}


// Bid search refine path
void Search::bid_search_refine_path(){
    V2(cout << "---------------------------------" << endl);
    V2(cout << "Refining path for problem " << problem->id_problem << endl);
    problem->valid = best_cost < inf;
    G_path = &problem->g_path_indices;
    if (problem->valid) problem->cost = best_cost + epsilon;
    else return;
    DIRECTED(refine_directed_half_path(FORWARD));
    UNDIRECTED(refine_undirected_half_path(FORWARD));
    std::reverse(problem->g_path_indices.begin(), problem->g_path_indices.end());
    V2(std::reverse(problem->id_path.begin(), problem->id_path.end()));
    G_path->push_back(sgs[0][best_node].mindex);
    V2(problem->id_path.push_back(best_node));
    V2(cout << "--------------------------------\nProcessing backward path" << endl);
    DIRECTED(refine_directed_half_path(BACKWARD));
    UNDIRECTED(refine_undirected_half_path(BACKWARD));

    V2(
        problem->sg_path_indices.resize(problem->id_path.size());
        for (size_t i = 0; i < problem->id_path.size(); i++)
        {
            problem->sg_path_indices[i] = sgs[0][problem->id_path[i]].mindex;
        }
    );
    // Validate path on G.
    // problem->validate_path();
}
