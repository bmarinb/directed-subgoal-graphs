#include "CH.hpp"
#include "heap.hpp"
#include "search.hpp"

/* 
CH
Implementation of Contraction Hierarchies with subgoal graphs
Based on implementation from https://algo2.iti.kit.edu/documents/routeplanning/geisberger_dipl.pdf
*/


CH::CH(){};

CH::~CH(){
    if (search != nullptr) delete search;
    if (positional_dijkstra != nullptr) delete positional_dijkstra;
    if (grid_explorer != nullptr) delete grid_explorer;
};


// Save ordered list with contracted nodes as json
void CH::save_contraction_order(){
    vector<NodeId> node_order(nodes_CH.size());
    for (size_t i = 0; i < contraction_order.size(); i++)
    {
        node_order[i] = contraction_order[i].first;
    }
    
    nlohmann::json json_ordering = node_order;
    save_json(json_ordering, ordering_path);
}


// N° of edges (forward and backward)
size_t CH::get_n_edges(bool fw, bool bw){
    size_t n_edges = 0;
    for (size_t i = 0; i < nodes_CH.size(); i++)
    {
        if (fw) n_edges += nodes_CH[i].edges[FORWARD].size();
        if (bw) n_edges += nodes_CH[i].edges[BACKWARD].size();
    }
    return n_edges;
}


// Node info. Includes CH info.
string CH::node_info(NodeId id, bool out_nbs, bool inc_nbs){

    string s = "Node " + std::to_string(id) + " in " + v2s(graph_DSG->sgs[id].get_pos()) + " " + (directed_graph? " towards " + dirs.names[graph_DSG->dir_indexes[id]] + " " : "")  +  nodes_CH[id].info();
    if (out_nbs or inc_nbs) s+= endl;
    EdgeCH* auxe;
    int i =0;
    if (out_nbs){
        s += "Out edges: " + endl;
        for (EdgeId eid: nodes_CH[id].edges[FORWARD]){
            s += std::to_string(i) +  ": Edge " + std::to_string(eid) + " " +  edges_CH[eid].info() + " level " + std::to_string(nodes_CH[edges_CH[eid].targets[FORWARD]].level) + " ctc " + std::to_string(nodes_CH[edges_CH[eid].targets[FORWARD]].is_contracted) + endl;
            i++;
        }
    }
    if (inc_nbs){
        i= 0;
        s += "Inc edges: " + endl;
        for (EdgeId eid: nodes_CH[id].edges[BACKWARD]){
            auxe =  &edges_CH[eid];
            s += std::to_string(i) +  ": Edge " + std::to_string(eid) + " " + auxe->info() + " level " + std::to_string(nodes_CH[auxe->targets[BACKWARD]].level) + " ctc " + std::to_string(nodes_CH[auxe->targets[BACKWARD]].is_contracted) + endl;
            i++;
        }
    }
    return s;
}


// Compare function for edge sorting
bool CH::compare_edge_CH(const EdgeCH& ed1, const EdgeCH& ed2) {
    NodeId c1, c1p, c1pp, c2, c2p, c2pp;
    if (nodes_CH[ed1.targets[FORWARD]].level <= nodes_CH[ed1.targets[BACKWARD]].level){
        c1 = ed1.targets[FORWARD];
        c1p = BACKWARD;
        c1pp = ed1.targets[BACKWARD];
    }
    else{
        c1 = ed1.targets[BACKWARD];
        c1p = FORWARD;
        c1pp = ed1.targets[FORWARD];
    } 
    if (nodes_CH[ed2.targets[FORWARD]].level <= nodes_CH[ed2.targets[BACKWARD]].level){
        c2 = ed2.targets[FORWARD];
        c2p = BACKWARD;
        c2pp = ed2.targets[BACKWARD];
    }
    else{
        c2 = ed2.targets[BACKWARD];
        c2p = FORWARD;
        c2pp = ed2.targets[FORWARD];
    } 
    return c1 < c2 or (c1 == c2 and c1p < c2p) or (c1 == c2 and c1p == c2p and c1pp < c2pp);
}


// Arg compare function for edge sorting
bool CH::arg_compare_edge_CH(const std::pair<EdgeCH, EdgeId>& pr1, const std::pair<EdgeCH, EdgeId>& pr2) {
    NodeId c1, c1p, c1pp, c2, c2p, c2pp;
    if (nodes_CH[pr1.first.targets[FORWARD]].level <= nodes_CH[pr1.first.targets[BACKWARD]].level){
        c1 = pr1.first.targets[FORWARD];
        c1p = BACKWARD;
        c1pp = pr1.first.targets[BACKWARD];
    }
    else{
        c1 = pr1.first.targets[BACKWARD];
        c1p = FORWARD;
        c1pp = pr1.first.targets[FORWARD];
    } 
    if (nodes_CH[pr2.first.targets[FORWARD]].level <= nodes_CH[pr2.first.targets[BACKWARD]].level){
        c2 = pr2.first.targets[FORWARD];
        c2p = BACKWARD;
        c2pp = pr2.first.targets[BACKWARD];
    }
    else{
        c2 = pr2.first.targets[BACKWARD];
        c2p = FORWARD;
        c2pp = pr2.first.targets[FORWARD];
    } 
    return c1 < c2 or (c1 == c2 and c1p < c2p) or (c1 == c2 and c1p == c2p and c1pp < c2pp);
}


// After CH is finished, build a search graph
void CH::generate_search_graph(array<vector<Node>*, 2>& search_nodes, vector<EdgeCH>& CH_edges, EdgeId& n_compressed){
    // Argsort edges
    vector<std::pair<EdgeCH, EdgeId>> edges_id(edges_size);
    for (size_t i = 0; i < edges_size; i++)
    {
        edges_id[i].first = edges_CH[i];
        edges_id[i].second = i;
    }
    std::sort(edges_id.begin(), edges_id.end(), 
    [this](const std::pair<EdgeCH, EdgeId>& pr1, const std::pair<EdgeCH, EdgeId>& pr2){
        return this->arg_compare_edge_CH(pr1, pr2);
        });
    vector<EdgeId> argsort(edges_size);
    for (size_t i = 0; i < edges_size; i++) argsort[edges_id[i].second] = i;

    // Sort edges
    std::sort(edges_CH.begin(), edges_CH.end(), 
    [this](const EdgeCH& ed1, const EdgeCH& ed2){
        return this->compare_edge_CH(ed1, ed2);
    });

    // Update e1 and e2
    EdgeId eid;
    n_compressed = 0;
    for (size_t i = 0; i < nodes_size; i++)
    {
        for (auto fw_bw: gdirs){
            for (size_t j = 0; j < nodes_CH[i].edges[fw_bw].size(); j++)
            {
                eid = argsort[nodes_CH[i].edges[fw_bw][j]];
                nodes_CH[i].edges[fw_bw][j] = eid; 
                if (edges_CH[eid].e1 <= NO_BASIC_REACHABILITY or edges_CH[eid].e2 <= NO_BASIC_REACHABILITY){
                    edges_CH[eid].e1 = argsort[edges_CH[eid].e1];
                    edges_CH[eid].e2 = argsort[edges_CH[eid].e2]; 
                    n_compressed++;
                } 
            }
            std::sort(nodes_CH[i].edges[fw_bw].begin(), nodes_CH[i].edges[fw_bw].end());
        }
    }
    
    // Generate new structures
    search_nodes[FORWARD] = new vector<Node>(nodes_size);
    if (directed_graph) search_nodes[BACKWARD] = new vector<Node>(nodes_size);

    CH_edges.reserve(directed_graph ? edges_CH.size() : edges_CH.size()/2 );
    EdgeId ned, current_edge = 0;
    array<EdgeId, 2> n_ed{0, 0};
    vector<EdgeId> eid_mapper(edges_CH.size());
    for (size_t i = 0; i < nodes_size; i++)
    {
        for (auto fw_bw: gdirs){
            search_nodes[fw_bw][0][i].first_edge = current_edge;
            ned = nodes_CH[i].edges[fw_bw].size();
            search_nodes[fw_bw][0][i].n_edges = (int16_t) ned;
            n_ed[fw_bw] += ned;
            for (EdgeId eid: nodes_CH[i].edges[fw_bw]){
                if (not directed_graph) eid_mapper[eid] = CH_edges.size();
                CH_edges.emplace_back(edges_CH[eid].targets, edges_CH[eid].cost, edges_CH[eid].e1, edges_CH[eid].e2);
            }
            current_edge += ned;
        }
    }

    if (not directed_graph){
        for (auto& ech: CH_edges){
            if (ech.e1 <= NO_BASIC_REACHABILITY) ech.e1 = eid_mapper[ech.e1];
            if (ech.e2 <= NO_BASIC_REACHABILITY) ech.e2 = eid_mapper[ech.e2];
        }
    }

    n_edges = CH_edges.size();
    n_edges_forward = n_ed[FORWARD];
    if (not directed_graph){
        n_shortcuts /=2;
        count_reachabilities[0] /= 2;
        count_reachabilities[1] /= 2;
        count_reachabilities[4] /= 2;
    } 
    if (not directed_graph) search_nodes[BACKWARD] = search_nodes[FORWARD];
    cout << "N° edges: " << CH_edges.size() << endl;
    cout << "FW BW edges: " << v2s(n_ed) << endl;
}


// Build forward and backward graphs
void CH::build_graphs(){
    nodes_size = graph_DSG->size(); 
    edges_size = graph_DSG->n_edges;
    Subgoal* aux_node, *nb_node;
    edges_CH.resize(edges_size);
    nodes_CH.resize(nodes_size);
    EdgeId current_edge = 0;
    EdgeCost ecost;
    for (size_t i = 0; i < nodes_size; i++)
    {
        aux_node = &graph_DSG->sgs[i];
        for (auto it = graph_DSG->first_edge_it(i); it < graph_DSG->last_edge_it(i); it++)
        {
            nb_node = &graph_DSG->sgs[it->target];
            nodes_CH[i].add_edge(FORWARD, current_edge);
            if (directed_graph) nodes_CH[it->target].add_edge(BACKWARD, current_edge);
            ecost = octile_distance<EdgeCost>(aux_node->get_pos(), nb_node->get_pos());
            edges_CH[current_edge] = EdgeCH(i, it->target, ecost);
            current_edge++;
        }
    }
}


// Generate hierarchy from ordering (json)
bool CH::generate_hierarchy_from_ordering(){

    if (not file_exists(ordering_path)){
        cout << "Ordering not found in " << ordering_path << endl;
        return false;
    } 
    else cout << "Using loaded ordering" << endl;
    nlohmann::json ordering_json;
    load_json(ordering_json, ordering_path);
    vector<NodeId> ordering = ordering_json.get<vector<NodeId>>();
    cout << "Ordering path: " << ordering_path << endl;
    cout << ordering.size() <<", " << graph_DSG->sgs.size() << endl;
    NodeId to_contract;
    NodeWeight node_priority;
    for (n_contr = 0; n_contr < ordering.size(); n_contr++)
    {
        to_contract = ordering[n_contr];
        evaluate_node(to_contract, node_priority, true);
        V1(cout << "Contraction number: " << n_contr << "." << "Node " << to_contract << " level " << node->level << endl);
        contraction_order[n_contr].first = to_contract;
        contraction_order[n_contr].second = node_priority;
    }
    return true;
}


// Generate hierarchy from scratch
void CH::generate_hierarchy(){
    auto start_time = set_timer();
    init_pqueue();
    cout << "Pqueue size: " << pqueue.size() << endl;
    auto elapsed = end_timer(start_time);
    cout << "Elapsed init queue: " << elapsed << endl;
    n_contr = 0;
    uint lazy_update_counter = 0, last_lazy_update_counter = 0;
    NodeId actual_min_id, new_min_id; 
    NodeWeight actual_min_priority, actual_min_new_priority, new_min_priority;
    while (pqueue.size() > 0){
        // Lazy update
        // Every lazy_update_check_interval iterations, check if complete update is needed
        if (n_contr%lazy_update_check_interval == 0 and n_contr > 0) 
        cout << n_contr <<  "|" << std::flush;
        
        if (n_contr%lazy_update_check_interval== 0 && n_contr >0)
        {
            // Complete update is needed only if since the last check, there were more lazy updates than 
            // lazy_update_check_interval*lazy_update_recalculate_th 
            if (lazy_update_counter - last_lazy_update_counter >= lazy_update_check_interval*lazy_update_recalculate_th){
                cout << endl <<  "Updating pqueue" << endl;
                start_time = set_timer();
                update_pqueue();
                last_lazy_update_counter = lazy_update_counter;
                elapsed = end_timer(start_time);
                cout << "Elapsed updating queue: " << elapsed << endl;
            }
        }
        // Lazy update: Find_consistent_mininimum
        pqueue.top(actual_min_id, actual_min_priority);
        V2(cout << "Actual min id, priority: " << actual_min_id << ", " << actual_min_priority << endl);
        evaluate_node(actual_min_id, actual_min_new_priority, false);

        while (fabs(actual_min_new_priority -  actual_min_priority) > 0.001)
        {

            lazy_update_counter ++;
            pqueue.update_key(actual_min_id, actual_min_new_priority);
            pqueue.top(new_min_id, new_min_priority);
            if (actual_min_id == new_min_id) break;
            // It isn't the same node.
            actual_min_id = new_min_id;
            actual_min_priority = new_min_priority;
            evaluate_node(actual_min_id, actual_min_new_priority, false);
            V2(cout << "Priority changed, new min id, priority: " << actual_min_id << ", " << actual_min_new_priority << endl);

        }
   
        // Remove the best node from the PQ
        pqueue.pop();
        // Contraction of the node
        V1(cout << "Starting contraction: " << n_contr << "." << "Node " << actual_min_id << " level " << node->level << endl);
        evaluate_node(actual_min_id, actual_min_priority, true);
        V1(
        cout << "Finishing contraction: " << n_contr << "." << "Node " << actual_min_id << " level " << node->level << endl;
        );
        contraction_order[n_contr].first = actual_min_id;
        contraction_order[n_contr].second = actual_min_priority;
        n_contr++;
    }
}


// Remove edges towards lower lever nodes.
void CH::eliminate_edges(){
    size_t total_deleted = 0;
    for (size_t i = 0; i < nodes_CH.size(); i++)
    {
        total_deleted += nodes_CH[i].eliminate_lower_level_edges();
    }
    edges_size = edges_CH.size();
}


// Read json params
void CH::read_params(const nlohmann::json& ch_params){
    edge_difference_weight = ch_params["edge_difference"];
    contracted_neighbours_weight = ch_params["contracted_neighbours"];
    search_space_weight_weight = ch_params["search_space_weight"];
    limit_search_space = ch_params["limit_search_space"];
    remove_dijkstra_redundant = ch_params["remove_dijkstra_redundant"];
    remove_symmetrical_redundant = ch_params["remove_symmetrical_redundant"];
    remove_octile_reachable = ch_params["remove_octile_reachable"];
    remove_manhattan_reachable = false; //ch_params["remove_manhattan_reachable"];
    remove_redundant_edges = remove_dijkstra_redundant or remove_symmetrical_redundant or remove_octile_reachable or remove_manhattan_reachable;
    lazy_update_recalculate_th = ch_params["lazy_update_recalculate_th"];
    lazy_update_check_interval = ch_params["lazy_update_check_interval"];
    debug_CH = ch_params["debug_CH"];
    save_ordering = ch_params["save_ordering"];
    load_ordering = ch_params["load_ordering"];
    reduce_shortucts_by_R = ch_params["reduce_shortucts_by_R"]["value"];
    if (reduce_shortucts_by_R){
        vector<string> freespace_RS =  ch_params["reduce_shortucts_by_R"]["RS"].get<vector<string>>();
        cout << "freespace R: " << v2s(freespace_RS) << endl;
        if (remove_octile_reachable){
            RDF = in_vector<string>("DF", freespace_RS);
            RCF = in_vector<string>("CF", freespace_RS);
        }
        if (remove_manhattan_reachable){
            RSQ0 = in_vector<string>("SQ0", freespace_RS);
            RSQ1 = in_vector<string>("SQ1", freespace_RS);
        }
    }
}


// Initialization. Performs preprocessing
CH::CH(SubgoalGraph* graph_DSG, string name, string type, bool save_problems, const nlohmann::json& CH_params) :
    graph_DSG(graph_DSG), directed_graph(graph_DSG->directed_graph)
{
    read_params(CH_params);
    default_unpacking = DIAGONAL_FIRST;
    gdirs = directed_graph ? vector<int>{FORWARD, BACKWARD}: vector<int>{FORWARD};
    BW_AUX = directed_graph?BACKWARD:FORWARD;
    //Dijkstra between all nodes
    grid_explorer = new Grid2d(graph_DSG->clr_no_nodes);
    DIRECTED(complete_graph_distances());
    UNDIRECTED(if (remove_octile_reachable) SG_get_reachabilities());
    build_graphs();
    base_ordering_path ="../node_ordering/" + graph_DSG->mapa->benchmark + "/"; 
    ordering_path = base_ordering_path + graph_DSG->mapa->name + "_" + graph_DSG->gtype_name + ".json";
    std::filesystem::create_directories(base_ordering_path);
    search = new Search(&graph_DSG->sgs, &nodes_CH, &edges_CH, DIJKSTRA, DIJKSTRA_SKIPPING_V);
    g_costs =  &search->g_cost;
    count_reachabilities.resize(5, 0);
    contraction_order.resize(nodes_CH.size());
    
    if (load_ordering and generate_hierarchy_from_ordering());
    else{
        load_ordering = false;
        generate_hierarchy();   
    } 
    V2(
    cout << "CH finished..." << endl;
    for (auto pair: contraction_order)
    {
        cout << "Level: " << nodes_CH[pair.first].level << " Node: " << pair.first << " = " << pair.second <<  endl;
    });
    for (size_t i = 0; i < nodes_CH.size(); i++)
    {
        assert(nodes_CH[i].is_contracted);
    }
    
    eliminate_edges();

    base_edges_size = get_n_edges();
    base_nodes_size = nodes_CH.size();
    if (save_ordering && not load_ordering) save_contraction_order();
    generate_search_graph(graph_DSG->bid_search_nodes, graph_DSG->CH_edges, graph_DSG->n_compressed);
    // Stats
    graph_DSG->CH_stats["E"] = graph_DSG->n_edges;
    graph_DSG->CH_stats["SC"] = n_shortcuts;
    graph_DSG->CH_stats["ECH"] = n_edges;
    graph_DSG->CH_stats["EF"] = n_edges_forward;
    graph_DSG->CH_stats["RED"] =  n_redundant_sc;
    graph_DSG->CH_stats["NDF"] = count_reachabilities[0];
    graph_DSG->CH_stats["NCF"] = count_reachabilities[1];
    cout << "N original edges: " << graph_DSG->n_edges<< endl;
    cout << "N added shortcuts: " << n_shortcuts << endl;
    cout << "N CH edges: " << n_edges<< endl;
    cout << "N redundants edges: " << n_redundant_sc << endl;
    if (RDF) cout << "N DIAGONAL_FIRST: " << count_reachabilities[0] << endl;
    if (RCF) cout << "N CARDINAL_FIRST: " << count_reachabilities[1] << endl;
    if (RSQ0) cout << "N SQUARE_0: " << count_reachabilities[2] << endl;
    if (RSQ1) cout << "N SQUARE_1: " << count_reachabilities[3] << endl;
    cout << "N NO_BASIC_REACHABILITY: " << count_reachabilities[4] << endl;  
}


// SG Get reachabilites. Used to detect freespace reachabilites and improve unpacking procedure.
void CH::SG_get_reachabilities(){
    APos p1, p2;
    int m = 0;
    uint8_t s1, s2;
    if (remove_octile_reachable or remove_manhattan_reachable) sg_reachabilities.resize(graph_DSG->n_nodes, vector<uint8_t>(graph_DSG->n_nodes, 0));
    for (size_t i = 0; i < graph_DSG->n_nodes; i++)
    {
        p1 = graph_DSG->sgs[i].get_pos();
        for (size_t j = i+1; j < graph_DSG->n_nodes; j++)
        {
            p2 = graph_DSG->sgs[j].get_pos();
            if (RDF and grid_explorer->check_diagonal_first(p1, p2)){
                sg_reachabilities[i][j] +=  1 << 0;
                sg_reachabilities[j][i] += 1 << 1;
            } 
            if (RCF and grid_explorer->check_diagonal_first(p2, p1)){
                sg_reachabilities[i][j] += 1 << 1;
                sg_reachabilities[j][i] += 1 << 0;
            }
            
            if (RSQ0 and grid_explorer->check_square_0(p1, p2)){
                sg_reachabilities[i][j] +=  1 << 2;
                sg_reachabilities[j][i] += 1 << 3;
            } 
            if (RSQ1 and grid_explorer->check_square_0(p2, p1)){
                sg_reachabilities[i][j] +=  1 << 3;
                sg_reachabilities[j][i] += 1 << 2;
            } 
        }
    }
    
}



// Complete distance matrix M'. First stage is with Dijsktra (D). Second stage is Symmetry (S) and Freespace reachable paths (F)
void CH::complete_graph_distances(){
    bool verbose = false;
    auto start_time = set_timer();
    auto elapsed = end_timer(start_time);
    if (remove_redundant_edges) position_costs.resize(graph_DSG->n_pos, vector<AstarCost>(graph_DSG->n_pos, std::numeric_limits<AstarCost>::infinity()));

    if (remove_octile_reachable or remove_manhattan_reachable) sg_reachabilities.resize(graph_DSG->n_pos, vector<uint8_t>(graph_DSG->n_pos, 0));
    NodeId first_node = 0;
    vector<APos> positions(graph_DSG->n_pos);
    for (size_t i = 0; i < graph_DSG->n_pos; i++)
        {
            positions[i] = graph_DSG->sgs[first_node].get_pos();
            first_node += graph_DSG->n_nodes_same_pos[i];
        }
    if (remove_dijkstra_redundant){
        start_time = set_timer();
        positional_dijkstra = new Search(&graph_DSG->sgs, &graph_DSG->search_nodes, &graph_DSG->search_edges, DIJKSTRA, POSITIONAL_DIJKSTRA, verbose, &graph_DSG->n_nodes_same_pos);
        first_node = 0;
        for (size_t i = 0; i < graph_DSG->n_pos; i++)
        {
            positional_dijkstra->reset_memory_and_stats();
            positional_dijkstra->positional_dijkstra(first_node, i, position_costs[i]);
            first_node += graph_DSG->n_nodes_same_pos[i];
        }
        elapsed = end_timer(start_time);
        cout << "Elapsed Dijkstras: " << elapsed << endl;
    }

    cout << "Symmetry and Freespace RR reductions" << endl;
    int n = 0;
    int m = 0;
    int q = 0;
    bool jump;
    float df_cost, cf_cost, sq0_cost, sq1_cost, valid_cost;
    start_time = set_timer();
    if (not (remove_symmetrical_redundant or remove_octile_reachable or remove_manhattan_reachable)) return;
    for (size_t i = 0; i < graph_DSG->n_pos; i++)
    {
        for (size_t j = i+1; j < graph_DSG->n_pos; j++)
        {   
            // Symmetry
            if (remove_symmetrical_redundant){
                if (position_costs[i][j] > position_costs[j][i] + search_epsilon) {
                    position_costs[i][j] = position_costs[j][i];
                    n++;
                }
                else if(position_costs[j][i] > position_costs[i][j] + search_epsilon) {
                    position_costs[j][i] = position_costs[i][j];
                    n++;
                }
            }
            // Freespace reachable
            if (remove_octile_reachable){

                df_cost = grid_explorer->try_DF_path_clrs(positions[i], positions[j]);
                cf_cost =  grid_explorer->try_DF_path_clrs(positions[j], positions[i]);

                if (df_cost > 0 or cf_cost > 0){
                    // Max because no path equals to -1
                    valid_cost = std::max(df_cost, cf_cost);
                    if (valid_cost < position_costs[i][j] - search_epsilon){
                        position_costs[i][j] = valid_cost;
                        position_costs[j][i] = valid_cost;
                        m+=2;
                    }

                    if (df_cost > 0){
                        sg_reachabilities[i][j] +=  1 << 0;
                        sg_reachabilities[j][i] += 1 << 1;
                    }
                    if (cf_cost > 0){
                        sg_reachabilities[i][j] += 1 << 1;
                        sg_reachabilities[j][i] += 1 << 0;
                    }

                } 
            }
            if (remove_manhattan_reachable){
                if (position_costs[i][j] + search_epsilon > manhattan_distance<float>(positions[i], positions[j])){
                    sq0_cost = grid_explorer->try_sq0_path(positions[i], positions[j]);
                    sq1_cost = grid_explorer->try_sq0_path(positions[j], positions[i]);

                    if (sq0_cost > 0 or sq1_cost > 0){
                        valid_cost = std::max(sq0_cost, sq1_cost);
                        position_costs[i][j] = valid_cost;
                        position_costs[j][i] = valid_cost;
                        q+=2;
                        if (sq0_cost > 0){
                            sg_reachabilities[i][j] += (1 << 2);
                            sg_reachabilities[j][i] += (1 << 3);
                        }
                        if (sq1_cost > 0){
                            sg_reachabilities[i][j] += (1 << 3);
                            sg_reachabilities[j][i] += (1 << 2);
                        }
                    }
                }
            }
        }
    }
    elapsed= end_timer(start_time);
    cout << "Elapsed updating max distance: " << elapsed << endl;
    cout << "N symmetry reductions: " << n << endl;
    cout << "N octile R reductions: " << m << endl;
    cout << "N Square R reductions: " << q << endl;
}


// Initialize priority queue
void CH::init_pqueue(){
    vector<std::pair<NodeId, NodeWeight>> pairs(nodes_CH.size());
    for (size_t node_id = 0; node_id < nodes_CH.size(); node_id++)
    { 
        evaluate_node(node_id, pairs[node_id].second, false);
        pairs[node_id].first = node_id; 
    }
    pqueue = BinaryHeap<NodeId, NodeWeight>(pairs);

}


// Update priority queue, after lazy update
void CH::update_pqueue(){
    float weight;   
    int total_updates = 0;
    int max_total = 0;
    for (size_t node_id = 0; node_id < nodes_CH.size(); node_id++)
    { 
        if (not nodes_CH[node_id].is_contracted) {
            evaluate_node(node_id, weight, false);    
            total_updates += pqueue.update_key(node_id, weight);
            max_total ++;
        }
    }
    cout << "Total updates:  " << total_updates << " from " << max_total << endl;
}


// Evaluate node. Main function to perform node ordering. It performs the contraction too.
void CH::evaluate_node(NodeId node_id, NodeWeight& elimination_weight, bool contract){
    new_edges.clear();
    shortcuts_needed = 0;
    NodeId v_id = node_id;
    node = &nodes_CH[v_id];
    uint search_space = 0;
    uint new_space;
    bool c1, c2;
    for (size_t i = node->fnc[BW_AUX]; i < node->edges_size(BW_AUX); i++)
    {
        inc_edge_id = node->edges[BW_AUX][i];
        inc_nb = edges_CH[inc_edge_id].targets[BW_AUX];
        assert(inc_nb != v_id);
        if (directed_graph) inc_pos = graph_DSG->search_pos_id[inc_nb];
        // Cost from incoming neighbor
        // Set max cost for this dijsktra execution
        inc_edge_cost = edges_CH[inc_edge_id].cost;
        max_cost = 0;
        for (size_t j = node->fnc[FORWARD]; j < node->edges_size(FORWARD); j++)
        {
            out_edge_id = node->edges[FORWARD][j];
            out_nb = edges_CH[out_edge_id].targets[FORWARD];
            if (out_nb == inc_nb) continue;
            out_edge_cost = edges_CH[out_edge_id].cost;
            if (out_edge_cost > max_cost) max_cost = out_edge_cost;
            nodes_CH[out_nb].is_target = true;
            out_size++;
        }
        max_cost += inc_edge_cost;

        search->reset_memory_and_stats();
        new_space = search->dijkstra_skipping_v(inc_nb, node_id, max_cost, out_size, (contract)?-1:limit_search_space);
        search_space += new_space;
        // Check if found by a shorter path
        new_shortcuts_needed = 0;
        for (size_t j = node->fnc[FORWARD]; j < node->edges_size(FORWARD); j++)
        {
            out_edge_id = node->edges[FORWARD][j];
            out_nb = edges_CH[out_edge_id].targets[FORWARD];
            if (directed_graph) out_pos = graph_DSG->search_pos_id[out_nb];
            if (nodes_CH[out_nb].is_contracted || out_nb == inc_nb) continue;
            passing_by_v_cost = inc_edge_cost + edges_CH[out_edge_id].cost;
            if (g_costs[0][out_nb] > (passing_by_v_cost + search_epsilon))
            {
                // Using M' matrix to prune shortcuts
                min_pos_cost = remove_redundant_edges ? position_costs[inc_pos][out_pos]: std::numeric_limits<EdgeCost>::infinity();
                c1 = !directed_graph or (directed_graph and !remove_redundant_edges);
                // Shortcut is not redundant
                c2 = (passing_by_v_cost <= min_pos_cost + search_epsilon);  
                if (c1 or c2){
                    new_shortcuts_needed += (directed_graph) ? 2: 1;
                    if (contract)
                    {
                        prepare_shortcut(inc_nb, out_nb, passing_by_v_cost, node->edges[BW_AUX][i], node->edges[FORWARD][j]);
                    }
                } else if (directed_graph && remove_redundant_edges && contract){
                    n_redundant_sc++;
                }
            }
            nodes_CH[out_nb].is_target = false;
        }
        shortcuts_needed += new_shortcuts_needed;
    }

    out_size = node->edges[FORWARD].size() - node->fnc[FORWARD];
    inc_size = node->edges[BACKWARD].size() - node->fnc[BACKWARD];
    int edge_difference = shortcuts_needed - (inc_size+out_size); 
    int n_contracted = node->fnc[FORWARD] + node->fnc[BACKWARD];
    elimination_weight = 
    edge_difference*edge_difference_weight + 
    n_contracted*contracted_neighbours_weight + search_space*search_space_weight_weight;
    if (contract) 
        {
            node->is_contracted = true;
            add_shortcuts();
            update_neighbours_edges(v_id);
            if (not load_ordering) update_neighbours_weight(v_id);
        }
}


// Return true if two nodes are neighbors.
bool CH::is_nb(NodeId n1, NodeId n2){
    NodeCH& node1 = nodes_CH[n1];
    EdgeCH* edge;
    for (EdgeId eid: node1.edges[0])
    {
        edge = &edges_CH[eid];
        if ((edge->targets[0] == n1 and edge->targets[1] == n2 )
            or 
            (edge->targets[1] == n1 and edge->targets[0] == n2)){
                return true;
            }
    }
    return false;
}


// After contraction a node, update its neighbors edges.
void CH::update_neighbours_edges(NodeId v_id){
    NodeId contracted_level = node->level;
    for (bool fw_bw:gdirs){
        for (size_t i = node->fnc[fw_bw]; i < node->edges[fw_bw].size(); i++)
        {
            eid1 = node->edges[fw_bw][i];
            nb_id = edges_CH[eid1].targets[fw_bw];
            nb = &nodes_CH[nb_id];
            if (nodes_CH[nb_id].level <= contracted_level) nodes_CH[nb_id].level = contracted_level+1;
            for (size_t j = nb->fnc[opposite_gdir(fw_bw)]; j < nb->edges[opposite_gdir(fw_bw)].size(); j++)
            {
                

                eid2 = nb->edges[opposite_gdir(fw_bw)][j];
                if (edges_CH[eid2].targets[opposite_gdir(fw_bw)] == v_id){
                    edge_id = nb->fnc[opposite_gdir(fw_bw)];
                    std::iter_swap(nb->edges[opposite_gdir(fw_bw)].begin() + edge_id , nb->edges[opposite_gdir(fw_bw)].begin() + j);
                    nb->fnc[opposite_gdir(fw_bw)] ++;
                }
                
            }
        }
    }   
}


// Update priority of a single neighbor.
void CH::update_nb_w(NodeId nb_id){
    V2(
        NodeWeight old_weight;
        old_weight = pqueue.get_value(nb_id);
    );
    evaluate_node(nb_id, nb_new_weight, false);
    V2(
        if (debug_CH){
            if (std::fabs(old_weight - nb_new_weight) > weight_epsilon){
                cout << "Node " << nb_id << " weight changed: " << old_weight << " to " << nb_new_weight << endl;
            }
        }
    );
    pqueue.update_key(nb_id, nb_new_weight);

}


// Check consistency of first-non-contracted neighbor.
bool CH::check_fnc(NodeId nid){
    nb = &nodes_CH[nid];
    for (int fw_bw:gdirs){
        for (size_t j = nb->fnc[fw_bw]; j < nb->edges[fw_bw].size(); j++)
        {
            eid2 = nb->edges[fw_bw][j];
            if (nodes_CH[edges_CH[eid2].targets[fw_bw]].is_contracted == 1)return false;
        }
        for (size_t j = 0; j < nb->fnc[fw_bw]; j++)
        {
            eid2 = nb->edges[fw_bw][j];
            if (nodes_CH[edges_CH[eid2].targets[fw_bw]].is_contracted == 0) return false;
        }

    }
    return true;
}


// Update neighbors weights
void CH::update_neighbours_weight(NodeId v_id){
    NodeCH* upd_nb = &nodes_CH[v_id];
    V2(if (debug_CH) cout << "Updating weights of neighbours of  contracted node "<< v_id  <<endl);
    NodeId contracted_level = node->level;
    for (int fw_bw: gdirs){
        for (size_t i = upd_nb->fnc[fw_bw]; i < upd_nb->edges[fw_bw].size(); i++)
        {
            edge_id = upd_nb->edges[fw_bw][i];
            nb_id = edges_CH[edge_id].targets[fw_bw];
            
            if (nodes_CH[nb_id].level <= contracted_level) nodes_CH[nb_id].level = contracted_level+1;
            update_nb_w(nb_id);
        }
    }
}


// Add corresponding edges to the graph
void CH::add_shortcuts(){
    EdgeId base_id = edges_CH.size();
    NodeId source, target;
    for (EdgeCH& ech: new_edges)
    {
        source = ech.targets[1];
        target = ech.targets[0];
        u = &nodes_CH[source];
        w = &nodes_CH[target];
        V2(cout << "Adding shortcuts from " << source << " to " << target << endl);
        u->edges[FORWARD].push_back(base_id);
        if (directed_graph) w->edges[BACKWARD].push_back(base_id);
        base_id ++;
    }
    if (new_edges.size() > 0){

        if (not directed_graph and new_edges.size()%2 != 0){
            cout << "New edges in undirected graph must be pairs" << endl;
            assert(false);
        }
        
        n_shortcuts += new_edges.size();
        edges_CH.insert(edges_CH.end(), new_edges.begin(), new_edges.end());
    }
}


// Prepare a single shortcut
void CH::prepare_shortcut(NodeId inc ,NodeId out, AstarCost cost, EdgeId e1, EdgeId e2){
    EdgeId r_type;
    NodeId posA, posB;
    uint8_t reach_Rs;
    Subgoal* s1, * s2;
    float mh_cost;
    if (directed_graph){
        posA = graph_DSG->search_pos_id[inc];
        posB = graph_DSG->search_pos_id[out];
        reach_Rs = sg_reachabilities.size()> 0 ? sg_reachabilities[posA][posB]: 0;
    } else{
        reach_Rs = sg_reachabilities.size()> 0 ? sg_reachabilities[inc][out]: 0;
    }
    if (RSQ0 or RSQ1){
        s1 = &graph_DSG->sgs[inc_nb];
        s2 = &graph_DSG->sgs[out_nb];
        mh_cost = manhattan_distance<float>(s1->get_x(), s1->get_y(), s2->get_x(), s2->get_y());
    }
    if (RDF and get_nth_byte(reach_Rs, 0)){
        r_type =  DIAGONAL_FIRST;
        count_reachabilities[0]++;
    } 
    else if (RCF and get_nth_byte(reach_Rs, 1)){
        r_type =  CARDINAL_FIRST;
        count_reachabilities[1]++;
    } 

    else if (RSQ0 and  get_nth_byte(reach_Rs, 2) and fabs(mh_cost - cost) < 0.1){

        r_type =  SQUARE_0;
        count_reachabilities[2]++;
    } 
    else if (RSQ1 and  get_nth_byte(reach_Rs, 3) and fabs(mh_cost - cost) < 0.1){
        r_type =  SQUARE_1;
        count_reachabilities[3]++;
    }
    
    else{
        r_type =  NO_BASIC_REACHABILITY;
        count_reachabilities[4]++;
    }

    if (r_type != NO_BASIC_REACHABILITY && reduce_shortucts_by_R){
        new_edges.push_back(EdgeCH(inc_nb, out_nb,  cost, r_type, r_type));
    } else {
        new_edges.push_back(EdgeCH(inc_nb, out_nb,  cost, e1, e2));
    }
    
}
