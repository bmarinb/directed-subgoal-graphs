#include "subgoal_graph.hpp"
/* 
Connection algorithms for Subgoal Graphs (SG) (2013)
Iterates through diagonal axis using jumps, similar to JP.
Also has detection of direct subgoals, using cardinal clearances as limits.
 */


// Forward connect
void SubgoalGraph::get_reachable_nodes_sg(){
    V2(if(cout_node_info)cout << "Connecting " << node_s->info() << endl);
    for (size_t i = 0; i < 4; i++)
    {
        dir_index = i*2;
        clr =clr_forward->get_clr(node_s->mindex, dir_index);
        if (clr > 0){
            final_index = node_s->mindex + dirs.dindex[dir_index]*clr;
            sg_single_connection(final_index);
            limits[dir_index/2] = clr;
        } else limits[dir_index/2] = INT_MAX;
    }

    for (size_t n = 0; n < 4; n++)
    {
        dir_index = 2*n+1;
        clr = clr_forward->get_clr(node_s->mindex, dir_index);
        V2(if(cout_node_info) cout << "Diagonal scanning in " << dirs.names[dir_index] << " clr " << clr << endl);
        aux_dir_right = dir_offset(dir_index, -1);
        aux_dir_left = dir_offset(dir_index, 1);

        max_right = limits[aux_dir_right/2];
        max_left = limits[aux_dir_left/2];
        jump_index = node_s->mindex;
        while (clr > 0){
            jump_index += dirs.dindex[dir_index]*clr;
            if (is_node[jump_index]){
                V2(if(cout_node_info) cout << "Connecting in diagonal axis to : " << v2s(i2p(jump_index)) << endl );
                sg_single_connection(jump_index);
                break;
            }
            V2(if(cout_node_info) cout << "Clr " << clr << endl);
            clr = clr_forward->get_clr(jump_index, aux_dir_right);
            if (clr > 0 && clr < max_right){
                final_index = jump_index + dirs.dindex[aux_dir_right]*clr;
                max_right = clr;
                sg_single_connection(final_index);
            }
            V2(if(cout_node_info) cout << "Clr " << clr << endl);
            clr = clr_forward->get_clr(jump_index, aux_dir_left);
            if (clr > 0 && clr < max_left){
                final_index = jump_index + dirs.dindex[aux_dir_left]*clr;
                max_left = clr;
                sg_single_connection(final_index);
            }
            clr = clr_forward->get_clr(jump_index, dir_index);
        }
    }
}
