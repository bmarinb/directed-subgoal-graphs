#ifndef SOLVER_H
#define SOLVER_H
#include "subgoal_graph.hpp"
#include "clrs.hpp"
#include "heap.hpp"
#include "search.hpp"
#include "CH.hpp"
#include "problem.hpp"
#include "grid.hpp"



/* 
Solver
Main class used to preprocess and solve all subgoal graphs. 

Solver Stats
Main class used to stats gathering and averaging.
 */

struct Solver;


struct SolverStats{
    nlohmann::json all_stats, game_folder_stats, stats_dict, map_stats;
    string graph_name;
    string stats_path;
    string base_path;
    string custom_graph_name;
    bool do_CH;
    int limit;
    Solver* solver;
    SolverStats(string graph_name, bool do_CH, int limit, nlohmann::json& stats_dict, string custom_graph_name);
    void update();
    void init_game_folder_stats(string game_name);
    void init_stats(nlohmann::json& stats, string name);
    void update_all_stats();
    string get_stats_filepath();
};






struct Solver{
    Mapa* map{nullptr};
    SubgoalGraph* sg_graph{nullptr}; 
    Graph* grid_graph{nullptr};
    Search *search{nullptr};
    CH* CHier{nullptr};
    ClearanceManager* clrs{nullptr};
    uint preprocessing_time;
    int verbose, algorithm, sub_algorithm, gtype;
    string benchmark, custom_graph_name;
    bool save_jsons, do_CH, debug_connection, rotate;
    array<bool, 2> avoid_search, avoid_connect;
    void solve_problem(Problem& problem);
    void solve_grid_problem(Problem& problem);
    void solve_single_problem(Problem& problem);
    nlohmann::json &general_stats, &stats_dict, &map_stats;
    void update_map_stats();
    void update_problem_stats(Problem& problem);
    array<Pos, 2> count_path_moves(vector<NodeId>&id_path);
    Search* init_forward_search(int alg, int sub_alg);
    vector<NodeId> parents;
    vector<AstarCost> g_costs;
    vector<AstarUint> pos_f_costs;
    vector<Bool> visited;
    void solve_problems(string scen_files, int n_problem=-1);
    Solver(int _gtype, string base_path, string benchmark, string _map_name, int _variant, bool debug_connection, NodeId _debug_node, bool _save_jsons, int sub_algorithm, bool _CH,const nlohmann::json& ch_params, const nlohmann::json& avoidance_params, bool rotate_map, nlohmann::json& _stats_dict, nlohmann::json& _stats, nlohmann::json& map_stats, bool SG_from_scratch, bool CH_from_scratch, bool clrs_from_scratch, string custom_graph_name);  
    string get_stats_filepath(string base_path);
    void process_problem_output(Problem& problem);
    void init_stats();
    Grid2d* grid_explorer{nullptr};
    ~Solver();
    void prepare_for_problems();
    int elapsed, elapsed_total;
    std::chrono::_V2::system_clock::time_point start_time, start_time_total;
    float direct_cost;
    double dif, exact_dif;
    uint old_size;
    NodeId start_id, goal_id;
    bool raise;
    APos move, total_moves;
    uint g_estimated_length, sg_estimated_length;
};


#endif
