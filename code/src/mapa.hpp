
#ifndef MAPA_H
#define MAPA_H

#include "cnpy.h"
#include "functions.hpp"
#include "algorithm"

/* 
Mapa (map in spanish, to avoid collision with (unordered) map)

Builds grid graph G and matrix A from a .npy or .map file.
Provides basic map interface.
Implements the grid as a linearized array. With dimensions WxH, the index of (i,j) is i*W + j

 */

const static std::vector<std::string> valid_extensions = {"npy", "map"};


struct Mapa{
    vector<uint8_t> matrix;
    string folder;
    const string name;
    string benchmark;
    bool rotate;
    const uint8_t free;
    const uint8_t obs;
    string type;
    vector<Pos> size;
    inline bool is_obstacle(Pos index) const{
        return is_inside(index) and matrix[index]==obs; 

    };
    Mapa(string map_path, string map_name, string benchmark, bool save, bool rotate_map=false, uint8_t _free=255, uint8_t _obs=0);
    void load_npy(string path);
    void load_map(string path);
    void to_npy() const;
    bool is_inside (APos cell) const{
        return cell[0] >= 0 and cell[0] < size[0] and cell[1] >= 0 and cell[1] < size[1];
    }
    inline bool is_free_unsafe(PosIndex pos_index) const{
        return matrix[pos_index] == free;

    };
    inline bool is_obstacle_unsafe(PosIndex pos_index) const{
        return matrix[pos_index] == obs;
    };
    inline bool is_free(PosIndex pos_index) const{
        return is_inside(pos_index) and matrix[pos_index] == free;
    };
    inline bool is_inside(PosIndex pos_index) const{
        return pos_index < matrix.size() and pos_index >= 0;
    };
    bool is_diagonal_reachable (PosIndex pos_index, Dir d) const;
    bool check_diag_move(PosIndex pos_index, Dir diag) const;
    PosIndex move(PosIndex pos_index, Dir move_dir, int n_steps) const;
    bool is_reachable_from(PosIndex pos_index, Dir dir_index) const;

};



#endif