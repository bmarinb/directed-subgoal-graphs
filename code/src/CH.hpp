#ifndef CH_H
#define CH_H
#include "subgoal_graph.hpp"
#include "problem.hpp"
#include "search.hpp"
#include "grid.hpp"
/* 
CH
Implementation of Contraction Hierarchies with subgoal graphs
Based on implementation from https://algo2.iti.kit.edu/documents/routeplanning/geisberger_dipl.pdf
*/


class CH{
    public:
    vector<vector<AstarCost>> position_costs;
    vector<vector<uint8_t>> sg_reachabilities;
    size_t get_n_edges(bool fw = true, bool bw = true);
    Search* search{nullptr};
    Search* positional_dijkstra{nullptr};
    SubgoalGraph* graph_DSG;
    string filepath;
    bool directed_graph;
    vector<int> gdirs;
    int BW_AUX;
    // Algorithm parameters
    NodeWeight edge_difference_weight, contracted_neighbours_weight, search_space_weight_weight; 
    bool reduce_shortucts_by_R, remove_redundant_edges, remove_dijkstra_redundant, remove_symmetrical_redundant;
    uint limit_search_space, lazy_update_recalculate_th, lazy_update_check_interval;
    vector<vector<uint8_t>> avoidable_nodes;
    CH(SubgoalGraph* graph_DSG, string name,string type, bool save_problems,
    const nlohmann::json& CH_params);
    CH();
    void build_CH();
    void evaluate_node(NodeId node_id, NodeWeight& elimination_weight, bool contract);
    void init_pqueue();
    void update_pqueue();
    void add_shortcuts();
    BinaryHeap<NodeId, NodeWeight> pqueue;
    NodeId base_nodes_size, current_level{0};
    EdgeId base_edges_size;
    vector<uint> count_reachabilities;
    vector<EdgeCH> new_edges;
    void update_neighbours_edges(NodeId v_id);
    void update_neighbours_weight(NodeId v_id);
    void update_nb_w(NodeId nb);
    void build_graphs();
    void generate_hierarchy();
    void read_params(const nlohmann::json& ch_params);
    void eliminate_edges();
    void prepare_shortcut(NodeId inc ,NodeId out, AstarCost cost, EdgeId e1, EdgeId e2);
    void complete_graph_distances();
    void SG_get_reachabilities();
    void save_contraction_order();
    void load_contraction_order();
    size_t nodes_size;
    size_t edges_size;
    string node_info(NodeId id, bool out_nbs=true, bool inc_nbs=true);
    vector<EdgeCH> edges_CH;
    vector<NodeCH> nodes_CH;
    Grid2d* grid_explorer{nullptr};
    int n_edges{0}, n_edges_forward{0};
    int n_redundant_sc{0};
    int n_shortcuts{0};
    ~CH();
    APos init, end;
    EdgeId eid1, eid2, default_unpacking;
    EdgeId inc_edge_id, out_edge_id, edge_id, new_shortcuts_needed, shortcuts_needed = 0, inc_size, out_size = 0;
    NodeCH *u, *node, *w, *nb;
    EdgeCost max_cost, passing_by_v_cost, min_pos_cost;
    NodeWeight inc_edge_cost, out_edge_cost, nb_new_weight;
    NodeId inc_nb, out_nb, inc_pos, out_pos, nb_id;
    bool is_nb(NodeId n1, NodeId n2);
    bool debug_CH, RDF{false}, RCF{false}, RSQ0{false}, RSQ1{false};
    double weight_epsilon{0.001f};
    double search_epsilon{0.001f};
    bool check_fnc(NodeId nid);
    int n_contr;
    inline int opposite_gdir(int fw_bw){
        return directed_graph ? 1 - fw_bw: FORWARD;
    };
    vector<AstarCost>*  g_costs;
    vector<std::pair<NodeId, NodeWeight>> contraction_order;
    bool save_ordering, load_ordering, remove_octile_reachable, remove_manhattan_reachable;
    string ordering_path, base_ordering_path, npy_base_path, npy_path;
    bool generate_hierarchy_from_ordering();
    void generate_search_graph(array<vector<Node>*, 2>& search_nodes, vector<EdgeCH>& CH_edges,
    EdgeId& n_compressed
    );
    bool compare_edge_CH(const EdgeCH& ed1, const EdgeCH& ed2) ;
    bool arg_compare_edge_CH(const std::pair<EdgeCH, EdgeId>& pr1, const std::pair<EdgeCH, EdgeId>& pr2) ;
};
#endif