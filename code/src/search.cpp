#include "search.hpp"
#include "CH.hpp"

/* 
Search
Contains functions to manage search algorithms
*/


// Node information
string Search::node_info(NodeId nid){
    return  std::to_string(nid) + ": " + sgs[0][nid].info() + " " + search_nodes[0][nid].info();
}


// Bidirectional(CH) node information
string Search::bid_node_info(NodeId nid){
    return  std::to_string(nid) + ": " + sgs[0][nid].info() + " " + bid_search_nodes[0][sel_open][0][nid].info();
}


Search::Search(){};


// Dijkstra
Search::Search(
    vector<Subgoal>* subgoals,
    vector<Node>* search_nodes,
    vector<Edge>* search_edges,
    int algorithm,
    int sub_algorithm,
    bool verbose,
    vector<Dir>* n_nodes_same_pos
    ) :
    sgs(subgoals),
    search_nodes(search_nodes),
    search_edges(search_edges),
    algorithm(algorithm),
    sub_algorithm(sub_algorithm),
    n_nodes_same_pos(n_nodes_same_pos),
    verbose(verbose)
    {
    dijkstra_init();
    }


// CH Dijkstra 
Search::Search(
    vector<Subgoal>* subgoals,
    vector<NodeCH>* nodes_CH,
    vector<EdgeCH>* prep_edges_CH,
    int algorithm,
    int sub_algorithm,
    bool verbose
) : sgs(subgoals), nodes_CH(nodes_CH), prep_edges_CH(prep_edges_CH), algorithm(algorithm), sub_algorithm(sub_algorithm) {
    dijkstra_init();
};


// Initialization for all Dijkstras
void Search::dijkstra_init(){
    inf = std::numeric_limits<AstarCost>::infinity();
    graph_size = (sub_algorithm == DIJKSTRA_SKIPPING_V) ? nodes_CH->size() : search_nodes->size() + 2;
    g_cost.resize(graph_size);
    if (sub_algorithm == DIJKSTRA_VANILLA) parents.resize(graph_size);
    if (sub_algorithm == POSITIONAL_DIJKSTRA) assert (n_nodes_same_pos != nullptr); 
}


// Initialization for all A* (directed subgoal graphs)
Search::Search(
    vector<Subgoal>* subgoals,    
    vector<Node>* search_nodes,
        vector<Edge>* search_edges,
        vector<NodeId>* search_pos_id,
        array<vector<Bool>, 2>* avoidance_table,
        vector<Pos>& map_size,
        array<bool, 2>& avoid_search,
        int algorithm,
        int sub_algorithm        
        ):
        sgs(subgoals),
        search_nodes(search_nodes),
        search_edges(search_edges),
        avoidance_table(avoidance_table),
        search_pos_id(search_pos_id),
        avoid_search(avoid_search),
        algorithm(algorithm),
        sub_algorithm(sub_algorithm)
        {
        inf = std::numeric_limits<AstarCost>::infinity();
        if (sub_algorithm == ASTAR_AVOID_VISITED or sub_algorithm == ASTAR_VISITED){
            graph_size = search_pos_id->size() + 2;
            visit = AVOID_VISITED_POSITIONS;
            repr_nodes.resize(graph_size);

        } else if (sub_algorithm == ASTAR_AVOID_POS_F or sub_algorithm == ASTAR_POS_F){
            graph_size = search_nodes->size() + 2;
            visit = AVOID_POSITIONS_HIGHER_F;
            pos_f.resize(graph_size);
        }
        else{
            graph_size = search_nodes->size() + 2;
          visit = VISIT_ALL;  
        } 
        open_2_crit = BinaryHeap<NodeId, AstarUint>(graph_size);
        define_2_key_priority_bits(map_size);
}


// Initialization for Vanilla A*
Search::Search(Graph* grid_graph, int algorithm, int sub_algorithm, bool verbose) : grid_graph(grid_graph), algorithm(algorithm), sub_algorithm(sub_algorithm), verbose(verbose){
    graph_size = grid_graph->nodes.size();
    visit = VISIT_ALL;
    inf = std::numeric_limits<AstarCost>::infinity();
    open_2_crit = BinaryHeap<NodeId, AstarUint>(graph_size);
    define_2_key_priority_bits(grid_graph->mapa.size);
}


// Initialization for 2-priority term representation
void Search::define_2_key_priority_bits(const vector<Pos>& map_size){
    max_bits_h = bits_for_heur(map_size) + 1;
    max_bits_g = bits_for_g(map_size);
    bits_for_decimals = 10;
    prec = (double)(1<<bits_for_decimals);
    bits_hprec = (64 - (max_bits_g + bits_for_decimals)); 
    parents.resize(graph_size);
    g_cost.resize(graph_size);
    epsilon_uint = ((AstarUint) (epsilon*prec)) << bits_hprec;
}


// Bidirectional Search
Search::Search(
    vector<Subgoal>* subgoals,
    array<vector<Node>*, 2>* bid_search_nodes,
    vector<EdgeCH>* CH_edges,
    array<vector<Bool>, 2>* avoidance_table,
    array<bool, 2>& avoid_search,
    int algorithm,
    int sub_algorithm,
    int directed_graph,
    vector<Pos>& map_size,
    int criterion,
    bool verbose
) :
    sgs(subgoals),
    bid_search_nodes(bid_search_nodes),
    CH_edges(CH_edges),
    avoidance_table(avoidance_table),
    avoid_search(avoid_search),
    algorithm(algorithm),
    sub_algorithm(sub_algorithm),
    directed_graph(directed_graph),
    criterion(criterion),
    verbose(verbose)
{
    inf = std::numeric_limits<AstarCost>::infinity();
    graph_size = sgs->size() + 2;
    bi_parents[0].resize(graph_size, -1);
    bi_parents[1].resize(graph_size, -1);
    bi_g[0].resize(graph_size, inf);
    bi_g[1].resize(graph_size, inf);
    if (sub_algorithm <= LAST_BID_DIJKSTRA){
        opens[0] = BinaryHeap<NodeId, AstarCost>(graph_size);
        opens[1] = BinaryHeap<NodeId, AstarCost>(graph_size);
    }
    else if (sub_algorithm >= FIRST_BID_ASTAR){
        bi_f[0].resize(graph_size, inf);
        bi_f[1].resize(graph_size, inf);
        opens_astar[0] = BinaryHeap<NodeId, AstarUint>(graph_size);
        opens_astar[1] = BinaryHeap<NodeId, AstarUint>(graph_size);
        define_2_key_priority_bits(map_size);
    }
}


// Reset memory between problems
void Search::reset_memory_and_stats(){

    if (algorithm == BID_SEARCH) {
        memset(&bi_parents[0][0],-1, sizeof(bi_parents[0][0])*graph_size);
        memset(&bi_parents[1][0],-1, sizeof(bi_parents[1][0])*graph_size);
        bi_g[0].clear();
        bi_g[1].clear();
        bi_g[0].resize(graph_size, inf);
        bi_g[1].resize(graph_size, inf);
        if (sub_algorithm >= BID_DIJKSTRA_WITH_STALL ) stalled = 0;
        n_expansions= 0;
        n_expansions_forward = 0;
        n_rel_per_exp = 0;
        n_avoided = 0;
        stalled = 0;
        n_percolations = 0;
        best_cost = inf;
        if (sub_algorithm <= LAST_BID_DIJKSTRA){
            opens[0].reset_queue();
            opens[1].reset_queue();
            bi_f[0].clear();
            bi_f[1].clear();
            bi_f[0].resize(graph_size, inf);
            bi_f[1].resize(graph_size, inf);
        } else{
            opens_astar[0].reset_queue();
            opens_astar[1].reset_queue();
        }
        V2(
            bid_expansions.fill(vector<NodeId>());
            id_stalled = vector<NodeId>();
        );
    } 
    else if (algorithm == DIJKSTRA){
        g_cost.clear();
        g_cost.resize(graph_size, inf);
        n_expansions = 0;

        if (sub_algorithm == DIJKSTRA_VANILLA )
        {
            memset(&parents[0],-1, sizeof(parents[0])*graph_size);
        }
    } 
    else if (algorithm == ASTAR){
        if (visit == AVOID_VISITED_POSITIONS){
            memset(&repr_nodes[0], 0, sizeof(repr_nodes[0])*graph_size);
        }
        else if (visit == AVOID_POSITIONS_HIGHER_F){
            memset(&pos_f[0],-1, sizeof(pos_f[0])*search_pos_id->size());
        }
        memset(&parents[0],-1, sizeof(parents[0])*graph_size);
        open_2_crit.reset_queue();
        g_cost.clear();
        g_cost.resize(graph_size, inf);
        n_expansions = 0;
        n_percolations = 0;
        n_rel_per_exp = 0;
        n_skipped = 0;
        n_avoided = 0;
        V2(
            id_expansions = vector<NodeId>();
            id_skipped = vector<NodeId>();
            id_avoided = vector<NodeId>();
        );
    }   
}


// Solves a problem using the specified search algorithm
void Search::solve(Problem* _problem){
    problem = _problem;
    V2(cout << "Algorithm is: ");
    switch (sub_algorithm)
        {
        case BID_ASTAR_AVOIDANCE:
            V2(cout << "BID_ASTAR_AVOIDANCE"<< endl);
            bid_astar_avoidance();
            break;      
        case BID_ASTAR_WITH_STALL:
            V2(cout << "BID_ASTAR"<< endl);
            bid_astar();
            break;       
        case BID_DIJKSTRA_AVOIDANCE:
            V2(cout << "BID_DIJKSTRA_AVOIDANCE"<< endl);
            bid_dijkstra_avoidance();
            break;            
        case BID_DIJKSTRA_WITH_STALL:
            V2(cout << "BID_DIJKSTRA_WITH_STALL"<< endl);
            bid_dijkstra();
            break;
        case BID_DIJKSTRA_WITHOUT_STALL:
            V2(cout << "BID_DIJKSTRA_WITHOUT_STAL"<< endl);
            bid_dijkstra_wo_stall();
            break;
        case ASTAR_AVOID_VISITED:
            V2(cout << "ASTAR_AVOID_VISITED"<< endl);
            astar_avoid_visited();
            break;
        case ASTAR_AVOID_POS_F:
            V2(cout << "ASTAR_AVOID_POS_F"<< endl);
            astar_avoid_pos_f();
            break;
        case ASTAR_VISITED:
            V2(cout << "ASTAR_VISITED"<< endl);
            astar_visited();
            break;
        case ASTAR_POS_F:
            V2(cout << "ASTAR_POS_F"<< endl);
            astar_pos_f();
            break;
        case ASTAR_VANILLA:
            V2(cout << "ASTAR_VANILLA"<< endl);
            astar();
            break;
        case GRID_ASTAR:
            grid_astar();
            break;
        }
    switch (algorithm)
    {
    case ASTAR:
        n_percolations = open_2_crit.n_percolations;
        break;
    
    case BID_SEARCH:
        if (sub_algorithm < FIRST_BID_ASTAR)
        n_percolations = opens[FORWARD].n_percolations + opens[BACKWARD].n_percolations;
        else n_percolations = opens_astar[FORWARD].n_percolations + opens_astar[BACKWARD].n_percolations;
        break;
    }
}


// Saves info to json
nlohmann::json Search::to_json(){
    nlohmann::json search_json;
    if (algorithm == ASTAR){
        search_json["expansions"] = id_expansions;
        search_json["skipped"] = id_skipped;
        search_json["avoided"] = id_avoided;

    }
    else if (algorithm == BID_SEARCH){
        search_json["expansions"] = id_expansions;
        search_json["skipped"] = id_skipped;
        search_json["avoided"] = id_avoided;
        search_json["stalled"] = stalled;

        if (sub_algorithm >= BID_DIJKSTRA_WITH_STALL) search_json["stalled_nodes"] = id_stalled;
    }
    return search_json;
}
