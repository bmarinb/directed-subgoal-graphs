#include "subgoal_graph.hpp"


/* 
Connection algorithms for Directed Subgoal Graphss (DSG)
*/

// Diagonal scan
void SubgoalGraph::dsg_diagonal_scan(){
    nb_dir = is_goal ? dir_offset(diag_dir, 4) : diag_dir;
    which = is_node[final_index];
    if (get_nth_byte(which, nb_dir)){
        nb_id = nodes_map[umap_index(final_index, nb_dir)];  
        MERGED(
        if (nb_id == -1) {
            for (int i = RIGHT; i <= LEFT; i+=2)
            {
                aux_dir = dir_offset(nb_dir , i);
                aux_nb_id = nodes_map[umap_index(final_index, aux_dir)]; 
                new_node_nbs.push_back(aux_nb_id);
            V2(if (cout_node_info) cout << "Connecting to " << aux_nb_id << " "  << subgoal_info(aux_nb_id) << endl);

            }    
            return;
        });
        // Both
        new_node_nbs.push_back(nb_id);
        V2(if (cout_node_info) cout << "Connecting to " << nb_id << " " << subgoal_info(nb_id) << endl);
        
    }
}


// Cardinal first connection (or Y connection)
void SubgoalGraph::dsg_Y_connection(Dir axis){
    card_dir = axis;
    V2(if (cout_node_info){
            cout << "Y connection. Card dir is " << dirs.names[card_dir] <<  
            ". lower bound: " << lower_bound  << " upper bound: " << upper_bound<< endl;
    });
    max_left = limits[((card_dir+1)/2)&3];    
    max_right = limits[((card_dir+7)/2)&3];
    V2(if (cout_node_info)  cout << "Diagonal maxim: " << v2s({max_left, max_right}) << endl);
    jump_index = node_s->mindex;
    clr = clr_forward->get_clr(jump_index, card_dir);
    V2(if (cout_node_info) cout  << "Initial clr: " << clr << endl);
    left_dir = dir_offset(card_dir, 2);
    right_dir = dir_offset(card_dir, -2);
    aux_dir_left = dir_offset(card_dir, 1);
    aux_dir_right = dir_offset(card_dir, -1);
    connect_cardinal = true;
    if (is_goal) {
        obs_dir_left = dir_offset(card_dir, 5);
        obs_dir_right = dir_offset(card_dir, -5);
        opposite_dir = dir_offset(card_dir, 4);
        dsg_loop_backward();
    }
    else dsg_loop_forward();
}


// Part of backward connect. In order to perform cardinal connection, there must be an obstacle.
void SubgoalGraph::check_obstacle_then_connect(bool is_left){
    nb_dir = is_left ? left_dir:right_dir;
    if (get_nth_byte(which, nb_dir)){
        corner_index = jump_index + dirs.dindex[is_left ? obs_dir_left:obs_dir_right];
        if (not mapa->is_obstacle_unsafe(corner_index)) return;
        if (is_left) do_CW = false;
        else  do_CCW = false;
        nb_id = nodes_map[umap_index(jump_index, nb_dir)]; 
        // Only if not merged
        NOT_MERGED(
        if (nb_id == (NodeId) -1){
            aux_dir = dir_offset(nb_dir , is_left?LEFT:RIGHT);
            aux_nb_id = nodes_map[umap_index(jump_index, aux_dir)]; 
            new_node_nbs.push_back(aux_nb_id);
            V2(if (cout_node_info) cout << "Connecting to " << subgoal_info(aux_nb_id) << endl);
            return;   
        });
        if(nb_id == last_connection) return;
        // Both
        new_node_nbs.push_back(nb_id);
        V2(if (cout_node_info) cout << "Connecting to " << subgoal_info(nb_id) << endl);
        return;
    }
}


// Backward cardinal connection
void SubgoalGraph::dsg_backward_cardinal_connection(){
    
    if (get_nth_byte(which, opposite_dir)) {
        V2( if (cout_node_info) cout << "Cardinal connection" << endl);
        NOT_MERGED(double_con = dsg_direct_connection(opposite_dir));
        MERGED(dsg_direct_connection(opposite_dir));
        connect_cardinal = false;
        NOT_MERGED(if (double_con) return);
        last_connection = nb_id;
    } 
    if (   
        get_nth_byte(which, card_dir)){
        V2( if (cout_node_info) cout << "Cardinal lateral connection" << endl);
        check_obstacle_then_connect(true);
        check_obstacle_then_connect(false);
    }
}


// Cardinal first loop (backward connection)
void SubgoalGraph::dsg_loop_backward(){
    V2(if (cout_node_info) cout << "Backwards loop " << endl);
    while (clr > 0 && ((do_CW or do_CCW) or connect_cardinal)){
        jump_index = jump_index + dirs.dindex[card_dir]*clr;
        V2(if (cout_node_info) cout << "Jumping at: " << v2s(i2p(jump_index)) << " with clr: " << clr  << " jump which: " << (int) which << endl);
        which = is_node[jump_index];
        if (which > 0){
            dsg_backward_cardinal_connection();
            if (not connect_cardinal) break;
        }
        dsg_diagonals_check();
    }   
}


// Cardinal first loop (forward connection)
void SubgoalGraph::dsg_loop_forward(){
    while (clr > 0 && ((do_CW or do_CCW) or connect_cardinal)){
        jump_index = jump_index + dirs.dindex[card_dir]*clr;
        which = is_node[jump_index];
        V2(if (cout_node_info) cout << "Jumping at: " << v2s(i2p(jump_index)) << " with clr: " << clr  << " jump which: " << (int) which << endl);
        if (which > 0){
            if (get_nth_byte(which, card_dir) && connect_cardinal){
                V2( if (cout_node_info) cout << "Cardinal connection" << endl);
                dsg_direct_connection(card_dir);
                connect_cardinal = false;
            }
            if (get_nth_byte(which, left_dir)) do_CW = false;
            if (get_nth_byte(which, right_dir)) do_CCW = false;
        }
        dsg_diagonals_check();
    }
}


// Diagonal part of the cardinal first connection. Perform two diagonal scans.
void SubgoalGraph::dsg_diagonals_check(){
    if (do_CCW){
        diag_dir = aux_dir_left; 
        clr = clr_forward->get_clr(jump_index, diag_dir);
        V2(if (cout_node_info) cout << "Checking dir: " << dirs.names[diag_dir] << 
        " Clr: " << clr << " Limit: " << max_left << endl);
        if (clr > 0 && clr <= max_left) 
        {
            final_index = jump_index + dirs.dindex[diag_dir]*clr;
            dsg_diagonal_scan(); 
            max_left = clr - !is_goal;
        }
    }
    if (do_CW){
        diag_dir = aux_dir_right;
        clr = clr_forward->get_clr(jump_index, diag_dir);
        V2(if (cout_node_info) cout << "Checking dir: " << dirs.names[diag_dir] << 
        " Clr: " << clr << " Limit: " << max_right << endl);
        if (clr > 0 && clr <= max_right) 
        {
            final_index = jump_index + dirs.dindex[diag_dir]*clr;
            dsg_diagonal_scan(); 
            max_right = clr - !is_goal;
        }
    }
    clr = clr_forward->get_clr(jump_index, card_dir);
}


// Initialization: Diagonal connection.
void SubgoalGraph::dsg_diagonal_connection(Dir base_dir, size_t n_its){
    for (size_t i = 0; i < n_its; i++)
    {
        diag_dir = dir_offset(base_dir, i*2);
        V2(if (cout_node_info) cout << "Diagonal connection in " << dirs.names[diag_dir] << endl);
        clr = clr_forward->get_clr(node_s->mindex, diag_dir);
        if (clr > 0)
        {
            limits[diag_dir/2] = clr - !is_goal;
            final_index = node_s->mindex + dirs.dindex[diag_dir]*clr;
            dsg_diagonal_scan();
        } else limits[diag_dir/2] = INT_MAX;
    }
    V2(if (cout_node_info) cout << "Diagonal limits are: " <<  v2s(limits) << endl);
}


// Forward and backward connect
void SubgoalGraph::dsg_connection(){
    V2(if (cout_node_info) cout << "Connecting " << subgoal_info(current_id)  
    << (is_goal ? " Is goal": (is_endpoint? " Is start": "")) <<
    endl);
    if (is_endpoint) {
        dsg_diagonal_connection(1, 4);
        last_connection = -1;
        for (size_t i = 0; i < 8; i+=2)
        {
            do_CCW = true;
            do_CW = true;
            dsg_Y_connection(i);
        }
    }
    else if (is_cardinal(current_dir)){
        dsg_diagonal_connection(dir_offset(current_dir, -1), 2);
        do_CCW = true;
        do_CW = true;
        dsg_Y_connection(current_dir);
        do_CCW = false;
        do_CW = true;
        dsg_Y_connection(dir_offset(current_dir, 2*LEFT));
        do_CCW = true;
        do_CW = false;
        dsg_Y_connection(dir_offset(current_dir, 2*RIGHT));
    } 
    else{
        dsg_diagonal_connection(current_dir, 1);
        do_CCW = true;
        do_CW = false;
        dsg_Y_connection(dir_offset(current_dir, RIGHT));
        do_CCW = false;
        do_CW = true;
        dsg_Y_connection(dir_offset(current_dir, LEFT));
    } 
}