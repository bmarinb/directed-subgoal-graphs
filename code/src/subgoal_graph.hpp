#ifndef SG_GRAPH_H
#define SG_GRAPH_H
#include "functions.hpp"
#include "mapa.hpp"
#include "nlohmann/json.hpp"
#include "clrs.hpp"
#include "search.hpp"
#include <functional>
#include <set>


/* 
Subgoal graph
Meta-class that implements the structure of all subgoal graphs and calls instance-specific subgoal graphs.
 */



class Search;
const int EXPECTED_EDGES_PER_NODE = 4;
const int DEFAULT_AVOIDANCE = 0;
const int NO_AVOIDANCE = 1;
const int CONNECTION_AVOIDANCE = 2;
const int SEARCH_AVOIDANCE = 3;
const int BOTH_AVOIDANCE = 4;
const int CH_FORWARD_AVOIDANCE = 0;
const int CH_BACKWARD_AVOIDANCE = 1;
const int CH_BOTH_AVOIDANCE = 2;




class SubgoalGraph{
    
    public:
    void update_clrs(vector<Clearance*>& clearances_vector);
    // vector<Node> nodes;
    SubgoalGraph(Mapa* _mapa, uint _gtype, uint _variant, bool debug_connection, NodeId _debug_node, bool do_CH, bool SG_from_scratch, bool CH_from_scratch, string custom_graph_name);
    bool cardinal_nodes;
    vector<IsNodeDir> is_node;
    inline uint size() const {return sgs.size();};
    uint n_nodes{0};
    uint n_edges{0};
    uint n_redundant_edges;
    void check_avoidance(const nlohmann::json& avoidance_params);
    nlohmann::json CH_stats;
    NodeId add_endpoint(const APos& pos, bool is_goal);
    NodeId add_endpoint_CH(const APos& pos, bool is_goal);
    void remove_last_node_CH(bool bid_removal);
    void remove_last_node(bool bid_removal);
    int n_pos{0};
    string gtype_name, npy_path, json_path, bid_npy_path, custom_graph_name; 
    void save_subgoals(string mode);
    void save_nodes(vector<Node>& nodes, string suffix, string mode);
    void save_base_edges(string mode);
    void save_referenced_nodes(string mode);
    void save_preprocessing_time(string mode);
    void save_CH_SG_stats(string mode);
    void save_CH_edges(string mode);
    void save_avoidance(string mode);
    void load_stats_from_npy();
    void load_avoidance_from_npy();
    void load_subgoals_from_npy();
    void load_nodes_from_npy(vector<Node>*& nodes, string suffix);
    void load_base_edges_from_npy();
    void load_CH_edges_from_npy();
    robin_hood::unordered_node_map<PosIndex, NodeId> nodes_map;
    cnpy::npz_t npy_zip;
    array<bool, 2>allow_avoidance, avoid_search, avoid_connect;
    array<NodeId,2> n_avoid_nodes;
    Clearance * clr_no_nodes;
    vector<Dir> n_nodes_same_pos;
    Mapa* mapa;
    int gtype;
    array<vector<Bool>, 2> avoidance_table;
    string edges_info();
    void connect();
    void to_json();
    void build_search_edges();
    int remove_redundant_edges();
    void prepare_search();
    vector<Subgoal> sgs;
    vector<Node> search_nodes, rsearch_nodes;
    vector<Edge> search_edges, rsearch_edges;
    uint sg_build_time{0}, CH_build_time{0};
    EdgeId n_compressed;
    vector<NodeId> search_pos_id;
    vector<Dir> dir_indexes;
    string node_info(NodeId node_id, bool nbs=true);
    string rnode_info(NodeId node_id, bool nbs=true);
    string bid_node_info(NodeId node_id);
    void load_npy();  
    bool use_reference_nodes;  
    void to_npy();
    ~SubgoalGraph();
    vector<NodeId> get_edges(NodeId node_id);
    vector<IsNodeDir> check_is_node;
    robin_hood::unordered_node_map<PosIndex, NodeId> check_nodes_map;
    vector<Edge>::iterator first_edge_it(NodeId node_id);
    vector<Edge>::iterator last_edge_it(NodeId node_id);
    bool graph_loaded{false};
    array<vector<bool>, 2> base_avoidable;
    array<vector<Node>*, 2> bid_search_nodes;
    vector<EdgeCH> CH_edges;
    vector<Node>* snodes;
    string subgoal_info(NodeId sg_id);
    bool is_nb(NodeId n1, NodeId n2);
    bool CH_directed, directed_graph, do_CH, SG_from_scratch, CH_from_scratch;
    int n_nbs;
    string base_json_path;
    string base_npy_path;
    void bid_to_npy();
    bool CH_loaded;
    vector<NodeId> new_node_nbs;

    private:
    void detect_corners(PosIndex cell_index);
    void generate_directed_nodes(PosIndex cell_index);
    void generate_undirected_nodes(PosIndex cell_index);
    Search *dijkstra_redundant{nullptr};
    void read_avoidance_params(const nlohmann::json& avoidance_params);
    Clearance* clr_forward, * clr_backward,  * clr_card1, 
    * clr_card2;
    void add_real_and_fake_nodes(Dir real_dir, Dir fake_dir);
    std::unordered_map<Dir, NodeId> real_nodes, fake_nodes;
    inline bool is_base_avoidable(int fw_bw, NodeId node_id)
    {
        return base_avoidable[fw_bw][node_id];
    }
    Subgoal* node_s;
    Node* actual_node;
    uint variant; 
    bool debug_connection;
    NodeId debug_node;
    void update_nbs_current_node();
    void define_nodes(); 
    EdgeId current_edge, first_edge;
    array<float, 2> avoid_th;
    EdgeId get_n_edges();
    NodeId get_n_nodes();
    void connect_node();

    // Excecution variables
    // Node definition
    PosIndex move_p, card1_p, card2_p;
    NodeId node_id{0};
    Dir obs_dir, cardinal_dir, diagonal_dir;
    vector<Bool> dir_gen;
    int left_right;
    bool there_is_node, merge_nodes;
    vector<NodeId> cell_nodes;
    // DSG
    bool is_goal, is_endpoint;
    NodeId nb_id, last_connection, aux_nb_id;
    IsNodeDir which;
    Dir corner_dir, free_dir, nb_dir;
    PosIndex corner_index, free_index, final_index, jump_index, endpoint_index, endpoint_pos_index;
    array<Pos, 4> limits;
    APos dmax, cmax, cindex;
    uint max_left, max_right;
    Pos clr, clr_left, clr_right, base_clr, main_clr, aux_clr, max_clr_card, diag_maxim, n_diag;
    inline void directed_add_edge_CH(NodeId nb_id){
        if (is_goal) CH_edges.emplace_back(nb_id, current_id, octile_distance<EdgeCost>(node_s->get_x(), node_s->get_y(), sgs[nb_id].get_x(), sgs[nb_id].get_y()), DIAGONAL_FIRST, DIAGONAL_FIRST);
        else CH_edges.emplace_back(current_id, nb_id, octile_distance<EdgeCost>(node_s->get_x(), node_s->get_y(), sgs[nb_id].get_x(), sgs[nb_id].get_y()), DIAGONAL_FIRST, DIAGONAL_FIRST);
    };
    inline void undirected_add_edge_CH(NodeId nb_id){
        CH_edges.emplace_back(current_id, nb_id, octile_distance<EdgeCost>(node_s->get_x(), node_s->get_y(), sgs[nb_id].get_x(), sgs[nb_id].get_y()), DIAGONAL_FIRST, DIAGONAL_FIRST);
    };
    inline void add_edge(NodeId nb_id){
        if (is_goal){
            search_edges[search_nodes[nb_id].first_edge + search_nodes[nb_id].n_edges] = Edge(current_id);
            search_nodes[nb_id].n_edges++;
            search_edges.emplace_back(nb_id);
        } else search_edges.emplace_back(nb_id);
    };
    bool do_quadrant, connect_cardinal;
    bool inc_left, inc_right, c1, c2;
    size_t ax, lower_bound, upper_bound;
    bool do_CCW, do_CW;
    Dir opposite_dir, left_dir, obs_dir_left, right_dir, obs_dir_right, aux_dir, card_dir, diag_dir, looked_dir, dir_index, aux_dir_index, current_dir, opposite_diag_dir, aux_dir_left, aux_dir_right;
    NodeId current_id;
    bool double_con;
    void prepare_loop();
    void update_map();
    void get_reachable_nodes_sg();
    inline void sg_single_connection(PosIndex pindex){
        new_node_nbs.push_back(nodes_map[pindex]);
    };
    bool cout_node_info;
    void dsg_connection();
    void dsg_Y_connection(Dir axis);
    void dsg_diagonal_connection(Dir base_dir, size_t n_its);
    void dsg_loop_forward();
    void dsg_loop_backward();
    void dsg_diagonals_check();
    void dsg_diagonal_scan();
    inline bool dsg_direct_connection(Dir nb_direct_dir){
        // Return true if double connection
        nb_id = nodes_map[umap_index(jump_index, nb_direct_dir)];
        // only not merged
        NOT_MERGED(
        if (nb_id == -1){
            for (int i = RIGHT; i <= LEFT; i+=2)
            {
                aux_dir = dir_offset(nb_direct_dir , i);
                aux_nb_id = nodes_map[umap_index(jump_index, aux_dir)]; 
                new_node_nbs.push_back(aux_nb_id);
            V2(if (cout_node_info) cout << "Connecting to " << subgoal_info(aux_nb_id) << endl);
            }    
            return true; 
        });
        
        new_node_nbs.push_back(nb_id);
        V2(if (cout_node_info) cout << "Connecting to " << subgoal_info(nb_id) << endl);
        return false;
    };
    void check_obstacle_then_connect(bool left_right);
    void dsg_backward_cardinal_connection();
    void get_reachable_nodes_jg();
    void jg_cardinal_connection();
    void jg_diagonal_connection();
    
    void build_reversed_edges();
    inline void cardinal_scan(PosIndex pindex){
        clr = clr_forward->get_clr(pindex, card_dir);
        V2(if (debug_connection) cout << "Cardinal scan " << dirs.names[card_dir] << " clr " << (int) clr << endl);
        if (clr > 0){
            final_index = pindex + dirs.dindex[card_dir]*clr;
            jg_single_connection(card_dir, final_index);
        }
    }
    void check_quadrants();
    inline bool jg_single_connection(Dir scan_dir, PosIndex pindex){
        which = is_node[pindex];
        if (get_nth_byte(which, scan_dir)==1){
            nb_id = nodes_map[umap_index(pindex, scan_dir)];
            new_node_nbs.push_back(nb_id);
            V2(if (debug_connection) cout << "Connecting to " << subgoal_info(nb_id) <<  endl);
            return true;
        } 
        return false;
    };
    // Connect to incoming lateral nodes. It is possible that the lateral node refers to the same incoming opposite node. To avoid double connectionss, last_connection mechanism is used.
    inline bool jg_incoming_lateral_conn(Dir scan_dir, int obstacle_dir){
        corner_index = jump_index + dirs.dindex[obstacle_dir];
        if (not mapa->is_obstacle_unsafe(corner_index)) return false;
        nb_id = nodes_map[umap_index(jump_index, scan_dir)];
        #if defined(ONLY_MERGED) or defined(VERBOSE2)
        if (nb_id != last_connection){
            V2(if (debug_connection) cout << "Connecting to " << subgoal_info(nb_id) << "***" << endl);
            new_node_nbs.push_back(nb_id);
            return true;
        }
        #else 
            new_node_nbs.push_back(nb_id);
            return true;
        #endif 
        V2(cout << "Repeated connection. Skipping" << endl);
        return false;
    };

    void load_avoidance_table();
    Dir first_quadrant;
    Dir last_quadrant;
    void jg_forward_loop();
    void jg_backward_connection();
    void jg_single_diagonal_connection();
    void jg_all_diagonals_connection();
    void goal_cardinal_connection();
    void jg_loop_diagonal_connection();
    bool cell_inc_left, cell_inc_right;

};


#endif