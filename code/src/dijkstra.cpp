#include "search.hpp"
/* 
Functions used to run different Dijkstra implementations
*/


// Vanilla Dijkstra
void Search::dijkstra_vanilla(NodeId start_id){
    AstarCost max_cost = 0;
    u_id = start_id;
    u_x = sgs[0][start_id].get_x();
    u_y = sgs[0][start_id].get_y();
    for (edge_id = search_nodes[0][start_id].first_edge; edge_id < search_nodes[0][start_id].last_edge(); edge_id++)
    {
        v_id = search_edges[0][edge_id].target;
        edge_cost = get_cost();
        if (edge_cost > max_cost) max_cost = edge_cost;
    }
    open_1_crit = BinaryHeap<NodeId, EdgeCost>(search_nodes[0].size());
    open_1_crit.insert(start_id, 0);
    g_cost[start_id] = 0;
    while (open_1_crit.size()>0)
    {
        dijkstra_extract_min();
        if (u_cost > max_cost + epsilon) break;
        V2( if (verbose) cout << "******************************************" << endl);
        V2( if (verbose) cout << "Extracting node " << u_id << " " << search_nodes[0][u_id].info() << " Cost: " << u_cost <<  endl);
        for (edge_id = search_nodes[0][u_id].first_edge; edge_id < search_nodes[0][u_id].last_edge(); edge_id++){
            v_id = search_edges[0][edge_id].target;
            edge_cost = get_cost();
            dijkstra_add_edge_to_open(inf);
        }
    }
}


// Dijkstra from all subgoals in a given cell
void Search::positional_dijkstra(NodeId node_id, NodeId pos_id, vector<AstarCost>& pos_g_costs){
    open_1_crit = BinaryHeap<NodeId, EdgeCost>(search_nodes->size());
    NodeId start_id = node_id;
    
    for (size_t i = 0; i < n_nodes_same_pos[0][pos_id]; i++)
    {
        open_1_crit.insert(start_id, 0);
        g_cost[start_id] = 0;
        start_id++;
    }

    while (open_1_crit.size()>0)
    {
        dijkstra_extract_min();
        V2( if(verbose) cout << "Extracting node: " << u_id << " " << search_nodes[0][u_id].info() << " Cost: " << u_cost <<  endl);
            for (edge_id = search_nodes[0][u_id].first_edge; edge_id < search_nodes[0][u_id].last_edge(); edge_id++){
            v_id = search_edges[0][edge_id].target; 
            edge_cost = get_cost();
            dijkstra_add_edge_to_open(inf);
        }
    }
    V2(if (verbose) cout << "Search from pos " << pos_id << " " << v2s(sgs[0][node_id].get_pos()) << endl);
    V2(if (verbose) cout << "First node is: " << node_id << endl);
    AstarCost min_cost;
    NodeId nid = 0;
    for (size_t i = 0; i < n_nodes_same_pos[0].size(); i++)
    {
        min_cost = inf;
        for (size_t j = 0; j < n_nodes_same_pos[0][i]; j++)
        {
            if (g_cost[nid] < min_cost) min_cost = g_cost[nid]; 
            nid ++;
        }
        if (min_cost < inf){
            pos_g_costs[i] = min_cost;
            V2(if (verbose) cout << "Pos " << i << " " <<v2s(sgs[0][nid-1].get_pos())
             << ": " << pos_g_costs[i] << endl);
        }
    }
}


// Dijkstra skipping a node v
uint Search::dijkstra_skipping_v(NodeId start_id, NodeId skip_id, EdgeCost max_cost, NodeId n_targets, uint max_expansions){
    open_1_crit = BinaryHeap<NodeId, EdgeCost>(nodes_CH->size());
    open_1_crit.insert(start_id, 0);
    g_cost[start_id] = 0;
    V2(if (verbose) cout << endl << "Search from " << start_id << " skipping " << skip_id << ". N targets: " << n_targets << endl);
    while (open_1_crit.size() > 0)
    {
        open_1_crit.extract_min(u_id, u_cost);
        V2(if (verbose) cout << "Extracting node: " << u_id <<  " Cost: " << u_cost << " max cost " << max_cost  << " open size " << open_1_crit.size() <<  endl);
        if (u_cost > (max_cost + epsilon)) {
            V2(cout << "Breaking with cost " << u_cost  << endl);
            break;
        }
        if (nodes_CH[0][u_id].is_target)
        {
            n_targets--;
            V2(if (verbose) cout << "Found target " << u_id << ". Targets remaining: " << n_targets << endl);
            if (n_targets == 0) break;
        }
        n_expansions++;
        if (n_expansions > max_expansions) break;
        for (size_t i = nodes_CH[0][u_id].fnc[FORWARD]; i < nodes_CH[0][u_id].edges_size(FORWARD); i++)
        {
            edge_id = nodes_CH[0][u_id].edges[FORWARD][i];
            v_id = prep_edges_CH[0][edge_id].targets[FORWARD];
            if (v_id == skip_id) continue;
            edge_cost = prep_edges_CH[0][edge_id].cost;
            dijkstra_add_edge_to_open(max_cost);
        }
    }
    return n_expansions;
}



// Dijkstra add edge to open
void Search::dijkstra_add_edge_to_open(EdgeCost max_cost){
    g = u_cost + edge_cost;
    V2( if (verbose ) cout << "Adding " << v_id << " G, cost: " << g << ", " << g_cost[v_id] << endl);
    if (g < (g_cost[v_id] - epsilon) &&  g < max_cost + epsilon)
    {
        V2( if (verbose) cout << "Adding node to open" << endl);
        g_cost[v_id] = g;
        if (parents.size() > 0){
            parents[v_id] = u_id;
        } 
        
        if (open_1_crit.in_queue(v_id)){
            open_1_crit.decrease_key(v_id, g);
        } else
        {
            open_1_crit.insert(v_id, g);
        }   
    }
    else if (parents.size() > 0 &&
             (g >= (g_cost[v_id] - epsilon) && g <= (g_cost[v_id] + epsilon))
             ){
        parents[v_id] = u_id;
    }
}


