#ifndef SEARCH_H
#define SEARCH_H
#include "problem.hpp"
#include "heap.hpp"
#include "grid.hpp"


/* 
Search
Contains different search algorithms.
Dijkstra:
1. Dijkstra
2. Dijkstra skipping V
3. Positional dijkstra
A*(for subgoal graphs):
1. A* avoid visited: Use avoidance and assumes each cell can be visited at most once.
2. A* visited: Assumes each cell can be visited at most once.
3. A* avoid pos f: Use avoidance and assumes each cell can be expanded if reached by the same f-value.
4. A* pos f: assumes each cell can be expanded if reached by the same f-value.
5. A*: Vanilla A* for subgoal graphs
Grid A*
Bidirectional search:
Bid Dijkstra:
1. Without stall
2. With stall and without avoidance
3. With stall and avoidance
Bid A*:
2. With stall and without avoidance
3. With stall and avoidance
*/

class Search{
    public:
    Search();
    void reset_memory_and_stats();
    void solve(Problem* problem);
    uint dijkstra_skipping_v(NodeId start_id,  NodeId skip_id, EdgeCost max_cost, NodeId n_targets, EdgeId max_expansions = -1);
    void dijkstra_vanilla(NodeId start_id);
    void bid_astar();
    void bid_astar_avoidance();
    bool choose_open_and_expand_astar();
    void bid_dijkstra_avoidance();
    void bid_dijkstra();
    void bid_dijkstra_wo_stall();
    void positional_dijkstra(NodeId node_id, NodeId pos_id, vector<AstarCost>& GG);
    void astar_avoid_visited();
    void astar_visited();
    void astar_avoid_pos_f();
    void astar_pos_f();
    void astar();
    void grid_astar();
    vector<AstarCost> g_cost;
    array<vector<AstarCost>, 2> bi_g;
    array<vector<AstarCost>, 2> bi_f;
    void bid_search_refine_path();
    void refine_directed_half_path(bool gdir);
    void refine_undirected_half_path(bool gdir);
    void bid_init_problem_dijkstra();
    void bid_init_problem_astar();
    bool choose_open_and_expand();
    void bid_relax_edges_dijkstra_avoidance();
    void bid_relax_edges_dijkstra();
    void bid_relax_edges_astar();
    void bid_relax_edges_astar_avoidance();
    void bid_process_edge_dijkstra();
    void bid_process_edge_astar();
    void check_frontier_dijkstra();
    void check_frontier_astar();
    void check_stall_dijkstra();
    void check_stall_astar();

    AstarCost inf;
    nlohmann::json to_json();
    string new_edge_info();
    void a_star_init_pos_f();

    void a_star_init_visited();
    NodeId goal_pos_id, u_pos_id, v_pos_id;

    vector<NodeId> repr_nodes;
    void extract_next_avoid_pos_f();
    void extract_next_avoid_visited();
    void extract_next();
    inline void finish_expansion(){
        V2(
            cout << "------------------" << endl << "Iteración: " << n_expansions << " Open size: " << open_2_crit.size() << endl;
            printf("Extrayendo %s G: %f F: %llX \n", node_info(u_id).c_str(), g_cost[(visit == AVOID_VISITED_POSITIONS) ? u_pos_id:u_id], u_f_cost);
        );
        n_expansions++;
        V2(id_expansions.push_back(u_id));    
        u_x = sgs[0][u_id].get_x();
        u_y = sgs[0][u_id].get_y();
    }

    void define_2_key_priority_bits(const vector<Pos>& map_size);
    Search(
        vector<Subgoal>* subgoals,
        vector<Node>* search_nodes,
            vector<Edge>* search_edges,
            vector<NodeId>* search_pos_id,
            array<vector<Bool>, 2>* avoidance_table,
            vector<Pos>& map_size,
            array<bool, 2>& avoid_search,
            int algorithm,
            int sub_algorithm
            );
    Search(
        vector<Subgoal>* subgoals,
        array<vector<Node>*, 2>* bid_search_nodes,
        vector<EdgeCH>* CH_edges,
        array<vector<Bool>, 2>* avoidance_table,
        array<bool, 2>& avoid_search,
        int algorithm,
        int sub_algorithm,
        int directed_graph,
        vector<Pos>& map_size,
        int criterion=MIN_MIN,
        bool verbose=true
    );
    Search(
        vector<Subgoal>* subgoals,        
        vector<Node>* search_nodes,
        vector<Edge>* search_edges,
        int algorithm,
        int sub_algorithm,
        bool verbose=false,
        vector<Dir>* n_nodes_same_pos = nullptr
    );

    Search(
        Graph* grid_graph,
        int algorithm,
        int sub_algorithm,
        bool verbose=true
    );
    Search(
        vector<Subgoal>* subgoals,        
        vector<NodeCH>* nodes_CH,
        vector<EdgeCH>* edges_CH,
        int algorithm,
        int sub_algorithm,
        bool verbose=false
    );

    Graph* grid_graph;
    uint stalled;
    vector<NodeId> id_stalled;
    string node_info(NodeId node_id);
    string bid_node_info(NodeId node_id);
    array<vector<NodeId>, 2> bid_expansions;
    vector<NodeId> id_expansions, id_skipped, id_avoided;
    array<bool, 2> finished;
    uint n_percolations{0}, n_expansions, n_expansions_forward, n_rel_per_exp;
    uint n_avoided, n_skipped;
    array<bool, 2> avoid_search;
    int visit;
    vector<NodeId> parents;
    bool verbose{false};
    void recover_path();
    void forward_search_refine_path();
    void forward_G_refine_path();
    EdgeCost epsilon{0.001f};
    vector<EdgeCH>* CH_edges;
    Problem* problem;
    vector<EdgeId> unpack_stack;
    vector<std::pair<EdgeId, bool>> ud_unpack_stack;
    private:
    FreespaceExplorer freespace;
    GridNode * u_node;
    void dijkstra_init();
    vector<NodeCH>* nodes_CH;
    vector<EdgeCH>* prep_edges_CH;
    vector<Dir>* n_nodes_same_pos;
    template<class Value>
    inline Value oct_dist(int x1, int x2, int y1, int y2){
        dx = std::abs((x1-y1));
        dy = std::abs((x2-y2));
        if (dx>=dy)
        {
            return dx + dy*r2m1;
        } 
        return dy + dx*r2m1;
    }
    float r2m1{0.41421356237};
    Pos dx;
    Pos dy;
    EdgeId v_eid;
    EdgeId edge_id;
    inline EdgeCost get_h(){
        return oct_dist<EdgeCost>(sgs[0][v_id].get_x(), sgs[0][v_id].get_y(), goal_x, goal_y);
    };
    inline EdgeCost get_reversed_h(){
        return oct_dist<EdgeCost>(sgs[0][v_id].get_x(), sgs[0][v_id].get_y(), start_x, start_y);      
    }
    inline EdgeId get_n_edges(){
        return search_nodes[0][u_id].n_edges;
    };
    inline EdgeCost get_cost(){
        return oct_dist<EdgeCost>(u_x, u_y, sgs[0][v_id].get_x(), sgs[0][v_id].get_y());
    };
    int aux_sel_open;
    vector<Node>* aux_search_nodes;
    vector<Node>* search_nodes;
    vector<Edge>* search_edges;
    array<vector<Node>*, 2>* bid_search_nodes;
    vector<NodeId>* search_pos_id;
    array<vector<Bool>, 2>* avoidance_table;
    vector<Subgoal>* sgs;
    int criterion;
    AstarUint epsilon_uint;
    void add_edge_to_open();
    void visited_add_edge_to_open();
    void dijkstra_add_edge_to_open(EdgeCost max_cost);
    inline void count_skip(bool is_expanding){
        n_skipped += 1;
        V2(if (is_expanding) id_skipped.push_back(u_id));
        V2(cout << "Node skipped. Position already reached with better F: " << node_info(is_expanding?u_id:v_id) << endl);
    };
    inline void count_avoid(){
        n_avoided += 1;
        V2(id_avoided.push_back(v_id));
        V2(cout << "Node avoided: " << node_info(v_id) << endl);
    };
    inline void dijkstra_extract_min(){
        open_1_crit.extract_min(u_id, u_cost);
        u_x = sgs[0][u_id].get_x();
        u_y = sgs[0][u_id].get_y();
    }
    NodeId u_id, v_id, goal_id;
    EdgeCost g, u_cost;
    Pos goal_x, goal_y, u_x, u_y, start_x, start_y;
    EdgeCost best_cost, match_cost;
    bool bid_forward, sel_open, can_stall;
    NodeId best_node, parent_id;
    EdgeCost edge_cost, v_cost;
    array<BinaryHeap<NodeId, AstarCost>, 2> opens;
    array<BinaryHeap<NodeId, AstarUint>, 2> opens_astar;
    inline AstarCost search_radius(bool n_open){
        if (opens_astar[n_open].size() > 0) {
            return bi_f[n_open][opens_astar[n_open].min_id()];
        } 
        else return inf;
    }
    AstarCost h;
    AstarUint f_cost_v, u_f_cost;
    BinaryHeap<NodeId, AstarUint> open_2_crit;
    BinaryHeap<NodeId, EdgeCost> open_1_crit;
    int directed_graph;    
    int graph_type, algorithm, sub_algorithm;
    bool special_edge;
    int sel_open_exp;
    NodeId graph_size;
    uint bits_for_decimals,  max_bits_h, max_bits_g, bits_hprec;
    bool is_nb(NodeId n1, NodeId n2, int graph_dir);
    AstarCost prec;
    array<vector<EdgeId>, 2> bi_parents;
    vector<AstarUint> pos_f;
    void finish_search();
    // Refine path
    int n_moves_diag, n_moves_card, abs_dx, abs_dy;
    PosIndex actual_index, diag_dindex, card_dindex;
    bool reversed;
    EdgeCH* unpack_edge;
    EdgeCH* actual_edge;
    PosIndex previous_index;
    NodeId ep_id;
    vector<PosIndex>* G_path;
    vector<PosIndex>* SG_path;

};

#endif
