#include "problem.hpp"


/* 
Problem
Stores information of an instance and its solution.
*/


// Problem initialization. Pass any value to real cost if the value is not known.
Problem::Problem(const APos& start, const APos& goal, AstarCost real_cost, uint id_problem, bool by_ch) : 
    start(start),
    goal(goal),
    start_id(0),
    goal_id(0),
    cost(0),
    gt_cost(real_cost),
    by_ch(by_ch),
    valid(0),
    direct(0),
    new_nodes(0),
    id_problem(id_problem),
    search_time(0),
    connect_time(0),
    problem_time(0),
    CSR_time(0),
    id_path(0){
    };



Problem::Problem(const APos& start, const APos& goal, AstarCost real_cost, uint id_problem)
: start(start), goal(goal), start_id(mindex(start)), goal_id(mindex(goal)), gt_cost(real_cost), id_problem(id_problem){};
    

// Validate a path on a freespace. Verifies consecutive movements are neighbors in the grid and also verifies the cost is correct.
void Problem::validate_path(){
    assert(std::unique(g_path_indices.begin(), g_path_indices.end()) == g_path_indices.end());
    APos p1, p2;
    double mod, total_cost, dif;
    total_cost = 0;
    for (size_t i = 0; i < g_path_indices.size()-1; i++)
    {
        p1 = i2p(g_path_indices.at(i));
        p2 = i2p(g_path_indices.at(i+1));
        mod = sqrt(pow(p2[0]-p1[0], 2) + pow(p2[1]-p1[1], 2));
        total_cost += mod;
        assert(mod == 1 or (fabs(mod -sqrt(2)) < 0.01));

    }
    dif = fabs(total_cost - gt_cost);
    if (dif > 0.01){
        cout << "GT cost: " << gt_cost << endl;
        cout << "Dif is: " << dif  << endl;
    }
    assert (dif < 0.01);
    assert(g_path_indices.front() == mindex(start));
    assert(g_path_indices.back() == mindex(goal));
}    


// Return problem info as a string.
string Problem::info(){
    string s;
    s.reserve((int) cost*3);
    string sgs = (direct ? "": " de " + std::to_string(start_id) + " a " + std::to_string(goal_id));
    s += "Problema " + std::to_string(id_problem) + sgs + endl;
    if (valid){
        s += "Hay solución. Costo es " + std::to_string(cost);
        if (gt_cost > 0){
            s+= " Costo referencial: " + std::to_string(gt_cost);
        }
        s += endl;
        if (!direct){
            s += "El camino en SG es de largo " + std::to_string(id_path.size()) + endl;    
             s += "ID path: " + endl + v2s(id_path) + endl;
            s += "SG path: " + endl;
            for (size_t i = 0; i < sg_path_indices.size(); i++)
            {
                s+= v2s(i2p(sg_path_indices[i]));
            }
            s += endl;
        }
        s += "G path (" + std::to_string(g_path_indices.size()) + "): " + endl;
        for (size_t i = 0; i < g_path_indices.size(); i++)
        {
            s+= v2s(i2p(g_path_indices[i]));
        }        
        s+= endl;
        
    } else{
        s += "No hay solución\n";
    }
    return s;
}


// Preallocate memory for the solution.
void Problem::preallocate(int sg_path_size, int g_path_size){
    g_path_indices.reserve(g_path_size);
    sg_path_indices.reserve(sg_path_size);
    id_path.reserve(sg_path_size);
}


// Returns problem info as a json.
nlohmann::json Problem::to_json(){
    nlohmann::json js;
    nlohmann::json json_id_path = nlohmann::json::array(); 
    nlohmann::json json_path = nlohmann::json::array(); 
    
    for (int i = path_x.size() -1 ; i >=0; i--)
    {
        json_path.push_back({path_x[i], path_y[i]});
    }
    
    for (auto id: boost::adaptors::reverse(id_path))
    {
        json_id_path.push_back(id);
    }
    js["start_id"] = start_id;
    js["goal_id"] = goal_id;
    js["cost"] = cost;
    js["id_path"] = json_id_path;
    js["path"] = json_path;
    js["connect_time"] = connect_time;
    js["search_time"] = search_time;
    return js;
}