#include "graph.hpp"

/* 
Graph
Graph representations for Subgoal Graphs, CH subgoal graphs and Grid Graph
 */


Edge::Edge(NodeId target) : target(target) {}


// Edge info as string
string Edge::info(){
    string out = "Edge to " + std::to_string(target);
    return out;
}


// Subgoal info as string
string Subgoal::info(){
    string out = "Subgoal in " + v2s(get_pos());
    return out;
}



EdgeCH::EdgeCH(NodeId source, NodeId target, EdgeCost cost, EdgeId e1, EdgeId e2) : cost(cost), e1(e1), e2(e2){
    targets[FORWARD] = target;
    targets[BACKWARD] = source;
};


// Returns true if CH edge is a shortcut that must be unpacked.
bool EdgeCH::zipped() {return e1 <= (EdgeId) NO_BASIC_REACHABILITY;}
NodeCH::NodeCH(){};


size_t NodeCH::n_edges(){
    return edges[FORWARD].size() + edges[BACKWARD].size();
}


// Node info as string
string NodeCH::info() const{
    return "Level " + std::to_string(level) + " " + " contracted: " + std::to_string(is_contracted) + " fnc: " + v2s(fnc) + " out edges: " + v2s(edges[FORWARD]) + " inc edges: " + v2s(edges[BACKWARD]);
}


// For CH, eliminate edges towards nodes with lower hierarchy level.
size_t NodeCH::eliminate_lower_level_edges(){
    for (auto bw_fw:{FORWARD, BACKWARD}){
        edges[bw_fw] = vector<EdgeId>(edges[bw_fw].begin() + fnc[bw_fw], edges[bw_fw].end());
    }
    return fnc[0] +fnc[1];
}


// Edge to json
nlohmann::json EdgeCH::json(){
    nlohmann::json data;
    data["source"] = targets[FORWARD];
    data["target"] = targets[BACKWARD];
    data["cost"] = cost;
    data["e1"] = e1;
    data["e2"] = e2;
    return data;
}


// Node to json
nlohmann::json NodeCH::json(){
    nlohmann::json data;
    data["level"] = level;
    data["out_edges"] = edges[FORWARD];
    data["inc_edges"] = edges[BACKWARD];
    return data;
}


// Edge info as string.
string EdgeCH::info(){
    string s = "From " + std::to_string(targets[BACKWARD]) + " to " + std::to_string(targets[FORWARD]) + " cost " + std::to_string(cost);
    if (e1 <= NO_BASIC_REACHABILITY) s += " pass through " + v2s(vector<EdgeId>{e1, e2});
    else s+= " freespace reachable " + std::to_string(UINT_MAX - e1);
    return s;
}


EdgeCH::EdgeCH(array<NodeId, 2> targets, EdgeCost cost, EdgeId e1, EdgeId e2) :targets(targets), cost(cost), e1(e1), e2(e2){};


Subgoal::Subgoal(){};


Subgoal::Subgoal(PosIndex mindex) : mindex(mindex) {};


Node::Node(EdgeId first_edge, uint16_t n_edges) : first_edge(first_edge), n_edges(n_edges) {};


// Node info as string
string Node::info(){
    return "First edge, n_edges: " + std::to_string(first_edge) + ", " + std::to_string(n_edges); 
}


uint8_t GridNode::n_nbs(){
     return std::popcount(nbs);
}


// Grid Node initialization
GridNode::GridNode(PosIndex mindex, const Mapa& mapa) : mindex(mindex){
    array<Bool, 8> dir_nbs;
    dir_nbs.fill(false);
    nbs = 0;
    if (mapa.is_obstacle(mindex)) return;
    PosIndex nb_index;
    Dir d1, d2;
    APos pos = i2p(mindex), new_pos;
    for (Dir card_index = 0; card_index < 8; card_index+=2)
    {
        new_pos[0] = pos[0] + dirs.arrays[card_index][0];
        new_pos[1] = pos[1] + dirs.arrays[card_index][1];
        if (not mapa.is_inside(new_pos)) continue;
        nb_index = mindex + dirs.dindex[card_index];
        if (mapa.is_free(nb_index)){
            dir_nbs[card_index] = true;
        } 
    }
    for (Dir diag_index = 1; diag_index < 8; diag_index+=2)
    {
        new_pos[0] = pos[0] + dirs.arrays[diag_index][0];
        new_pos[1] = pos[1] + dirs.arrays[diag_index][1];
        if (not mapa.is_inside(new_pos)) continue;
        nb_index = mindex + dirs.dindex[diag_index];
        if (mapa.is_free(nb_index)){
            d1 = dir_offset(diag_index, LEFT);
            d2 = dir_offset(diag_index, RIGHT);
            if (dir_nbs[d1] and dir_nbs[d2]){
                dir_nbs[diag_index] = true;
            }
        }
    }
    
    for (Dir i = 0; i < 8; i++)
    {
        if (dir_nbs[i]) nbs += 1<<i;
    }
}


// Grid graph initialization.
Graph::Graph(const Mapa& mapa) : mapa(mapa) , name("grid-graph"){
    nodes.reserve(mapa.matrix.size());
    n_edges = 0;
    n_nodes = 0;
    for (size_t mindex = 0; mindex < mapa.matrix.size(); mindex++)
    {
        nodes.emplace_back(mindex, mapa);
        n_edges += nodes.back().n_nbs();
        if (mapa.is_free(mindex)) n_nodes++;
    }
    cout << "N nodes: " << n_nodes << endl;
    cout << "N edges: " << n_edges << endl;
}