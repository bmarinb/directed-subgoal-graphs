#ifndef GRAPH_H
#define GRAPH_H

#include "functions.hpp"
#include "mapa.hpp"
#include <bit>
#include <bitset>

/* 
Graph
Graph representations for Subgoal Graphs, CH subgoal graphs and Grid Graph
 */


// Edge in subgoal graphs 
struct Edge{
    NodeId target;
    Edge(NodeId target = -1);
    string info();
};


// CH edge
struct EdgeCH{
    array<NodeId, 2> targets;
    EdgeCost cost;
    EdgeId e1, e2;
    EdgeCH(array<NodeId, 2> targets, EdgeCost cost, EdgeId e1, EdgeId e2);
    EdgeCH(NodeId source=-1, NodeId target=-1, EdgeCost cost=0, EdgeId e1=DIAGONAL_FIRST, EdgeId e2=DIAGONAL_FIRST);
    string info();
    nlohmann::json json();
    bool zipped();
};


// Node CH.
class NodeCH{
    public:
    NodeCH();
    string info() const;
    inline void add_edge(bool is_bw, EdgeId eid){ 
        return edges[is_bw].push_back(eid);
    };
    size_t n_edges();
    nlohmann::json json();
    array<vector<EdgeId>, 2> edges;
    inline size_t edges_size(bool is_bw){
        return edges[is_bw].size();
    }
    bool is_contracted{false}, is_target{false};
    // Hierachy level
    NodeId level{0};
    size_t eliminate_lower_level_edges();
    // First non contracted neighbour
    array<EdgeId, 2> fnc{0, 0};
    private:
};


// Node edges 
class Node{
    public:
    Node(EdgeId first_edge=0, uint16_t n_edges=0);
    EdgeId first_edge;
    uint16_t n_edges;
    string info();
    inline EdgeId last_edge(){return first_edge + n_edges;};
    private:
};


// Subgoal position and direction
class Subgoal{
    public:
    Subgoal();
    Subgoal(PosIndex mindex);
    inline Pos get_x(){return mindex/width;}
    inline Pos get_y(){return mindex%width;}
    inline APos get_pos(){return i2p(mindex);};
    PosIndex mindex;
    string info();

    private:
};


// Grid node
class GridNode{
    public:
    PosIndex mindex;
    inline Pos get_x(){return mindex/width;}
    inline Pos get_y(){return mindex%width;}
    inline APos get_pos(){return i2p(mindex);};
    GridNode(PosIndex mindex, const Mapa& mapa);
    // Edges are represented in a single 8-bit integer.
    uint8_t nbs;
    uint8_t n_nbs();
    private:
};


// Grid graph
class Graph{
    public:
    Graph(const Mapa& mapa);
    uint n_edges, n_nodes;
    vector<GridNode> nodes;
    const Mapa& mapa;
    string name;
    private:
};




#endif