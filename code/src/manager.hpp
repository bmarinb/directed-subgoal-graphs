#ifndef MANAGER_H
#define MANAGER_H

#include "functions.hpp"
#include "solver.hpp"

/* 
Manager
Receives command line arguments and performs corresponding Solver and SolverStats calls.
Main usages:
- Solve a single map
- Solve a single benchmark
 */

class Manager{
    public:
    Manager(
        string root_folder, string game_name, string map_name, string custom_map_name,  string custom_graph_name, string ch_params_path, string avoidance_params_path, string stats_file_path, int gtype, int variant, int sub_algorithm, int n_problem,  uint n_maps, int first_map, int debug_node, bool debug_connection,  bool save_jsons, bool do_CH, bool rotate_map, bool solve, bool save_stats, const vector<int>& start, const vector<int>& goal, bool SG_from_scratch ,bool CH_from_scratch ,bool clrs_from_scratch
    );
    void solve_benchmark();
    void solve_map();
    ~Manager();

    private:

    string root_folder, game_name, map_name, custom_graph_name;
    int gtype, variant, sub_algorithm, 
    n_problem, n_maps, first_map, debug_node;
    bool debug_connection, save_jsons, do_CH, rotate_map, solve, save_stats, SG_from_scratch, CH_from_scratch, clrs_from_scratch;

    nlohmann::json ch_params;
    nlohmann::json avoidance_params;
    nlohmann::json stats_dict;
    Problem * custom_problem{nullptr};
    string scen_file, benchmark_folder, all_benchmarks_path, game_path, map_path;
    Solver * solver{nullptr};
    SolverStats * solver_stats{nullptr};
    string get_graph_name();
    void set_benchmarks_path();
    void set_game_path();
    void set_map_path();
    void set_scen_path();
    void average_and_save_stats(string filepath, nlohmann::json& stats, bool single_game, bool print=true);

};

void average_stats(nlohmann::json& stats_dict,nlohmann::json& averaged_stats, nlohmann::json& stats, bool include_map_name);

#endif