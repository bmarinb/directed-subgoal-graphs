# Directed Subgoal Graphs

Implementation of Directed Subgoal Graphs (DSG). Written in C++ - standard C++2a.


## Description
This repository contains the source code of several subgoal graphs that are used to solve the path planning problem in a 8-connected grid graph.
These algorithms are:
1. Directed Subgoal Graphs
2. Subgoal Graphs
3. Jump Point Graphs
4. Diagonal Merged Jump Point Graphs

We also provide an implementation of A* over the grid graph.

We use these algorithms to solve <a href="https://movingai.com/benchmarks/grids.html" >MovingAI </a> benchmark.


## Dependencies.
This implementation uses the following dependencies:
1. <a href="https://github.com/nlohmann/json"> nlohmann JSON </a>
2. <a href="https://github.com/rogersce/cnpy"> cnpy </a>
3. <a href="https://github.com/martinus/robin-hood-hashing"> Robin hood hashing </a> (included in ./code/src)
   
## Compilation
Create the a folder named *build* in the same level than *src*.
Compile with make and one of the following keywords. 
1. make om: To run Directed Subgoal Graphs (DSG) or Diagonal Merged Jump Point Graphs (JPD)
2. make nm: To run Jump Point Graphs (JP)
3. make ou: To run Subgoal Graphs (SG)



## Execution
Once the executable file is ready, the following parameters can be passed.

* --root_folder (-R): Path to MovingAI folder. Default is ../MovingAI/.
* --benchmark (-B): Name of the benchmark to execute.
  * Valid benchmarks are:
    * bg512, bgmaps, da2, dao
    * maze-X with X in {1, 2, 4, 8, 17}
    * random-X with X in {10, 15, 20, 25, 30, 35, 40}
    * room-X with X in {8, 16, 32, 64}
    * street-X with X in {256, 512, 1024}
* --map_name (-M): Name of the map to execute. A missing value preprocess all maps from the benchmark
* --graph_type (-T): 
    0. SG
    1. JP
    2. JPD
    3. DSG
    4. DSG (only diagonal subgoals)
    5. Grid Graph
* --CH (-C): 1 to use Contraction Hierarchies, 0 otherwise.
* --solve (-W): 1 to solve instances, 0 to perform preprocessing phase only.

A complete list of the parameters can be found in ./code/src/main.cpp


## Parameter files
The parameters to use Avoidance and Contraction Hierarchies can be found at ./code/params.
There, a json file determines the execution of the above mentioned procedures.
### Avoidance.
For each graph direction, forward and backward, we specify: 
1. Search avoidance: Boolean value to use search avoidance. True value is only used when CH is false.
2. Connect avoidance: Boolean value to use connect avoidance.  True value is only valid for backward connection.
3. Threshold: Fraction of avoidable subgoals that are required to use avoidance. 1 equals never using avoidance and 0 equals to always using avoidance.
### Contraction Hierarchies.
Parameters from <a href="https://algo2.iti.kit.edu/documents/routeplanning/geisberger_dipl.pdf">  CH </a>:
* Edge difference
* Contracted neighbours
* Search space weight
* Limit search space
* Lazy update recalculate threshold
* Lazy update check interval
  
Parameters introduced with DSG:
* Reduce shortcuts by R: Using a freespace reduction to improve unpacking procedure. The reachability relations currently supported are DF and CF.
* Remove redundant dijkstra: To remove redundant edges using Dijkstra based technique
* Remove symmetrical redundant: To remove redundant edgs using Symmetry based technique
* Remove octile reachable: To remove redundant edges using freespace reachability relations technique.
* Save/load ordering: Boolean. The default path for ordering is ./node_ordering.
